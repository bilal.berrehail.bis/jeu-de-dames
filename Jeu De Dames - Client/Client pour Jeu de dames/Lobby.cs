﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client_pour_Jeu_de_dames
{
    public partial class Lobby : Form
    {
        // --------- OBJETS ---------
        PageDeConnexion maPageDeConnexion; // Objet de la Form "PageDeConnexion"
        Menu monMenu; // Objet de la Form "Menu"
        PlateauDeJeu monPlateau; // Objet de la Form "PlateauDeJeu"
        ListeTournois mesTournois; // Objet de la Form "ListeTournois"
        TraitementDuClient monClient; // Objet de la classe "TraitementDuClient"
        // --------- OBJETS ---------

        /* Variables et importation nécéssaire à l'action de l'utilisateur sur le fenêtre */
        // --------- VARIABLES ---------
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        // --------- VARIABLES ---------

        // Constructeur
        public Lobby()
        {
            InitializeComponent();
        }

        // Evenement lorsque le clic de la souris est enfoncée et que la souris est sur le contrôle
        private void formMobile_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture(); 
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        // Récupère tous les objets de chaque Form/Classe afin de les réutiliser
        public void restoreObjects(PageDeConnexion _maPageDeConnexion, Menu _monMenu, PlateauDeJeu _monPlateau, ListeTournois _mesTournois, TraitementDuClient _monClient)
        {
            maPageDeConnexion = _maPageDeConnexion;
            monPlateau = _monPlateau;
            monMenu = _monMenu;
            mesTournois = _mesTournois;
            monClient = _monClient;
        }

        // Evenement du bouton pour fermer la Form
        private void boutonFermer_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        int tournmt_id; // Identifiant du tournoi

        // Envoi d'un paquet au serveur pour récuperer l'état du tournoi inscrits
        public void sendGetLobbyTournmtPacket()
        {
            monClient.send("get_lobby_tournmt");
        }
        
        // Evenement appelé lorsque le Lobby est chargé
        private void Lobby_Load(object sender, EventArgs e)
        {
            sendGetLobbyTournmtPacket();
        }

        // Evenement appelé lorsque le bouton de connexion au Lobby est appuyé
        private void lobbyBtn_Click(object sender, EventArgs e)
        {
            switch (lobbyBtn.Text)
            {
                case "Se connecter":
                    monClient.send(String.Format("connect_lobby {0}", tournmt_id));
                    break;
                case "Se déconnecter":
                    monClient.send("disconnect_lobby");
                    break;
            }
            sendGetLobbyTournmtPacket();
        }
        
        // Evenement appelé lorsque le bouton de présentation à une phase de tournoi est appuyé
        private void phaseBtn_Click(object sender, EventArgs e)
        {
            switch (phaseBtn.Text)
            {
                case "Se présenter":
                    monClient.send(String.Format("connect_phase_tournmt {0}", tournmt_id));
                    break;
                case "Annuler":
                    monClient.send("disconnect_phase_tournmt");
                    break;
            }
            sendGetLobbyTournmtPacket();
        }
        
        // Evenement appelé lorsque le bouton d'actualisation de Lobby est appuyé
        private void refreshBtn_Click(object sender, EventArgs e)
        {
            sendGetLobbyTournmtPacket();
        }

        // Cache la page des Lobbys
        public void clearTournmtLobby()
        {
            if (this.InvokeRequired)
            {
                this.Invoke((MethodInvoker)delegate ()
                {
                    tournmtBox.Visible = false;
                });
            }
        }

        // Affichage de l'état du tournoi (Lobby) après réception d'un paquet envoyé par le serveur
        public void getFullTournmtLobby(int _tournmt_id, int state_lobby, int state_phase, string etat_tournmt, string tournmt_name, string dateStart, string nextPhaseStart)
        {
            tournmt_id = _tournmt_id;

            tournmtName.Text = tournmt_name;
            dateTournmt.Text = dateStart;

            etatTournmt.Text = etat_tournmt.Replace('_', ' ').Replace("@", Environment.NewLine);
            datePhaseTournmt.Text = nextPhaseStart;

            if (state_lobby == 1)
            {
                lobbyBtn.Text = "Se déconnecter";
            }
            else
                lobbyBtn.Text = "Se connecter";

            if (state_phase == 1)
            {
                phaseBtn.Text = "Annuler";
            }
            else
                phaseBtn.Text = "Se présenter";

            tournmtBox.Visible = true;

        }
    }
}
