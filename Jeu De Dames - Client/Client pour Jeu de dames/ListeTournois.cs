﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Bunifu.Framework.UI;

namespace Client_pour_Jeu_de_dames
{
    public partial class ListeTournois : Form
    {
        // --------- OBJETS ---------
        public PageDeConnexion maPageDeConnexion; // Objet de la Form "PageDeConnexion"
        public Menu monMenu; // Objet de la Form "Menu"
        public PlateauDeJeu monPlateau; // Objet de la Form "PlateauDeJeu"
        public Lobby monLobby; // Objet de la Form "Lobby"
        public TraitementDuClient monClient; // Objet de la classe "TraitementDuClient"
        // --------- OBJETS ---------

        // --------- VARIABLES ---------
        public structTournoi[] mesTournois; // Contient tous les tournois créer par l'animateur

        /* Variables et importation nécéssaire à l'action de l'utilisateur sur le fenêtre */
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        // --------- VARIABLES ---------

        // Constructeur
        public ListeTournois()
        {
            InitializeComponent();
        }
        
        // Récupère tous les objets de chaque Form/Classe afin de les réutiliser
        public void restoreObjects(PageDeConnexion _maPageDeConnexion, Menu _monMenu, PlateauDeJeu _monPlateau, Lobby _monLobby, TraitementDuClient _monClient)
        {
            maPageDeConnexion = _maPageDeConnexion;
            monPlateau = _monPlateau;
            monMenu = _monMenu;
            monLobby = _monLobby;
            monClient = _monClient;
        }

        // Evenement lorsque le clic de la souris est enfoncée et que la souris est sur le contrôle
        private void formMobile_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        // Evenement du bouton fermer de la Form
        private void boutonFermer_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        // Structure dédié caractéristiques de chaque tournoi
        public struct structTournoi
        {
            public int tournmt_id;
            public int nb_inscrits;
            public int max_players;
            public string nom_tournoi;
            public string deadline_reg;
            public string date_start;
            public int isregistered;
        }

        // Evenement appelé lorsque le bouton "inscription_Btn" est appelé
        private void inscription_BtnClick(object sender, EventArgs e)
        {
            Button inscriptionBtn = (Button)sender;

            int index_tournmt = Convert.ToInt32(inscriptionBtn.Name);

            int tournmt_id = mesTournois[index_tournmt].tournmt_id;

            string message = String.Format("Date limite d'inscription : {0} @Le tournoi commencera le : {1} @Souhaitez-vous vous inscrire à ce tournoi ?",
                mesTournois[index_tournmt].deadline_reg,
                mesTournois[index_tournmt].date_start);
            message = message.Replace("@", Environment.NewLine);

            DialogResult response = MessageBox.Show(message, "Information du tournoi", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

            if (response == DialogResult.Yes)
            {
                monClient.send(String.Format("inscription_tournmts {0}", tournmt_id));
            }
        }

        // Evenement appelé lorsque le bouton "desinscription_Btn" est appelé
        private void desinscription_BtnClick(object sender, EventArgs e)
        {
            Button inscriptionBtn = (Button)sender;

            int index_tournmt = Convert.ToInt32(inscriptionBtn.Name);

            int tournmt_id = mesTournois[index_tournmt].tournmt_id;

            monClient.send(String.Format("desinscription_tournmts {0}", tournmt_id));
        }

        // Affichage d'un message de réussite lorsque de la désinscription au tournoi a été faite
        public void desinscription_success()
        {
            MessageBox.Show("Votre désinscription a bien été prise en compte.", "Désinscription réussie", MessageBoxButtons.OK, MessageBoxIcon.Information);

            sendGetTournmtsPacket();
        }

        // Permet de récuperer les tournois suite à la réception d'un paquet envoyé par le serveur
        public void recupererTournois(string packet)
        {
            packet = packet.Remove(0, packet.Split(' ')[0].Length + 1);

            string[] soloTournoi = packet.Split('|');

            mesTournois = new structTournoi[soloTournoi.Length];

            for (int i = 0; i < soloTournoi.Length; i++)
            {
                string[] infos_tournoi = soloTournoi[i].Split(' ');

                if (infos_tournoi.Length >= 9)
                {
                    if (FonctionsSecondaires.estUnNombre(infos_tournoi[0]) &&
                    FonctionsSecondaires.estUnNombre(infos_tournoi[1]) &&
                    FonctionsSecondaires.estUnNombre(infos_tournoi[2]) &&
                    FonctionsSecondaires.estUnNombre(infos_tournoi[3]))
                    {
                        structTournoi nouveauTournoi = new structTournoi();
                        nouveauTournoi.tournmt_id = Convert.ToInt32(infos_tournoi[0]);
                        nouveauTournoi.nb_inscrits = Convert.ToInt32(infos_tournoi[1]);
                        nouveauTournoi.max_players = Convert.ToInt32(infos_tournoi[2]);
                        nouveauTournoi.isregistered = Convert.ToInt32(infos_tournoi[3]);

                        string fusion_deadline = String.Format("{0} {1}", infos_tournoi[4], infos_tournoi[5]);
                        string fusion_startdate = String.Format("{0} {1}", infos_tournoi[6], infos_tournoi[7]);

                        nouveauTournoi.deadline_reg = fusion_deadline;
                        nouveauTournoi.date_start = fusion_startdate;

                        StringBuilder nom_tournoi = new StringBuilder();

                        for (int e = 8; e < infos_tournoi.Length; e++)
                        {
                            nom_tournoi.Append(infos_tournoi[e]);
                            nom_tournoi.Append(" ");
                        }

                        nouveauTournoi.nom_tournoi = nom_tournoi.ToString();

                        mesTournois[i] = nouveauTournoi;
                    }
                }
            }
        }

        Label[] nomTournoi; // Liste d'objet de Label permettant l'affichage du nom du tournoi
        Label[] nbPlayers; // Liste d'objet de Label permettant l'affichage du nombre de joueurs inscrits
        Button[] btnInscription; // Liste d'objet de Bouton permettant l'inscription/la désinscription d'un tournoi
        BunifuSeparator[] separationObj; // Liste d'objet de BunifuSeparator permettant la distinction entre chaque tournoi (barre horizontale)

        // Réinitialisation des listes des tous les objets permettant l'affichage des tournois
        public void clearTournmtsControls()
        {
            if (mesTournois != null)
            {
                int countControlsCreated = mesTournois.Length;

                for (int i = 0; i < countControlsCreated; i++)
                {
                    if (this.InvokeRequired)
                    {
                        this.Invoke((MethodInvoker)delegate ()
                        {
                            this.Controls.Remove(nomTournoi[i]);
                            this.Controls.Remove(nbPlayers[i]);
                            this.Controls.Remove(btnInscription[i]);
                            this.Controls.Remove(separationObj[i]);
                        });
                    }
                }
            }
        }

        // Permet d'afficher les tournois grâce à la récuperation des tournois effectué lors de l'envoi du paquet de la liste des tournois
        public void afficherTournois(string packet)
        {
            clearTournmtsControls();

            recupererTournois(packet);

            nomTournoi = new Label[mesTournois.Length];
            nbPlayers = new Label[mesTournois.Length];
            btnInscription = new Button[mesTournois.Length];
            separationObj = new BunifuSeparator[mesTournois.Length];

            for (int i = 0; i < mesTournois.Length; i++)
            {
                nomTournoi[i] = new Label();
                nomTournoi[i].Font = new Font("Century Gothic", 12);
                nomTournoi[i].Location = new Point(25, 70 + i * 50);
                nomTournoi[i].Text = mesTournois[i].nom_tournoi;
                nomTournoi[i].BackColor = Color.Transparent;
                nomTournoi[i].ForeColor = Color.White;

                nbPlayers[i] = new Label();
                nbPlayers[i].Font = new Font("Century Gothic", 12);
                nbPlayers[i].Location = new Point(165, 70 + i * 50);
                nbPlayers[i].Text = String.Format("{0} P / {1} P", mesTournois[i].nb_inscrits, mesTournois[i].max_players);
                nbPlayers[i].BackColor = Color.Transparent;
                nbPlayers[i].ForeColor = Color.White;
                nbPlayers[i].Size = new Size(70, 20);

                btnInscription[i] = new Button();
                btnInscription[i].Font = new Font("Century Gothic", 12);
                btnInscription[i].Location = new Point(270, 65 + i * 50);
                btnInscription[i].Size = new Size(105, 30);
                btnInscription[i].Name = i.ToString();
                btnInscription[i].Cursor = Cursors.Hand;

                if (mesTournois[i].isregistered == 1)
                {
                    btnInscription[i].Text = "Désinscrire";
                    btnInscription[i].Click += desinscription_BtnClick;
                }
                else
                {
                    btnInscription[i].Text = "Inscrire";
                    btnInscription[i].Click += inscription_BtnClick;
                }

                separationObj[i] = new BunifuSeparator();
                separationObj[i].Location = new Point(14, 100 + i * 50);
                separationObj[i].Size = new Size(375, 10);

                if (this.InvokeRequired)
                {
                    this.Invoke((MethodInvoker)delegate ()
                    {
                        this.Controls.Add(nomTournoi[i]);
                        this.Controls.Add(nbPlayers[i]);
                        this.Controls.Add(btnInscription[i]);
                        this.Controls.Add(separationObj[i]);
                    });
                }
            }
        }

        // Affichage des messages d'états en fonction de la réponse du serveur suite à une tentative d'inscription
        public void responseInscriptionTournmt(int response_id)
        {
            switch (response_id)
            {
                case 0:
                    MessageBox.Show("La connexion au serveur a échoué", "Impossible de se connecter au serveur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case 1:
                    MessageBox.Show("Le tournoi choisi n'existe pas", "Tournoi introuvable", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    break;
                case 2:
                    MessageBox.Show("Le tournoi a déjà commencé.", "Tournoi en cours", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    break;
                case 3:
                    MessageBox.Show("Les inscriptions au tournoi sont closes", "Trop tard pour l'inscription", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case 4:
                    MessageBox.Show("Vous êtes déjà inscrit dans un tournoi", "Déjà inscrit", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case 5:
                    MessageBox.Show("Plus de places disponibles dans ce tournoi", "Choisissez-en un autre", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case 6:
                    MessageBox.Show("L'inscription au tournoi a été pris en compte !", "Inscription réussie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
            }
            sendGetTournmtsPacket();
        }
        
        // Evenement du bouton afin d'actualiser l'état de la liste des tournois
        private void refreshBtn_Click(object sender, EventArgs e)
        {
            sendGetTournmtsPacket();
        }

        // Envoi d'un paquet au serveur permettant de récuperer le paquets de la liste des tournois
        public void sendGetTournmtsPacket()
        {
            monClient.send("get_tournmts");
        }
    }
}
