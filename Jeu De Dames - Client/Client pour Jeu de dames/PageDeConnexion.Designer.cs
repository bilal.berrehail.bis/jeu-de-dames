﻿namespace Client_pour_Jeu_de_dames
{
	partial class PageDeConnexion
	{
		/// <summary>
		/// Variable nécessaire au concepteur.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Nettoyage des ressources utilisées.
		/// </summary>
		/// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Code généré par le Concepteur Windows Form

		/// <summary>
		/// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette méthode avec l'éditeur de code.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageDeConnexion));
            this.boutonFermer = new Bunifu.Framework.UI.BunifuImageButton();
            this.boutonReduire = new Bunifu.Framework.UI.BunifuImageButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonConnecter = new System.Windows.Forms.PictureBox();
            this.pswBox = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.userBox = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.boutonFermer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.boutonReduire)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonConnecter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // boutonFermer
            // 
            this.boutonFermer.BackColor = System.Drawing.Color.Transparent;
            this.boutonFermer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.boutonFermer.Image = ((System.Drawing.Image)(resources.GetObject("boutonFermer.Image")));
            this.boutonFermer.ImageActive = null;
            this.boutonFermer.Location = new System.Drawing.Point(310, 10);
            this.boutonFermer.Name = "boutonFermer";
            this.boutonFermer.Size = new System.Drawing.Size(30, 30);
            this.boutonFermer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.boutonFermer.TabIndex = 13;
            this.boutonFermer.TabStop = false;
            this.boutonFermer.Zoom = 10;
            this.boutonFermer.Click += new System.EventHandler(this.boutonFermer_Click_1);
            // 
            // boutonReduire
            // 
            this.boutonReduire.BackColor = System.Drawing.Color.Transparent;
            this.boutonReduire.Cursor = System.Windows.Forms.Cursors.Hand;
            this.boutonReduire.Image = ((System.Drawing.Image)(resources.GetObject("boutonReduire.Image")));
            this.boutonReduire.ImageActive = null;
            this.boutonReduire.Location = new System.Drawing.Point(310, 46);
            this.boutonReduire.Name = "boutonReduire";
            this.boutonReduire.Size = new System.Drawing.Size(30, 30);
            this.boutonReduire.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.boutonReduire.TabIndex = 14;
            this.boutonReduire.TabStop = false;
            this.boutonReduire.Zoom = 10;
            this.boutonReduire.Click += new System.EventHandler(this.boutonReduire_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Monotype Corsiva", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.Control;
            this.label2.Location = new System.Drawing.Point(91, 264);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(168, 33);
            this.label2.TabIndex = 20;
            this.label2.Text = "Mot de passe :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Monotype Corsiva", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Control;
            this.label1.Location = new System.Drawing.Point(68, 183);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(215, 33);
            this.label1.TabIndex = 19;
            this.label1.Text = "Nom d\'utilisateur :";
            // 
            // buttonConnecter
            // 
            this.buttonConnecter.BackColor = System.Drawing.Color.Transparent;
            this.buttonConnecter.Image = ((System.Drawing.Image)(resources.GetObject("buttonConnecter.Image")));
            this.buttonConnecter.Location = new System.Drawing.Point(42, 378);
            this.buttonConnecter.Name = "buttonConnecter";
            this.buttonConnecter.Size = new System.Drawing.Size(266, 38);
            this.buttonConnecter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.buttonConnecter.TabIndex = 18;
            this.buttonConnecter.TabStop = false;
            this.buttonConnecter.Click += new System.EventHandler(this.buttonConnecter_Click);
            this.buttonConnecter.MouseDown += new System.Windows.Forms.MouseEventHandler(this.buttonConnecter_MouseDown);
            this.buttonConnecter.MouseLeave += new System.EventHandler(this.buttonConnecter_MouseLeave);
            this.buttonConnecter.MouseMove += new System.Windows.Forms.MouseEventHandler(this.buttonConnecter_MouseMove);
            // 
            // pswBox
            // 
            this.pswBox.BackColor = System.Drawing.Color.Black;
            this.pswBox.BorderColorFocused = System.Drawing.Color.Silver;
            this.pswBox.BorderColorIdle = System.Drawing.Color.White;
            this.pswBox.BorderColorMouseHover = System.Drawing.Color.Silver;
            this.pswBox.BorderThickness = 3;
            this.pswBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.pswBox.Font = new System.Drawing.Font("Constantia", 13F, System.Drawing.FontStyle.Bold);
            this.pswBox.ForeColor = System.Drawing.Color.LightCyan;
            this.pswBox.isPassword = true;
            this.pswBox.Location = new System.Drawing.Point(49, 298);
            this.pswBox.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.pswBox.Name = "pswBox";
            this.pswBox.Size = new System.Drawing.Size(252, 40);
            this.pswBox.TabIndex = 17;
            this.pswBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.pswBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.pswBox_KeyDown);
            // 
            // userBox
            // 
            this.userBox.BackColor = System.Drawing.Color.Black;
            this.userBox.BorderColorFocused = System.Drawing.Color.Silver;
            this.userBox.BorderColorIdle = System.Drawing.Color.White;
            this.userBox.BorderColorMouseHover = System.Drawing.Color.Silver;
            this.userBox.BorderThickness = 3;
            this.userBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.userBox.Font = new System.Drawing.Font("Constantia", 13F, System.Drawing.FontStyle.Bold);
            this.userBox.ForeColor = System.Drawing.Color.LightGoldenrodYellow;
            this.userBox.isPassword = false;
            this.userBox.Location = new System.Drawing.Point(49, 216);
            this.userBox.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.userBox.Name = "userBox";
            this.userBox.Size = new System.Drawing.Size(252, 40);
            this.userBox.TabIndex = 16;
            this.userBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(72, 10);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(211, 155);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 15;
            this.pictureBox2.TabStop = false;
            // 
            // PageDeConnexion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(350, 452);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonConnecter);
            this.Controls.Add(this.pswBox);
            this.Controls.Add(this.userBox);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.boutonReduire);
            this.Controls.Add(this.boutonFermer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PageDeConnexion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Page principale";
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PageDeConnexion_MouseDown);
            ((System.ComponentModel.ISupportInitialize)(this.boutonFermer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.boutonReduire)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonConnecter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

        private Bunifu.Framework.UI.BunifuImageButton boutonFermer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox buttonConnecter;
        private Bunifu.Framework.UI.BunifuMetroTextbox pswBox;
        private Bunifu.Framework.UI.BunifuMetroTextbox userBox;
        private System.Windows.Forms.PictureBox pictureBox2;
        private Bunifu.Framework.UI.BunifuImageButton boutonReduire;


    }
}

