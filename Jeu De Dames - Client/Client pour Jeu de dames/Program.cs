﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client_pour_Jeu_de_dames
{
      class Program
    {
        private static string ip = "127.0.0.1"; // IP de connexion au serveur
        private static int port = 8080; // Port de connexion au serveur

        // Chemin de fichier des pions
        private static string pionUnName = "Images/pions/pion_1.png"; 
        private static string pionUnNameKing = "Images/pions/pion_1_queen.png";
        private static string pionDeuxName = "Images/pions/pion_2.png";
        private static string pionDeuxNameKing = "Images/pions/pion_2_queen.png";

        [STAThread]
        // Fonction qui se lance en premier
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new PageDeConnexion());
        }
        
        // Récupère l'adresse IP
        public static string getIp()
        {
            return ip;
        }

        // Récupère le port
        public static int getPort()
        {
            return port;
        }

        // Récupère le chemin de fichier d'un pion en fonction de son id
        public static string getPionNameById(int id)
        {
            switch (id)
            {
                case 1:
                    return pionUnName;
                case 2:
                    return pionUnNameKing;
                case 3:
                    return pionDeuxName;
                case 4:
                    return pionDeuxNameKing;
            }
            return "-1";
        }

        // Récupère le chemin de fichier d'un pion
        public static string getPionUnName()
        {
            return pionUnName;
        }

        // Récupère le chemin de fichier d'un pion
        public static string getPionUnNameKing()
        {
            return pionUnNameKing;
        }

        // Récupère le chemin de fichier d'un pion
        public static string getPionDeuxName()
        {
            return pionDeuxName;
        }

        // Récupère le chemin de fichier d'un pion
        public static string getPionDeuxNameKing()
        {
            return pionDeuxNameKing;
        }
    }
}
