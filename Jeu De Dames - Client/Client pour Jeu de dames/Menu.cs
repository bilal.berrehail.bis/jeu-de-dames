﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client_pour_Jeu_de_dames
{
    public partial class Menu : Form
    {
        // --------- OBJETS ---------
        PageDeConnexion maPageDeConnexion; // Objet de la Form "PageDeConnexion"
        PlateauDeJeu monPlateau; // Objet de la Form "PlateauDeJeu"
        ListeTournois mesTournois; // Objet de la Form "ListeTournois"
        Lobby monLobby; // Objet de la Form "Lobby"
        TraitementDuClient monClient; // Objet de la classe "TraitementDuClient"
        // --------- OBJETS ---------

        /* Variables et importation nécéssaire à l'action de l'utilisateur sur le fenêtre */
        // --------- VARIABLES ---------
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        
        string pseudo;
        bool enCoursDeRecherche = false;
        // --------- VARIABLES ---------
        
        // Constructeur
        public Menu()
        {
            InitializeComponent();
        }

        // Récupère tous les objets de chaque Form/Classe afin de les réutiliser
        public void restoreObjects(PageDeConnexion _maPageDeConnexion, PlateauDeJeu _monPlateau, ListeTournois _mesTournois, Lobby _monLobby, TraitementDuClient _monClient)
        {
            maPageDeConnexion = _maPageDeConnexion;
            monPlateau = _monPlateau;
            mesTournois = _mesTournois;
            monClient = _monClient;
            monLobby = _monLobby;
        }
        
        // Récupere le pseudo de la page de connexion
        public void restorePseudo(string pseudoClient)
        {
            pseudo = pseudoClient;
        }

        // Actualise le score du joueur
        public void refreshScore(int victoireCount, int defaiteCount)
        {
            victoireCompteur.Text = victoireCount.ToString();
            defaiteCompteur.Text = defaiteCount.ToString();
        }

        // Evenement lorsque le clic de la souris est enfoncée et que la souris est sur le contrôle
        private void formMobile_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left) // Si le bouton pressé est bien celui de gauche
            {
                ReleaseCapture(); // On communique l'état de la form
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0); // En fonction du déplacement, on actualise la position de la form
            }
        }

        // Evenement appelé lorsque le bouton réduire est appelé
        private void boutonReduire_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        // Evenement appelé lorsque le bouton fermer est appelé
        private void boutonFermer_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        // On réinitialise les variables et les controls en rendant possible l'inscription à la liste d'attente
        public void reinitialiserLaRecherche()
        {
            enCoursDeRecherche = false;
            annulerBouton.Visible = false;
            inscriptionBouton.Visible = true;

            searchPlayerTextBox.BackColor = Color.FromArgb(36, 25, 80);
            searchPlayerTextBox.Enabled = true;
            searchPlayerCheckBox.Enabled = true;
            carefulModeCheckBox.Enabled = true;
        }

        // Fonction qui se lance au chargement du menu de jeu
        private void Menu_Load(object sender, EventArgs e) 
        {
            bienvenueLabel.Text = "Bienvenue," + Environment.NewLine + pseudo; // On affiche le nom d'utilisateur du client dans un message de bienvenue

            monClient.send("get_score"); // On récupère le score du client
        }

        // Si le client ferme le menu de jeu, tout l'environnement se ferme
        private void Menu_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(1);
        }

        // Inscription au tornoi
        public void inscription_Success(int successCode)
        {
            switch (successCode)
            {
                case 0: // Recherche aléatoire
                    this.Invoke((MethodInvoker)delegate ()
                    {
                        searchPlayerCheckBox.Enabled = false;
                        searchPlayerTextBox.Enabled = false;
                        carefulModeCheckBox.Enabled = false;
                        searchPlayerTextBox.BackColor = Color.Gray;

                        enCoursDeRecherche = true;

                        inscriptionBouton.Visible = false;
                        annulerBouton.Visible = true;
                    });
                    break;
                case 1: // Recherche par pseudo
                    this.Invoke((MethodInvoker)delegate ()
                    {
                        searchPlayerCheckBox.Enabled = false;
                        searchPlayerTextBox.Enabled = false;
                        carefulModeCheckBox.Enabled = false;
                        searchPlayerTextBox.BackColor = Color.Gray;

                        enCoursDeRecherche = true;

                        inscriptionBouton.Visible = false;
                        annulerBouton.Visible = true;
                    });
                    break;
            }
        }

        // Evenement appelé lorsque le bouton "inscriptionBouton" est appelé
        private void inscriptionBouton_Click(object sender, EventArgs e)
        {
            int carefulMode = Convert.ToInt32(carefulModeCheckBox.Checked);
            if (!enCoursDeRecherche) // Si le client n'est pas encore inscrit à la liste d'attente
            {
                if (!searchPlayerCheckBox.Checked) // La recherche est aléatoire
                {
                    monClient.send("inscription " + carefulMode); // On demande au serveur de l'inscrire à la liste d'attente
                }
                else // Le client recherche un joueur précis
                {
                    if (!String.IsNullOrEmpty(searchPlayerTextBox.text))
                    {
                        string pseudo_opponent = searchPlayerTextBox.text;
                        if (searchPlayerTextBox.text.Length >= 0 && searchPlayerTextBox.text.Length <= 12) // TODO : >= 5
                        {
                            if (pseudo_opponent != pseudo)
                            {
                                monClient.send("inscription " + searchPlayerTextBox.text + " " + carefulMode);
                            }
                            else
                            {
                                MessageBox.Show("Vous ne pouvez pas vous recherchez vous même ... T'es con !", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Le pseudo doit être compris entre 5 et 12 caractères !", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Le champ est vide !", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }

        // Evenement appelé lorsque le bouton "annulerBouton" est appelé
        private void annulerBouton_Click(object sender, EventArgs e)
        {
            if (enCoursDeRecherche) // Si le client est déjà inscrit à la liste d'attente
            {
                monClient.send("stop_inscription"); // On demande au serveur de l'enlever de la liste d'attente

                reinitialiserLaRecherche(); // On réinitialise les controls et les variables pour rendre à nouveau l'inscription possible
            }
        }

        // Evenement appelé lorsque l'état du le checkBox "searchPlayerCheckBox" change
        private void searchPlayerCheckBox_OnChange(object sender, EventArgs e)
        {
            searchPlayerTextBox.Enabled = searchPlayerCheckBox.Checked;
        }

        // ----- EVENEMENTS TOURNMTS BTN -----
        // Evenement lorsque la souris bouge sur le controle
        private void tournmtsBtn_MouseMove(object sender, MouseEventArgs e)
        {
            tournmtsBtn.BackColor = Color.FromArgb(36, 25, 120);
        }

        // Evenement lorsque la souris sort du controle
        private void tournmtsBtn_MouseLeave(object sender, EventArgs e)
        {
            tournmtsBtn.BackColor = Color.FromArgb(36, 25, 80);
        }

        // Evenement lorsque le bouton "tournmtsBtn" est cliqué
        private void tournmtsBtn_Click(object sender, EventArgs e)
        {
            if (!mesTournois.IsDisposed)
            {
                mesTournois.Show();
                mesTournois.sendGetTournmtsPacket();
            }
        }

        // Evenement lorsque le bouton "lobbyBtn" est cliqué
        private void lobbyBtn_Click(object sender, EventArgs e)
        {
            if (!monLobby.IsDisposed)
            {
                monLobby.Show();
                monLobby.sendGetLobbyTournmtPacket();
            }
        }

        // Evenement lorsque le souris bouge sur le controle
        private void lobbyBtn_MouseMove(object sender, MouseEventArgs e)
        {
            lobbyBtn.BackColor = Color.FromArgb(36, 25, 120);
        }

        // Evenement lorsque la souris quitte le controle
        private void lobbyBtn_MouseLeave(object sender, EventArgs e)
        {
            lobbyBtn.BackColor = Color.FromArgb(36, 25, 80);
        }

        // Evenement lorsque la souris reste sur le controle
        private void lobbyBtn_MouseHover(object sender, EventArgs e)
        {
            ToolTip myToolTip = new ToolTip();
            myToolTip.SetToolTip(lobbyBtn, "Salle d'attente des tournois");
        }

        // Evenement lorsque la souris reste sur le controle
        private void tournmtsBtn_MouseHover(object sender, EventArgs e)
        {
            ToolTip myToolTip = new ToolTip();
            myToolTip.SetToolTip(tournmtsBtn, "Liste des tournois");
        }
        // ----- EVENEMENTS TOURNMTS BUTTONS -----
    }
}