﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client_pour_Jeu_de_dames
{
	public partial class PlateauDeJeu : Form
	{
        // --------- OBJETS ---------
        public PageDeConnexion maPageDeConnexion; // Objet de la Form "PageDeConnexion"
        public Menu monMenu; // Objet de la Form "Menu"
        public ListeTournois mesTournois; // Objet de la Form "ListeTournois"
        public Lobby monLobby; // Objet de la Form "Lobby"
        public TraitementDuClient monClient; // Objet de la classe "TraitementDuClient"
        // --------- OBJETS ---------

        // --------- VARIABLES ---------
        /* Variables et importation nécéssaire à l'action de l'utilisateur sur le fenêtre */
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        bool creationDuTableau = false; // Si le plateau a déjà été déssiné

		Cases[][] cases = new Cases[10][]; // Tableau double dimension contenat les cases du plateau de jeu

		int nombreRectangleHorizontal = 0; // Nombre de rectangles à l'horizontal
		int nombreRectangleVertical = 0; // Nombre de rectangles à la vertical
        
		const int rectangleWidth = 50; // Largeur d'un rectangle
		const int rectangleHeight = 50; // Hauteur d'un rectangle
        
        bool infoOpen = false; // Si la fenêter "InfoPlayer" est ouvert
        // --------- VARIABLES ---------

        // Constructeur
        public PlateauDeJeu()
        {
            InitializeComponent();
        }

        // Récupère tous les objets de chaque Form/Classe afin de les réutiliser
        public void restoreObjects(PageDeConnexion _maPageDeConnexion, Menu _monMenu, ListeTournois _mesTournois, Lobby _monLobby, TraitementDuClient _monClient)
        {
            maPageDeConnexion = _maPageDeConnexion;
            monMenu = _monMenu;
            mesTournois = _mesTournois;
            monLobby = _monLobby;
            monClient = _monClient;
        }
        
        // Evenement lorsque le clic de la souris est enfoncée et que la souris est sur le contrôle
        private void PlateauDeJeu_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        // Structure dédié aux cases du plateau de jeu
        public struct Cases
		{
			public Rectangle rectangle;
			public bool isBlack;
			public PictureBox pictureB;
			public bool pawnExist;
			public bool pawnTop;
		}
        
        // Initialisation des cases du plateau de jeu
		public void initialisationVariableCases()
		{
			bool caseNoir = false;
			int index = 0;
			int indexNouvelleCase = 0;
			int[] ligne = new int[10];

			for (int i = 0; i < 100; i++)
			{
				int localisationCase = i + 1;

				if (localisationCase % 10 == 0 && i != 0)
				{
					ligne[index] = Convert.ToInt32(caseNoir);
					Cases[] nouvelleCase = new Cases[10];

					for (int e = 0; e < 10; e++)
					{
						nouvelleCase[e].isBlack = Convert.ToBoolean(ligne[e]);
					}

					cases[indexNouvelleCase] = nouvelleCase;
					ligne = new int[10];

                    index = 0;
					indexNouvelleCase++;
				}
				else if (i % 2 == 0)
				{
					ligne[index] = Convert.ToInt32(caseNoir);
					index++;
					caseNoir = !caseNoir;
				}
				else
				{
					ligne[index] = Convert.ToInt32(caseNoir);
                    index++;
                    caseNoir = !caseNoir;
				}
			}
		}

        // Insertion des images et des etats de la pictureBox présents dans chaque cases du plateau de jeu
		public void insertionPictureBox()
		{
			for (int y = 0; y < nombreRectangleHorizontal; y++) // On récupère tous les rectangles créé à l'horizontal
			{
                for (int x = 0; x < nombreRectangleVertical; x++) // On récupère tous les rectangles créé à la vertical
				{
					if (cases[y][x].isBlack) // Si la case est noire
					{
						PictureBox nouvellePictureBox = new PictureBox(); // On créé une nouvelle PictureBox

                        bool playerTop = monClient.playerTop;

						int locaX = cases[y][x].rectangle.X; // On récupère sa position X
						int locaY = cases[y][x].rectangle.Y; // On récupère sa position Y

                        // On transmet les variables récupérée dans les cases dans une PictureBox
						nouvellePictureBox.Location = new Point(locaX, locaY);
                        nouvellePictureBox.Width = cases[y][x].rectangle.Width;
                        nouvellePictureBox.Height = cases[y][x].rectangle.Height;
                        nouvellePictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
                        nouvellePictureBox.BackColor = Color.Transparent;
                        nouvellePictureBox.BorderStyle = BorderStyle.None;

                        // On créé des évenements permettant d'intéragir avec les cases
                        nouvellePictureBox.MouseDown += PictureBox_Down;
                        nouvellePictureBox.MouseUp += PictureBox_Up;
						
						Bitmap imagePion = null; // On créé un nouvel objet contenant l'image du pion

                        // Changement en fonction du "playerTop"
                        if (playerTop)
                        {
                            if (y > 5)
                            {
                                imagePion = (Bitmap)recupererImagesPions(true);
                            }
                            if (y < 4)
                            {
                                imagePion = (Bitmap)recupererImagesPions(false);
                            }
                        }
                        else
                        {
                            if (y < 4)
                            {
                                imagePion = (Bitmap)recupererImagesPions(true);
                            }
                            if (y > 5)
                            {
                                imagePion = (Bitmap)recupererImagesPions(false);
                            }
                        }

                        nouvellePictureBox.Image = imagePion; // On initialise l'image de la PictureBox avec l'image qu'on vient de récuperer

                        cases[y][x].pictureB = nouvellePictureBox; // On ajoute la PictureBox dans la case

						try
						{
                            panelPrincipal.Controls.Add(nouvellePictureBox); // On ajoute dans notre panel principale (Contenant les cases) la nouvelle PictureBox qu'on vient de créé
						}
						catch (Exception ex)
						{ }
					}
				}
			}
		}

        // Réinitialisation des pictureBox de chaque cases du plateau de jeu
		public void miseAZeroPictureBox()
		{
			for (int y = 0; y < nombreRectangleHorizontal; y++) // On traite tous les réctangles à l'horizontal
			{
				for (int x = 0; x < nombreRectangleVertical; x++) // On traite tous les réctangles à la verticale
				{
					if (cases[y][x].isBlack) // Si la case est noire
					{
                        bool playerTop = monClient.playerTop;

						cases[y][x].pictureB.Image = null; // On réinitialise l'image de PictureBox

                        // Changement de position en fonction du "playerTop"
                        if (playerTop)
                        {
                            if (y > 5)
                            {
                                cases[y][x].pictureB.Image = (Bitmap)recupererImagesPions(true);
                            }
                            if (y < 4)
                            {
                                cases[y][x].pictureB.Image = (Bitmap)recupererImagesPions(false);
                            }
                        }
                        else
                        {
                            if (y < 4)
                            {
                                cases[y][x].pictureB.Image = (Bitmap)recupererImagesPions(true);
                            }
                            if (y > 5)
                            {
                                cases[y][x].pictureB.Image = (Bitmap)recupererImagesPions(false);
                            }
                        }
					}
				}
			}
		}

        // Récuperation de l'image lié au pion en fonction de sa position dans le plateau et de son etat (Dame ou non)
		public Image recupererImagesPions(bool playerTop, bool isking = false)
		{
			if (playerTop) // Si le joueur se positionne en haut
			{
				if (isking) // Si le pion est un pion Roi
				{
                    return new Bitmap(Program.getPionUnNameKing()); // On récupère l'image du pion Roi
				}
                return new Bitmap(Program.getPionUnName()); // On récupère l'image du pion basique
			}
            // Si le joueur se positionne en bas
            if (isking) // Si le pion est un pion Roi
			{
                return new Bitmap(Program.getPionDeuxNameKing()); // On récupère l'image du pion Roi
			}
            return new Bitmap(Program.getPionDeuxName()); // On récupère l'image du pion basique
		}

        // Modification d'une case du plateau de jeu en fonction de ses coordonnées X et Y, de l'image à insérer, de la présence d'un pion ou non et de la position de base du pion sur le plateau
		public void modificationDesCases(int x, int y, string imageDuPion, bool pionExistant, bool pionEnHaut)
		{
			try
			{
                bool playerTop = monClient.playerTop;

                if (playerTop) // Changement de position en fonction du "playerTop"
                {
                    y = 9 - y;
                    x = 9 - x;
                }

				if (imageDuPion != "-1") // Si on ne souhaite pas supprimer le pion de la case
				{
					cases[y][x].pictureB.Image = new Bitmap(imageDuPion); // On récupère l'image du nouveau pion
				}
				else // On supprime le pion de la case
					cases[y][x].pictureB.Image = null;

				cases[y][x].pawnExist = pionExistant;
				cases[y][x].pawnTop = pionEnHaut;
			}
			catch (Exception ex)
			{ }
		}

        // Evenement se déclenchant au chargement du plateau de jeu
        private void PlateauDeJeu_Load(object sender, EventArgs e)
		{
			initialisationVariableCases(); // On initialise complétement les cases
		}

        // On dessine sur le panel principal (Où les 100 cases seront placées)
		private void panelPrincipal_Paint(object sender, PaintEventArgs e)
		{
			try
			{
				if (!creationDuTableau) // Si le tableau n'a toujours pas été créé
				{
					int nombreRectangle = 0; // Variable stockant le nombre de rectangle créé au fur et à mesure

                    // Récupère la taille du panel principal
					int panelWidth = panelPrincipal.Width;
					int panelHeight = panelPrincipal.Height;

					Pen monPinceau = new Pen(new SolidBrush(Color.Black), 1); // Objet permettant de dessiner nos traits avec l'épaisseur souhaitée

					for (int i = rectangleHeight; i <= panelHeight; i += rectangleHeight) // Dessine les lignes horizontales
					{
						//e.Graphics.DrawLine(monPinceau, 0, i, panelWidth, i); // Dessine la ligne
						nombreRectangleHorizontal++;
					}

					for (int i = rectangleWidth; i <= panelWidth; i += rectangleWidth) // Dessine les lignes verticales
					{
						//e.Graphics.DrawLine(monPinceau, i, 0, i, panelHeight);
                        nombreRectangleVertical++;
					}

					nombreRectangle = nombreRectangleHorizontal * nombreRectangleVertical; // On récupère le nombre de rectangle en fonction du nombre de rectangle à la verticale et à l'horizontal

					for (int x = 0; x < nombreRectangleHorizontal; x++)
					{
						for (int y = 0; y < nombreRectangleVertical; y++)
						{
							Rectangle nouveauRectangle = new Rectangle(rectangleHeight * y, rectangleWidth * x, rectangleWidth, rectangleHeight); // Création d'un nouvel objet Rectangle
							cases[x][y].rectangle = nouveauRectangle; // On ajoute le rectangle à notre case
						}
					}

					creationDuTableau = true; // On vient de créé le tableau donc on peut changer l'état de cette variable

					insertionPictureBox(); // On insère à présent les PictureBox qu'on va créé dans les cases
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
		}

        // Si le client ferme le plateau de jeu, tout l'environnement se ferme
        private void PlateauDeJeu_FormClosing(object sender, FormClosingEventArgs e)
		{
			Environment.Exit(1);
		}

		bool pionSelectionne = false; // Variable permettant de savoir si le client a déjà un pion en mémoire

		int xSelectionne; // Coordonnée X du pion que le client souhaite déplacer
        int ySelectionne; // Coordonnée Y du pion que le client souhaite déplacer

        // Prendre le pion
        private void PictureBox_Down(object sender, MouseEventArgs e)
		{
			if (!pionSelectionne) // Si aucun pion n'est sélectionné par le client
			{
				PictureBox pion_PictureBox = (PictureBox)sender; // On récupère la PictureBox sur lequel le client vient de cliquer

                xSelectionne = pion_PictureBox.Location.X / rectangleWidth; // On récupère les coordonnées X du pion
                ySelectionne = pion_PictureBox.Location.Y / rectangleHeight; // On récupère les coordonnées Y du pion

				pionSelectionne = true; // Le client vient, à présent, de sélectionner un pion
			}
		}

        // Lacher le pion
        private void PictureBox_Up(object sender, MouseEventArgs e)
		{
			if (pionSelectionne) // Si le client a sélectionné un pion
			{
				try
				{
                    bool playerTop = monClient.playerTop;
					// Fonctions contenues dans la classe "RecupererCoordonnesCurseur" trouvées sur Internet
                    Control controlObject = RecupererCoordonnesCurseur.findControlAtCursor(panelPrincipal.FindForm()); // On récupère les coordonnées du curseur en fonction du plateau de jeu

                    int x = controlObject.Location.X / rectangleWidth; // On récupère la coordonnée X de la case où le client souhaite déplacer son pion
                    int y = controlObject.Location.Y / rectangleHeight; // On récupère la coordonnée Y de la case où le client souhaite déplacer son pion
                    
                    // Changement de position en fonction du "playerTop"
                    if (playerTop)
                    {
                        x = 9 - x;
                        y = 9 - y;
                        xSelectionne = 9 - xSelectionne;
                        ySelectionne = 9 - ySelectionne;
                    }

					monClient.send("select " + x + " " + y + " " + xSelectionne + " " + ySelectionne); // On envoit le déplacement que le client souhaite effectué avec le pion selectionné au serveur
				}
				catch (Exception ex)
				{}
				pionSelectionne = false; // Le client ne sélectionne à présent, aucun pion
			}
		}

        // Fonction permettant d'ajouter un message à la ListBox depuis une autre classe
        public void addMessageChatBox(string message, int message_id) 
        {
            chatBox.Items.Add(message);
            chatBox.SelectedIndex = chatBox.Items.Count - 1;
        }

        // Fonction permettant de réinitialiser la ListBox depuis une autre classe
        public void clearChatBox()
        {
            chatBox.Items.Clear();
        }

        // Fonction permettant de changer l'accessibilité à la panel principal
        public void changeEnablePanel(bool stats)
        {
            panelPrincipal.Enabled = stats;
        }

        // Envoi d'un message via le chat du plateau de jeu
        private void sendMessage_Click(object sender, EventArgs e)
        {
            sendMessages();
        }

        // Envoi du message au serveur
        public void sendMessages()
        {
            string message = messengerBox.Text; // On récupère le message que l'on souhaite envoyé

            if (!String.IsNullOrEmpty(message)) // Si le message n'est pas vide
            {
                monClient.send("chat " + message); // On envoit le message au serveur
            }

            messengerBox.Text = ""; // On réinitialise la TextBox contenant le message que l'on souhaite envoyé
        }

        // Permet d'appuyer sur la touche entrée lorsque l'on souhaite envoyer un message
        private void messengerBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                sendMessages();
            }
        }

        // Ouverture de la fenêtre "InfosPlayer"
        private void infoOpponentButton_Click(object sender, EventArgs e)
        {
            if (!infoOpen)
            {
                monClient.send("infos_opponent");
            }
        }

        // Evenement proposant au joueur s'il souhaite abandonner la partie ou non
        private void closeButton_Click(object sender, EventArgs e)
        {
            DialogResult response = MessageBox.Show("Êtes-vous sûre de vouloir abandonner la partie ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (response == DialogResult.Yes)
            {
                Environment.Exit(1);
            }
        }

        // Initialisation de la variable permettant de savoir si la Form "InfosPlayer" est ouverte ou non
        public void disabledinfoOpen()
        {
            infoOpen = false;
        }

        // Ouverture de la Form "InfoPlayer" afin de visualiser les informations de l'adversaire
        public void openInfoPlayers(string pseudoPlayer, int victoryCount, int defeatCount, string dateCreated)
        {
            InfosPlayer monInfosPlayer = new InfosPlayer(this, pseudoPlayer, victoryCount, defeatCount, dateCreated);
            monInfosPlayer.Show();
            infoOpen = true;
        }
    }
}
