﻿namespace Client_pour_Jeu_de_dames
{
	partial class PlateauDeJeu
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PlateauDeJeu));
            this.chatBox = new System.Windows.Forms.ListBox();
            this.sendMessage = new System.Windows.Forms.Button();
            this.closeButton = new Bunifu.Framework.UI.BunifuFlatButton();
            this.infoOpponentButton = new Bunifu.Framework.UI.BunifuFlatButton();
            this.messengerBox = new Bunifu.Framework.UI.BunifuMetroTextbox();
            this.panelPrincipal = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // chatBox
            // 
            this.chatBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(43)))), ((int)(((byte)(61)))));
            this.chatBox.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chatBox.ForeColor = System.Drawing.SystemColors.ScrollBar;
            this.chatBox.FormattingEnabled = true;
            this.chatBox.ItemHeight = 15;
            this.chatBox.Location = new System.Drawing.Point(532, 57);
            this.chatBox.Name = "chatBox";
            this.chatBox.Size = new System.Drawing.Size(310, 469);
            this.chatBox.TabIndex = 5;
            // 
            // sendMessage
            // 
            this.sendMessage.Location = new System.Drawing.Point(769, 532);
            this.sendMessage.Name = "sendMessage";
            this.sendMessage.Size = new System.Drawing.Size(73, 31);
            this.sendMessage.TabIndex = 7;
            this.sendMessage.Text = "Envoyer";
            this.sendMessage.UseVisualStyleBackColor = true;
            this.sendMessage.Click += new System.EventHandler(this.sendMessage_Click);
            // 
            // closeButton
            // 
            this.closeButton.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(145)))), ((int)(((byte)(20)))), ((int)(((byte)(0)))));
            this.closeButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(145)))), ((int)(((byte)(20)))), ((int)(((byte)(0)))));
            this.closeButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.closeButton.BorderRadius = 0;
            this.closeButton.ButtonText = "Abandonner";
            this.closeButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.closeButton.DisabledColor = System.Drawing.Color.Gray;
            this.closeButton.Iconcolor = System.Drawing.Color.Transparent;
            this.closeButton.Iconimage = ((System.Drawing.Image)(resources.GetObject("closeButton.Iconimage")));
            this.closeButton.Iconimage_right = null;
            this.closeButton.Iconimage_right_Selected = null;
            this.closeButton.Iconimage_Selected = null;
            this.closeButton.IconMarginLeft = 0;
            this.closeButton.IconMarginRight = 0;
            this.closeButton.IconRightVisible = true;
            this.closeButton.IconRightZoom = 0D;
            this.closeButton.IconVisible = false;
            this.closeButton.IconZoom = 90D;
            this.closeButton.IsTab = false;
            this.closeButton.Location = new System.Drawing.Point(285, 14);
            this.closeButton.Name = "closeButton";
            this.closeButton.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(145)))), ((int)(((byte)(20)))), ((int)(((byte)(0)))));
            this.closeButton.OnHovercolor = System.Drawing.Color.White;
            this.closeButton.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(145)))), ((int)(((byte)(20)))), ((int)(((byte)(0)))));
            this.closeButton.selected = false;
            this.closeButton.Size = new System.Drawing.Size(227, 37);
            this.closeButton.TabIndex = 8;
            this.closeButton.Text = "Abandonner";
            this.closeButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.closeButton.Textcolor = System.Drawing.Color.White;
            this.closeButton.TextFont = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // infoOpponentButton
            // 
            this.infoOpponentButton.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(220)))));
            this.infoOpponentButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(220)))));
            this.infoOpponentButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.infoOpponentButton.BorderRadius = 0;
            this.infoOpponentButton.ButtonText = "Informations sur l\'adversaire";
            this.infoOpponentButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.infoOpponentButton.DisabledColor = System.Drawing.Color.Gray;
            this.infoOpponentButton.Iconcolor = System.Drawing.Color.Transparent;
            this.infoOpponentButton.Iconimage = ((System.Drawing.Image)(resources.GetObject("infoOpponentButton.Iconimage")));
            this.infoOpponentButton.Iconimage_right = null;
            this.infoOpponentButton.Iconimage_right_Selected = null;
            this.infoOpponentButton.Iconimage_Selected = null;
            this.infoOpponentButton.IconMarginLeft = 0;
            this.infoOpponentButton.IconMarginRight = 0;
            this.infoOpponentButton.IconRightVisible = true;
            this.infoOpponentButton.IconRightZoom = 0D;
            this.infoOpponentButton.IconVisible = false;
            this.infoOpponentButton.IconZoom = 90D;
            this.infoOpponentButton.IsTab = false;
            this.infoOpponentButton.Location = new System.Drawing.Point(22, 14);
            this.infoOpponentButton.Name = "infoOpponentButton";
            this.infoOpponentButton.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(220)))));
            this.infoOpponentButton.OnHovercolor = System.Drawing.Color.White;
            this.infoOpponentButton.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(220)))));
            this.infoOpponentButton.selected = false;
            this.infoOpponentButton.Size = new System.Drawing.Size(257, 37);
            this.infoOpponentButton.TabIndex = 9;
            this.infoOpponentButton.Text = "Informations sur l\'adversaire";
            this.infoOpponentButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.infoOpponentButton.Textcolor = System.Drawing.Color.White;
            this.infoOpponentButton.TextFont = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.infoOpponentButton.Click += new System.EventHandler(this.infoOpponentButton_Click);
            // 
            // messengerBox
            // 
            this.messengerBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(43)))), ((int)(((byte)(61)))));
            this.messengerBox.BorderColorFocused = System.Drawing.Color.White;
            this.messengerBox.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(177)))), ((int)(((byte)(136)))));
            this.messengerBox.BorderColorMouseHover = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(190)))), ((int)(((byte)(136)))));
            this.messengerBox.BorderThickness = 2;
            this.messengerBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.messengerBox.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messengerBox.ForeColor = System.Drawing.SystemColors.ScrollBar;
            this.messengerBox.isPassword = false;
            this.messengerBox.Location = new System.Drawing.Point(532, 533);
            this.messengerBox.Margin = new System.Windows.Forms.Padding(1);
            this.messengerBox.Name = "messengerBox";
            this.messengerBox.Size = new System.Drawing.Size(233, 31);
            this.messengerBox.TabIndex = 10;
            this.messengerBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.messengerBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.messengerBox_KeyDown);
            // 
            // panelPrincipal
            // 
            this.panelPrincipal.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelPrincipal.BackgroundImage")));
            this.panelPrincipal.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelPrincipal.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panelPrincipal.Enabled = false;
            this.panelPrincipal.Location = new System.Drawing.Point(22, 57);
            this.panelPrincipal.Name = "panelPrincipal";
            this.panelPrincipal.Size = new System.Drawing.Size(500, 500);
            this.panelPrincipal.TabIndex = 4;
            this.panelPrincipal.Paint += new System.Windows.Forms.PaintEventHandler(this.panelPrincipal_Paint);
            // 
            // PlateauDeJeu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(43)))), ((int)(((byte)(61)))));
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(857, 581);
            this.Controls.Add(this.messengerBox);
            this.Controls.Add(this.infoOpponentButton);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.sendMessage);
            this.Controls.Add(this.chatBox);
            this.Controls.Add(this.panelPrincipal);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PlateauDeJeu";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PlateauDeJeu";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PlateauDeJeu_FormClosing);
            this.Load += new System.EventHandler(this.PlateauDeJeu_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PlateauDeJeu_MouseDown);
            this.ResumeLayout(false);

		}

		#endregion
        private System.Windows.Forms.ListBox chatBox;
		private System.Windows.Forms.Button sendMessage;
        private Bunifu.Framework.UI.BunifuFlatButton closeButton;
        private Bunifu.Framework.UI.BunifuFlatButton infoOpponentButton;
        private Bunifu.Framework.UI.BunifuMetroTextbox messengerBox;
        private System.Windows.Forms.Panel panelPrincipal;
    }
}