﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;

namespace Client_pour_Jeu_de_dames
{
    public partial class PageDeConnexion : Form
    {
        // --------- OBJETS ---------
        Menu monMenu; // Objet de la Form "Menu"
        PlateauDeJeu monPlateau; // Objet de la Form "PlateauDeJeu"
        ListeTournois mesTournois; // Objet de la Form "ListeTournois"
        Lobby monLobby; // Objet de la Form "Lobby"
        TraitementDuClient monClient; // Objet de la classe "TraitementDuClient"
        // --------- OBJETS ---------

        /* Variables et importation nécéssaire à l'action de l'utilisateur sur le fenêtre */
        // --------- VARIABLES ---------
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        // --------- VARIABLES ---------

        // Constructeur
        public PageDeConnexion()
        {
            InitializeComponent();

            monMenu = new Menu();
            monPlateau = new PlateauDeJeu();
            mesTournois = new ListeTournois();
            monLobby = new Lobby();
            monClient = new TraitementDuClient(this, monMenu, monPlateau, mesTournois, monLobby);
        }

        // Evenement lorsque le clic de la souris est enfoncée et que la souris est sur le contrôle
        private void PageDeConnexion_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        // Tentative de connexion au serveur
        public void connexion()
        {
            try
            {
                string utilisateur = userBox.Text;
                string passe = pswBox.Text;
                
                if (String.IsNullOrEmpty(utilisateur) ||
                    String.IsNullOrEmpty(passe))
                {
                    MessageBox.Show("Veuillez remplir tous les champs vides.", "Erreur de remplissage des champs", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }

                monMenu.restorePseudo(utilisateur);

                monClient.connectServer(utilisateur, passe);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter au serveur. Veuillez réessayez.", "Erreur de connexion aux serveurs", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        // Affichage d'un message en fonction de l'état de la réponse du serveur suite à une tentative de connexion
        public void verifyAccount(int result)
        {
            switch (result)
            {
                case 0: // N'existe pas
                    MessageBox.Show("Les identifiants de connexion sont incorrects. Veuillez réessayez.", "Erreur d'authentification", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    break;
                case 1: // Déjà connecté
                    MessageBox.Show("Votre compte de jeu est déjà connecté.", "Erreur de connexion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case 2: // Disponible
                    monMenu.Show();
                    this.Hide();
                    break;
                case 3: // Pas de personnage " Jeu de dame " trouvé
                    MessageBox.Show("Votre compte de jeu est correct mais aucun personnage pour le Jeu de dames n'a été trouvé, veuillez en créer un", "Erreur de connexion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
            }
        }

        // Evenement appelé lorsque le bouton "boutonFermer" est cliqué
        private void boutonFermer_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }

        // Evenement appelé lorsque le bouton "boutonReduire" est cliqué
        private void boutonReduire_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        // Evenement appelé lorsque le bouton "buttonConnecter" est cliqué
        private void buttonConnecter_Click(object sender, EventArgs e)
        {
            connexion();
        }

        // Evenement appelé lorsque le bouton "buttonConnecte" quitte le controle
        private void buttonConnecter_MouseLeave(object sender, EventArgs e)
        {
            buttonConnecter.Image = Image.FromFile("Images/buttons/BTN_CONNECTER.jpg");
        }

        // Evenement appelé lorsque le bouton "buttonConnecter" se trouve sur le controle
        private void buttonConnecter_MouseMove(object sender, MouseEventArgs e)
        {
            buttonConnecter.Image = Image.FromFile("Images/buttons/BTN_CONNECTER_HOVER.jpg");
        }

        // Evenement appelé lorsque la souris reste cliqué sur le bouton "buttonConnecter"
        private void buttonConnecter_MouseDown(object sender, MouseEventArgs e)
        {
            buttonConnecter.Image = Image.FromFile("Images/buttons/BTN_CONNECTER_APPUYE.jpg");
        }

        // Evenement appelé lorsque une touche du clavier est appuyé lorsque le focus est présent sur la textBox "pswBox"
        private void pswBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                connexion();
            }
        }
    }
}
