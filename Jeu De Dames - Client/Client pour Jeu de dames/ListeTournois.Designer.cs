﻿namespace Client_pour_Jeu_de_dames
{
    partial class ListeTournois
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListeTournois));
            this.formMobile = new System.Windows.Forms.PictureBox();
            this.bunifuSeparator1 = new Bunifu.Framework.UI.BunifuSeparator();
            this.boutonFermer = new System.Windows.Forms.PictureBox();
            this.refreshBtn = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.formMobile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.boutonFermer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.refreshBtn)).BeginInit();
            this.SuspendLayout();
            // 
            // formMobile
            // 
            this.formMobile.BackColor = System.Drawing.Color.Transparent;
            this.formMobile.Location = new System.Drawing.Point(43, 7);
            this.formMobile.Name = "formMobile";
            this.formMobile.Size = new System.Drawing.Size(318, 32);
            this.formMobile.TabIndex = 16;
            this.formMobile.TabStop = false;
            this.formMobile.MouseDown += new System.Windows.Forms.MouseEventHandler(this.formMobile_MouseDown);
            // 
            // bunifuSeparator1
            // 
            this.bunifuSeparator1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.bunifuSeparator1.LineThickness = 1;
            this.bunifuSeparator1.Location = new System.Drawing.Point(11, 33);
            this.bunifuSeparator1.Name = "bunifuSeparator1";
            this.bunifuSeparator1.Size = new System.Drawing.Size(380, 17);
            this.bunifuSeparator1.TabIndex = 14;
            this.bunifuSeparator1.Transparency = 255;
            this.bunifuSeparator1.Vertical = false;
            // 
            // boutonFermer
            // 
            this.boutonFermer.BackColor = System.Drawing.Color.Transparent;
            this.boutonFermer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.boutonFermer.Image = ((System.Drawing.Image)(resources.GetObject("boutonFermer.Image")));
            this.boutonFermer.Location = new System.Drawing.Point(366, 10);
            this.boutonFermer.Name = "boutonFermer";
            this.boutonFermer.Size = new System.Drawing.Size(26, 26);
            this.boutonFermer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.boutonFermer.TabIndex = 13;
            this.boutonFermer.TabStop = false;
            this.boutonFermer.Click += new System.EventHandler(this.boutonFermer_Click);
            // 
            // refreshBtn
            // 
            this.refreshBtn.BackColor = System.Drawing.Color.Transparent;
            this.refreshBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.refreshBtn.Image = ((System.Drawing.Image)(resources.GetObject("refreshBtn.Image")));
            this.refreshBtn.Location = new System.Drawing.Point(9, 7);
            this.refreshBtn.Name = "refreshBtn";
            this.refreshBtn.Size = new System.Drawing.Size(33, 34);
            this.refreshBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.refreshBtn.TabIndex = 17;
            this.refreshBtn.TabStop = false;
            this.refreshBtn.Click += new System.EventHandler(this.refreshBtn_Click);
            // 
            // ListeTournois
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(403, 470);
            this.Controls.Add(this.refreshBtn);
            this.Controls.Add(this.formMobile);
            this.Controls.Add(this.bunifuSeparator1);
            this.Controls.Add(this.boutonFermer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ListeTournois";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ListeTournois";
            ((System.ComponentModel.ISupportInitialize)(this.formMobile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.boutonFermer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.refreshBtn)).EndInit();
            this.ResumeLayout(false);

        }



        #endregion

        private System.Windows.Forms.PictureBox formMobile;
        private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator1;
        private System.Windows.Forms.PictureBox boutonFermer;
        private System.Windows.Forms.PictureBox refreshBtn;
    }
}