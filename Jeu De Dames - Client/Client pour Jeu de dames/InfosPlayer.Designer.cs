﻿namespace Client_pour_Jeu_de_dames
{
    partial class InfosPlayer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InfosPlayer));
            this.formMobile = new System.Windows.Forms.PictureBox();
            this.bunifuSeparator1 = new Bunifu.Framework.UI.BunifuSeparator();
            this.boutonFermer = new System.Windows.Forms.PictureBox();
            this.pseudoLabel = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel2 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.victoryCountLabel = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.defeatCountLabel = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel3 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.createdDateLabel = new Bunifu.Framework.UI.BunifuCustomLabel();
            ((System.ComponentModel.ISupportInitialize)(this.formMobile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.boutonFermer)).BeginInit();
            this.SuspendLayout();
            // 
            // formMobile
            // 
            this.formMobile.BackColor = System.Drawing.Color.Transparent;
            this.formMobile.Location = new System.Drawing.Point(11, 4);
            this.formMobile.Name = "formMobile";
            this.formMobile.Size = new System.Drawing.Size(278, 35);
            this.formMobile.TabIndex = 16;
            this.formMobile.TabStop = false;
            this.formMobile.MouseDown += new System.Windows.Forms.MouseEventHandler(this.formMobile_MouseDown);
            // 
            // bunifuSeparator1
            // 
            this.bunifuSeparator1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.bunifuSeparator1.LineThickness = 1;
            this.bunifuSeparator1.Location = new System.Drawing.Point(12, 35);
            this.bunifuSeparator1.Name = "bunifuSeparator1";
            this.bunifuSeparator1.Size = new System.Drawing.Size(300, 10);
            this.bunifuSeparator1.TabIndex = 14;
            this.bunifuSeparator1.Transparency = 255;
            this.bunifuSeparator1.Vertical = false;
            // 
            // boutonFermer
            // 
            this.boutonFermer.BackColor = System.Drawing.Color.Transparent;
            this.boutonFermer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.boutonFermer.Image = ((System.Drawing.Image)(resources.GetObject("boutonFermer.Image")));
            this.boutonFermer.Location = new System.Drawing.Point(291, 3);
            this.boutonFermer.Name = "boutonFermer";
            this.boutonFermer.Size = new System.Drawing.Size(26, 26);
            this.boutonFermer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.boutonFermer.TabIndex = 13;
            this.boutonFermer.TabStop = false;
            this.boutonFermer.Click += new System.EventHandler(this.boutonFermer_Click);
            // 
            // pseudoLabel
            // 
            this.pseudoLabel.BackColor = System.Drawing.Color.Transparent;
            this.pseudoLabel.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pseudoLabel.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.pseudoLabel.Location = new System.Drawing.Point(33, 57);
            this.pseudoLabel.Name = "pseudoLabel";
            this.pseudoLabel.Size = new System.Drawing.Size(256, 24);
            this.pseudoLabel.TabIndex = 21;
            this.pseudoLabel.Text = "Pseudo_Player";
            this.pseudoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.AutoSize = true;
            this.bunifuCustomLabel1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel1.ForeColor = System.Drawing.Color.Silver;
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(100, 99);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(103, 24);
            this.bunifuCustomLabel1.TabIndex = 22;
            this.bunifuCustomLabel1.Text = "Victoire : ";
            this.bunifuCustomLabel1.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // bunifuCustomLabel2
            // 
            this.bunifuCustomLabel2.AutoSize = true;
            this.bunifuCustomLabel2.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel2.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel2.ForeColor = System.Drawing.Color.Silver;
            this.bunifuCustomLabel2.Location = new System.Drawing.Point(101, 123);
            this.bunifuCustomLabel2.Name = "bunifuCustomLabel2";
            this.bunifuCustomLabel2.Size = new System.Drawing.Size(102, 24);
            this.bunifuCustomLabel2.TabIndex = 23;
            this.bunifuCustomLabel2.Text = "Défaite : ";
            this.bunifuCustomLabel2.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // victoryCountLabel
            // 
            this.victoryCountLabel.AutoSize = true;
            this.victoryCountLabel.BackColor = System.Drawing.Color.Transparent;
            this.victoryCountLabel.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.victoryCountLabel.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.victoryCountLabel.Location = new System.Drawing.Point(198, 99);
            this.victoryCountLabel.Name = "victoryCountLabel";
            this.victoryCountLabel.Size = new System.Drawing.Size(22, 24);
            this.victoryCountLabel.TabIndex = 24;
            this.victoryCountLabel.Text = "0";
            this.victoryCountLabel.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // defeatCountLabel
            // 
            this.defeatCountLabel.AutoSize = true;
            this.defeatCountLabel.BackColor = System.Drawing.Color.Transparent;
            this.defeatCountLabel.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.defeatCountLabel.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.defeatCountLabel.Location = new System.Drawing.Point(198, 123);
            this.defeatCountLabel.Name = "defeatCountLabel";
            this.defeatCountLabel.Size = new System.Drawing.Size(22, 24);
            this.defeatCountLabel.TabIndex = 25;
            this.defeatCountLabel.Text = "0";
            this.defeatCountLabel.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // bunifuCustomLabel3
            // 
            this.bunifuCustomLabel3.AutoSize = true;
            this.bunifuCustomLabel3.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel3.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel3.ForeColor = System.Drawing.Color.Silver;
            this.bunifuCustomLabel3.Location = new System.Drawing.Point(81, 164);
            this.bunifuCustomLabel3.Name = "bunifuCustomLabel3";
            this.bunifuCustomLabel3.Size = new System.Drawing.Size(180, 24);
            this.bunifuCustomLabel3.TabIndex = 28;
            this.bunifuCustomLabel3.Text = "Inscrit depuis le : ";
            this.bunifuCustomLabel3.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // createdDateLabel
            // 
            this.createdDateLabel.AutoSize = true;
            this.createdDateLabel.BackColor = System.Drawing.Color.Transparent;
            this.createdDateLabel.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createdDateLabel.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.createdDateLabel.Location = new System.Drawing.Point(49, 200);
            this.createdDateLabel.Name = "createdDateLabel";
            this.createdDateLabel.Size = new System.Drawing.Size(226, 24);
            this.createdDateLabel.TabIndex = 29;
            this.createdDateLabel.Text = "XX/XX/XXXX XX:XX:XX";
            this.createdDateLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // InfosPlayer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(49)))), ((int)(((byte)(60)))));
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(324, 260);
            this.Controls.Add(this.createdDateLabel);
            this.Controls.Add(this.bunifuCustomLabel3);
            this.Controls.Add(this.defeatCountLabel);
            this.Controls.Add(this.victoryCountLabel);
            this.Controls.Add(this.bunifuCustomLabel2);
            this.Controls.Add(this.bunifuCustomLabel1);
            this.Controls.Add(this.pseudoLabel);
            this.Controls.Add(this.formMobile);
            this.Controls.Add(this.bunifuSeparator1);
            this.Controls.Add(this.boutonFermer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "InfosPlayer";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "InfosPlayer";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.InfoPlayers_Load);
            ((System.ComponentModel.ISupportInitialize)(this.formMobile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.boutonFermer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox formMobile;
        private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator1;
        private System.Windows.Forms.PictureBox boutonFermer;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel2;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
        private Bunifu.Framework.UI.BunifuCustomLabel pseudoLabel;
        private Bunifu.Framework.UI.BunifuCustomLabel createdDateLabel;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel3;
        private Bunifu.Framework.UI.BunifuCustomLabel defeatCountLabel;
        private Bunifu.Framework.UI.BunifuCustomLabel victoryCountLabel;
    }
}