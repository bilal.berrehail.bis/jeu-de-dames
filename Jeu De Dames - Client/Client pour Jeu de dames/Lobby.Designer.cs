﻿namespace Client_pour_Jeu_de_dames
{
    partial class Lobby
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Lobby));
            this.formMobile = new System.Windows.Forms.PictureBox();
            this.bunifuSeparator1 = new Bunifu.Framework.UI.BunifuSeparator();
            this.boutonFermer = new System.Windows.Forms.PictureBox();
            this.bienvenueLabel = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuSeparator2 = new Bunifu.Framework.UI.BunifuSeparator();
            this.tournmtBox = new System.Windows.Forms.GroupBox();
            this.phaseBtn = new Bunifu.Framework.UI.BunifuFlatButton();
            this.datePhaseTournmt = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel4 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.etatTournmt = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel3 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuSeparator4 = new Bunifu.Framework.UI.BunifuSeparator();
            this.lobbyBtn = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuCustomLabel2 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.dateTournmt = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.tournmtName = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuSeparator3 = new Bunifu.Framework.UI.BunifuSeparator();
            this.refreshBtn = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.formMobile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.boutonFermer)).BeginInit();
            this.tournmtBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.refreshBtn)).BeginInit();
            this.SuspendLayout();
            // 
            // formMobile
            // 
            this.formMobile.BackColor = System.Drawing.Color.Transparent;
            this.formMobile.Location = new System.Drawing.Point(51, 9);
            this.formMobile.Name = "formMobile";
            this.formMobile.Size = new System.Drawing.Size(474, 32);
            this.formMobile.TabIndex = 20;
            this.formMobile.TabStop = false;
            this.formMobile.MouseDown += new System.Windows.Forms.MouseEventHandler(this.formMobile_MouseDown);
            // 
            // bunifuSeparator1
            // 
            this.bunifuSeparator1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.bunifuSeparator1.LineThickness = 1;
            this.bunifuSeparator1.Location = new System.Drawing.Point(11, 34);
            this.bunifuSeparator1.Name = "bunifuSeparator1";
            this.bunifuSeparator1.Size = new System.Drawing.Size(546, 17);
            this.bunifuSeparator1.TabIndex = 18;
            this.bunifuSeparator1.Transparency = 255;
            this.bunifuSeparator1.Vertical = false;
            // 
            // boutonFermer
            // 
            this.boutonFermer.BackColor = System.Drawing.Color.Transparent;
            this.boutonFermer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.boutonFermer.Image = ((System.Drawing.Image)(resources.GetObject("boutonFermer.Image")));
            this.boutonFermer.Location = new System.Drawing.Point(530, 8);
            this.boutonFermer.Name = "boutonFermer";
            this.boutonFermer.Size = new System.Drawing.Size(26, 26);
            this.boutonFermer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.boutonFermer.TabIndex = 17;
            this.boutonFermer.TabStop = false;
            this.boutonFermer.Click += new System.EventHandler(this.boutonFermer_Click);
            // 
            // bienvenueLabel
            // 
            this.bienvenueLabel.AutoSize = true;
            this.bienvenueLabel.BackColor = System.Drawing.Color.Transparent;
            this.bienvenueLabel.Font = new System.Drawing.Font("Century Gothic", 14.25F);
            this.bienvenueLabel.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.bienvenueLabel.Location = new System.Drawing.Point(117, 50);
            this.bienvenueLabel.Name = "bienvenueLabel";
            this.bienvenueLabel.Size = new System.Drawing.Size(337, 22);
            this.bienvenueLabel.TabIndex = 22;
            this.bienvenueLabel.Text = "Tournoi dans lequel vous êtes inscrit.";
            this.bienvenueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // bunifuSeparator2
            // 
            this.bunifuSeparator2.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.bunifuSeparator2.LineThickness = 1;
            this.bunifuSeparator2.Location = new System.Drawing.Point(170, 75);
            this.bunifuSeparator2.Name = "bunifuSeparator2";
            this.bunifuSeparator2.Size = new System.Drawing.Size(222, 10);
            this.bunifuSeparator2.TabIndex = 23;
            this.bunifuSeparator2.Transparency = 255;
            this.bunifuSeparator2.Vertical = false;
            // 
            // tournmtBox
            // 
            this.tournmtBox.BackColor = System.Drawing.Color.Transparent;
            this.tournmtBox.Controls.Add(this.phaseBtn);
            this.tournmtBox.Controls.Add(this.datePhaseTournmt);
            this.tournmtBox.Controls.Add(this.bunifuCustomLabel4);
            this.tournmtBox.Controls.Add(this.etatTournmt);
            this.tournmtBox.Controls.Add(this.bunifuCustomLabel3);
            this.tournmtBox.Controls.Add(this.bunifuSeparator4);
            this.tournmtBox.Controls.Add(this.lobbyBtn);
            this.tournmtBox.Controls.Add(this.bunifuCustomLabel2);
            this.tournmtBox.Controls.Add(this.bunifuCustomLabel1);
            this.tournmtBox.Controls.Add(this.dateTournmt);
            this.tournmtBox.Controls.Add(this.tournmtName);
            this.tournmtBox.Location = new System.Drawing.Point(23, 91);
            this.tournmtBox.Name = "tournmtBox";
            this.tournmtBox.Size = new System.Drawing.Size(525, 268);
            this.tournmtBox.TabIndex = 26;
            this.tournmtBox.TabStop = false;
            this.tournmtBox.Visible = false;
            // 
            // phaseBtn
            // 
            this.phaseBtn.Activecolor = System.Drawing.SystemColors.ActiveCaption;
            this.phaseBtn.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.phaseBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.phaseBtn.BorderRadius = 0;
            this.phaseBtn.ButtonText = "Se présenter";
            this.phaseBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.phaseBtn.DisabledColor = System.Drawing.Color.Gray;
            this.phaseBtn.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phaseBtn.Iconcolor = System.Drawing.Color.Transparent;
            this.phaseBtn.Iconimage = null;
            this.phaseBtn.Iconimage_right = null;
            this.phaseBtn.Iconimage_right_Selected = null;
            this.phaseBtn.Iconimage_Selected = null;
            this.phaseBtn.IconMarginLeft = 0;
            this.phaseBtn.IconMarginRight = 0;
            this.phaseBtn.IconRightVisible = true;
            this.phaseBtn.IconRightZoom = 0D;
            this.phaseBtn.IconVisible = true;
            this.phaseBtn.IconZoom = 55D;
            this.phaseBtn.IsTab = false;
            this.phaseBtn.Location = new System.Drawing.Point(278, 207);
            this.phaseBtn.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.phaseBtn.Name = "phaseBtn";
            this.phaseBtn.Normalcolor = System.Drawing.SystemColors.ActiveCaption;
            this.phaseBtn.OnHovercolor = System.Drawing.SystemColors.MenuHighlight;
            this.phaseBtn.OnHoverTextColor = System.Drawing.Color.Black;
            this.phaseBtn.selected = false;
            this.phaseBtn.Size = new System.Drawing.Size(241, 44);
            this.phaseBtn.TabIndex = 36;
            this.phaseBtn.Text = "Se présenter";
            this.phaseBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.phaseBtn.Textcolor = System.Drawing.Color.Black;
            this.phaseBtn.TextFont = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phaseBtn.Click += new System.EventHandler(this.phaseBtn_Click);
            // 
            // datePhaseTournmt
            // 
            this.datePhaseTournmt.BackColor = System.Drawing.Color.Transparent;
            this.datePhaseTournmt.Font = new System.Drawing.Font("Century Gothic", 14.25F);
            this.datePhaseTournmt.ForeColor = System.Drawing.Color.White;
            this.datePhaseTournmt.Location = new System.Drawing.Point(279, 144);
            this.datePhaseTournmt.Name = "datePhaseTournmt";
            this.datePhaseTournmt.Size = new System.Drawing.Size(240, 37);
            this.datePhaseTournmt.TabIndex = 35;
            this.datePhaseTournmt.Text = "{Date et heure}";
            this.datePhaseTournmt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bunifuCustomLabel4
            // 
            this.bunifuCustomLabel4.AutoSize = true;
            this.bunifuCustomLabel4.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel4.Font = new System.Drawing.Font("Century Gothic", 16.25F);
            this.bunifuCustomLabel4.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.bunifuCustomLabel4.Location = new System.Drawing.Point(295, 111);
            this.bunifuCustomLabel4.Name = "bunifuCustomLabel4";
            this.bunifuCustomLabel4.Size = new System.Drawing.Size(224, 25);
            this.bunifuCustomLabel4.TabIndex = 34;
            this.bunifuCustomLabel4.Text = "Début de la phase :";
            this.bunifuCustomLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // etatTournmt
            // 
            this.etatTournmt.BackColor = System.Drawing.Color.Transparent;
            this.etatTournmt.Font = new System.Drawing.Font("Century Gothic", 14.25F);
            this.etatTournmt.ForeColor = System.Drawing.Color.White;
            this.etatTournmt.Location = new System.Drawing.Point(279, 53);
            this.etatTournmt.Name = "etatTournmt";
            this.etatTournmt.Size = new System.Drawing.Size(240, 37);
            this.etatTournmt.TabIndex = 33;
            this.etatTournmt.Text = "{Etat}";
            this.etatTournmt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bunifuCustomLabel3
            // 
            this.bunifuCustomLabel3.AutoSize = true;
            this.bunifuCustomLabel3.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel3.Font = new System.Drawing.Font("Century Gothic", 16.25F);
            this.bunifuCustomLabel3.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.bunifuCustomLabel3.Location = new System.Drawing.Point(301, 19);
            this.bunifuCustomLabel3.Name = "bunifuCustomLabel3";
            this.bunifuCustomLabel3.Size = new System.Drawing.Size(201, 25);
            this.bunifuCustomLabel3.TabIndex = 32;
            this.bunifuCustomLabel3.Text = "Phase du tournoi :";
            this.bunifuCustomLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // bunifuSeparator4
            // 
            this.bunifuSeparator4.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator4.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.bunifuSeparator4.LineThickness = 1;
            this.bunifuSeparator4.Location = new System.Drawing.Point(263, 19);
            this.bunifuSeparator4.Name = "bunifuSeparator4";
            this.bunifuSeparator4.Size = new System.Drawing.Size(10, 161);
            this.bunifuSeparator4.TabIndex = 30;
            this.bunifuSeparator4.Transparency = 255;
            this.bunifuSeparator4.Vertical = true;
            // 
            // lobbyBtn
            // 
            this.lobbyBtn.Activecolor = System.Drawing.SystemColors.ActiveCaption;
            this.lobbyBtn.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.lobbyBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.lobbyBtn.BorderRadius = 0;
            this.lobbyBtn.ButtonText = "Se connecter";
            this.lobbyBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lobbyBtn.DisabledColor = System.Drawing.Color.Gray;
            this.lobbyBtn.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lobbyBtn.Iconcolor = System.Drawing.Color.Transparent;
            this.lobbyBtn.Iconimage = null;
            this.lobbyBtn.Iconimage_right = null;
            this.lobbyBtn.Iconimage_right_Selected = null;
            this.lobbyBtn.Iconimage_Selected = null;
            this.lobbyBtn.IconMarginLeft = 0;
            this.lobbyBtn.IconMarginRight = 0;
            this.lobbyBtn.IconRightVisible = true;
            this.lobbyBtn.IconRightZoom = 0D;
            this.lobbyBtn.IconVisible = true;
            this.lobbyBtn.IconZoom = 55D;
            this.lobbyBtn.IsTab = false;
            this.lobbyBtn.Location = new System.Drawing.Point(8, 207);
            this.lobbyBtn.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.lobbyBtn.Name = "lobbyBtn";
            this.lobbyBtn.Normalcolor = System.Drawing.SystemColors.ActiveCaption;
            this.lobbyBtn.OnHovercolor = System.Drawing.SystemColors.MenuHighlight;
            this.lobbyBtn.OnHoverTextColor = System.Drawing.Color.Black;
            this.lobbyBtn.selected = false;
            this.lobbyBtn.Size = new System.Drawing.Size(249, 44);
            this.lobbyBtn.TabIndex = 31;
            this.lobbyBtn.Text = "Se connecter";
            this.lobbyBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lobbyBtn.Textcolor = System.Drawing.Color.Black;
            this.lobbyBtn.TextFont = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lobbyBtn.Click += new System.EventHandler(this.lobbyBtn_Click);
            // 
            // bunifuCustomLabel2
            // 
            this.bunifuCustomLabel2.AutoSize = true;
            this.bunifuCustomLabel2.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel2.Font = new System.Drawing.Font("Century Gothic", 16.25F);
            this.bunifuCustomLabel2.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.bunifuCustomLabel2.Location = new System.Drawing.Point(8, 111);
            this.bunifuCustomLabel2.Name = "bunifuCustomLabel2";
            this.bunifuCustomLabel2.Size = new System.Drawing.Size(261, 25);
            this.bunifuCustomLabel2.TabIndex = 30;
            this.bunifuCustomLabel2.Text = "Date/heure du tournoi :";
            this.bunifuCustomLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.AutoSize = true;
            this.bunifuCustomLabel1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Century Gothic", 16.25F);
            this.bunifuCustomLabel1.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(43, 19);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(189, 25);
            this.bunifuCustomLabel1.TabIndex = 29;
            this.bunifuCustomLabel1.Text = "Nom du tournoi :";
            this.bunifuCustomLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dateTournmt
            // 
            this.dateTournmt.BackColor = System.Drawing.Color.Transparent;
            this.dateTournmt.Font = new System.Drawing.Font("Century Gothic", 14.25F);
            this.dateTournmt.ForeColor = System.Drawing.Color.White;
            this.dateTournmt.Location = new System.Drawing.Point(12, 137);
            this.dateTournmt.Name = "dateTournmt";
            this.dateTournmt.Size = new System.Drawing.Size(256, 44);
            this.dateTournmt.TabIndex = 28;
            this.dateTournmt.Text = "{Date et heure}";
            this.dateTournmt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tournmtName
            // 
            this.tournmtName.BackColor = System.Drawing.Color.Transparent;
            this.tournmtName.Font = new System.Drawing.Font("Century Gothic", 14.25F);
            this.tournmtName.ForeColor = System.Drawing.Color.White;
            this.tournmtName.Location = new System.Drawing.Point(13, 46);
            this.tournmtName.Name = "tournmtName";
            this.tournmtName.Size = new System.Drawing.Size(244, 44);
            this.tournmtName.TabIndex = 27;
            this.tournmtName.Text = "{Nom}";
            this.tournmtName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bunifuSeparator3
            // 
            this.bunifuSeparator3.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator3.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.bunifuSeparator3.LineThickness = 1;
            this.bunifuSeparator3.Location = new System.Drawing.Point(170, 367);
            this.bunifuSeparator3.Name = "bunifuSeparator3";
            this.bunifuSeparator3.Size = new System.Drawing.Size(222, 10);
            this.bunifuSeparator3.TabIndex = 27;
            this.bunifuSeparator3.Transparency = 255;
            this.bunifuSeparator3.Vertical = false;
            // 
            // refreshBtn
            // 
            this.refreshBtn.BackColor = System.Drawing.Color.Transparent;
            this.refreshBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.refreshBtn.Image = ((System.Drawing.Image)(resources.GetObject("refreshBtn.Image")));
            this.refreshBtn.Location = new System.Drawing.Point(13, 7);
            this.refreshBtn.Name = "refreshBtn";
            this.refreshBtn.Size = new System.Drawing.Size(33, 34);
            this.refreshBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.refreshBtn.TabIndex = 29;
            this.refreshBtn.TabStop = false;
            this.refreshBtn.Click += new System.EventHandler(this.refreshBtn_Click);
            // 
            // Lobby
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(570, 399);
            this.Controls.Add(this.refreshBtn);
            this.Controls.Add(this.bunifuSeparator3);
            this.Controls.Add(this.tournmtBox);
            this.Controls.Add(this.bunifuSeparator2);
            this.Controls.Add(this.bienvenueLabel);
            this.Controls.Add(this.formMobile);
            this.Controls.Add(this.bunifuSeparator1);
            this.Controls.Add(this.boutonFermer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Lobby";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.Lobby_Load);
            ((System.ComponentModel.ISupportInitialize)(this.formMobile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.boutonFermer)).EndInit();
            this.tournmtBox.ResumeLayout(false);
            this.tournmtBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.refreshBtn)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox formMobile;
        private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator1;
        private System.Windows.Forms.PictureBox boutonFermer;
        private Bunifu.Framework.UI.BunifuCustomLabel bienvenueLabel;
        private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator2;
        private System.Windows.Forms.GroupBox tournmtBox;
        private Bunifu.Framework.UI.BunifuCustomLabel dateTournmt;
        private Bunifu.Framework.UI.BunifuCustomLabel tournmtName;
        private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator3;
        private System.Windows.Forms.PictureBox refreshBtn;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel2;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
        private Bunifu.Framework.UI.BunifuFlatButton lobbyBtn;
        private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator4;
        private Bunifu.Framework.UI.BunifuCustomLabel etatTournmt;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel3;
        private Bunifu.Framework.UI.BunifuCustomLabel datePhaseTournmt;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel4;
        private Bunifu.Framework.UI.BunifuFlatButton phaseBtn;
    }
}