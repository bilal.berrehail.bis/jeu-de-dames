﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client_pour_Jeu_de_dames
{
    public partial class InfosPlayer : Form
    {
        /* Variables et importation nécéssaire à l'action de l'utilisateur sur le fenêtre */
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public string pseudoPlayer; // Pseudo du joueur
        public int victoryCount; // Nombre de victoire
        public int defeatCount; // Nombre de défaite
        public string createdDate; // Date à laquelle le joueur a créer son compte

        public PlateauDeJeu monPlateau; // Objet du plateau

        // Constructeur
        public InfosPlayer(PlateauDeJeu _monPlateau, string _pseudoPlayer, int _victoryCount, int _defeatCount, string _createdDate)
        {
            monPlateau = _monPlateau;

            pseudoPlayer = _pseudoPlayer;
            victoryCount = _victoryCount;
            defeatCount = _defeatCount;
            createdDate = _createdDate;

            InitializeComponent();
        }

        // Evenement lorsque le clique est enfoncé et que la souris est sur le controle
        private void formMobile_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left) // Si le bouton pressé est bien celui de gauche
            {
                ReleaseCapture(); // On communique l'état de la form
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0); // En fonction du déplacement, on actualise la position de la form
            }
        }

        // Evenement bouton exit de la fenêtre
        private void boutonFermer_Click(object sender, EventArgs e)
        {
            monPlateau.disabledinfoOpen();
            this.Close();
        }

        // Evenement qui se déclenche lors du chargement de la Form
        private void InfoPlayers_Load(object sender, EventArgs e)
        {
            pseudoLabel.Text = pseudoPlayer;
            victoryCountLabel.Text = victoryCount.ToString();
            defeatCountLabel.Text = defeatCount.ToString();
            createdDateLabel.Text = createdDate;
        }
    }
}
