﻿namespace Client_pour_Jeu_de_dames
{
	partial class Menu
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Menu));
            this.formMobile = new System.Windows.Forms.PictureBox();
            this.boutonReduire = new System.Windows.Forms.PictureBox();
            this.bunifuSeparator1 = new Bunifu.Framework.UI.BunifuSeparator();
            this.boutonFermer = new System.Windows.Forms.PictureBox();
            this.annulerBouton = new Bunifu.Framework.UI.BunifuFlatButton();
            this.defaiteCompteur = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.victoireCompteur = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel4 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel3 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bienvenueLabel = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.inscriptionBouton = new Bunifu.Framework.UI.BunifuFlatButton();
            this.searchPlayerCheckBox = new Bunifu.Framework.UI.BunifuCheckbox();
            this.bunifuCustomLabel2 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.searchPlayerTextBox = new Bunifu.Framework.UI.BunifuTextbox();
            this.bunifuCustomLabel5 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.carefulModeCheckBox = new Bunifu.Framework.UI.BunifuCheckbox();
            this.tournmtsBtn = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuSeparator2 = new Bunifu.Framework.UI.BunifuSeparator();
            this.bunifuSeparator3 = new Bunifu.Framework.UI.BunifuSeparator();
            this.bunifuSeparator4 = new Bunifu.Framework.UI.BunifuSeparator();
            this.lobbyBtn = new Bunifu.Framework.UI.BunifuImageButton();
            ((System.ComponentModel.ISupportInitialize)(this.formMobile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.boutonReduire)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.boutonFermer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tournmtsBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lobbyBtn)).BeginInit();
            this.SuspendLayout();
            // 
            // formMobile
            // 
            this.formMobile.BackColor = System.Drawing.Color.Transparent;
            this.formMobile.Location = new System.Drawing.Point(2, 1);
            this.formMobile.Name = "formMobile";
            this.formMobile.Size = new System.Drawing.Size(382, 35);
            this.formMobile.TabIndex = 12;
            this.formMobile.TabStop = false;
            this.formMobile.MouseDown += new System.Windows.Forms.MouseEventHandler(this.formMobile_MouseDown);
            // 
            // boutonReduire
            // 
            this.boutonReduire.BackColor = System.Drawing.Color.Transparent;
            this.boutonReduire.Cursor = System.Windows.Forms.Cursors.Hand;
            this.boutonReduire.Image = ((System.Drawing.Image)(resources.GetObject("boutonReduire.Image")));
            this.boutonReduire.Location = new System.Drawing.Point(388, 11);
            this.boutonReduire.Name = "boutonReduire";
            this.boutonReduire.Size = new System.Drawing.Size(26, 26);
            this.boutonReduire.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.boutonReduire.TabIndex = 11;
            this.boutonReduire.TabStop = false;
            this.boutonReduire.Click += new System.EventHandler(this.boutonReduire_Click);
            // 
            // bunifuSeparator1
            // 
            this.bunifuSeparator1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.bunifuSeparator1.LineThickness = 1;
            this.bunifuSeparator1.Location = new System.Drawing.Point(13, 28);
            this.bunifuSeparator1.Name = "bunifuSeparator1";
            this.bunifuSeparator1.Size = new System.Drawing.Size(424, 18);
            this.bunifuSeparator1.TabIndex = 10;
            this.bunifuSeparator1.Transparency = 255;
            this.bunifuSeparator1.Vertical = false;
            // 
            // boutonFermer
            // 
            this.boutonFermer.BackColor = System.Drawing.Color.Transparent;
            this.boutonFermer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.boutonFermer.Image = ((System.Drawing.Image)(resources.GetObject("boutonFermer.Image")));
            this.boutonFermer.Location = new System.Drawing.Point(417, 7);
            this.boutonFermer.Name = "boutonFermer";
            this.boutonFermer.Size = new System.Drawing.Size(26, 26);
            this.boutonFermer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.boutonFermer.TabIndex = 9;
            this.boutonFermer.TabStop = false;
            this.boutonFermer.Click += new System.EventHandler(this.boutonFermer_Click);
            // 
            // annulerBouton
            // 
            this.annulerBouton.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(40)))), ((int)(((byte)(0)))));
            this.annulerBouton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(40)))), ((int)(((byte)(0)))));
            this.annulerBouton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.annulerBouton.BorderRadius = 0;
            this.annulerBouton.ButtonText = "Annuler";
            this.annulerBouton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.annulerBouton.DisabledColor = System.Drawing.Color.Gray;
            this.annulerBouton.Iconcolor = System.Drawing.Color.Transparent;
            this.annulerBouton.Iconimage = null;
            this.annulerBouton.Iconimage_right = null;
            this.annulerBouton.Iconimage_right_Selected = null;
            this.annulerBouton.Iconimage_Selected = null;
            this.annulerBouton.IconMarginLeft = 0;
            this.annulerBouton.IconMarginRight = 0;
            this.annulerBouton.IconRightVisible = true;
            this.annulerBouton.IconRightZoom = 0D;
            this.annulerBouton.IconVisible = true;
            this.annulerBouton.IconZoom = 90D;
            this.annulerBouton.IsTab = false;
            this.annulerBouton.Location = new System.Drawing.Point(24, 204);
            this.annulerBouton.Name = "annulerBouton";
            this.annulerBouton.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(40)))), ((int)(((byte)(0)))));
            this.annulerBouton.OnHovercolor = System.Drawing.Color.White;
            this.annulerBouton.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(40)))), ((int)(((byte)(0)))));
            this.annulerBouton.selected = false;
            this.annulerBouton.Size = new System.Drawing.Size(254, 34);
            this.annulerBouton.TabIndex = 26;
            this.annulerBouton.Text = "Annuler";
            this.annulerBouton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.annulerBouton.Textcolor = System.Drawing.Color.White;
            this.annulerBouton.TextFont = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.annulerBouton.Visible = false;
            this.annulerBouton.Click += new System.EventHandler(this.annulerBouton_Click);
            // 
            // defaiteCompteur
            // 
            this.defaiteCompteur.AutoSize = true;
            this.defaiteCompteur.BackColor = System.Drawing.Color.Transparent;
            this.defaiteCompteur.Font = new System.Drawing.Font("Century Gothic", 12.25F, System.Drawing.FontStyle.Bold);
            this.defaiteCompteur.ForeColor = System.Drawing.Color.Silver;
            this.defaiteCompteur.Location = new System.Drawing.Point(255, 66);
            this.defaiteCompteur.Name = "defaiteCompteur";
            this.defaiteCompteur.Size = new System.Drawing.Size(19, 19);
            this.defaiteCompteur.TabIndex = 24;
            this.defaiteCompteur.Text = "0";
            // 
            // victoireCompteur
            // 
            this.victoireCompteur.AutoSize = true;
            this.victoireCompteur.BackColor = System.Drawing.Color.Transparent;
            this.victoireCompteur.Font = new System.Drawing.Font("Century Gothic", 12.25F, System.Drawing.FontStyle.Bold);
            this.victoireCompteur.ForeColor = System.Drawing.Color.Silver;
            this.victoireCompteur.Location = new System.Drawing.Point(255, 43);
            this.victoireCompteur.Name = "victoireCompteur";
            this.victoireCompteur.Size = new System.Drawing.Size(19, 19);
            this.victoireCompteur.TabIndex = 23;
            this.victoireCompteur.Text = "0";
            // 
            // bunifuCustomLabel4
            // 
            this.bunifuCustomLabel4.AutoSize = true;
            this.bunifuCustomLabel4.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel4.Font = new System.Drawing.Font("Century Gothic", 12.25F, System.Drawing.FontStyle.Bold);
            this.bunifuCustomLabel4.ForeColor = System.Drawing.Color.Silver;
            this.bunifuCustomLabel4.Location = new System.Drawing.Point(174, 66);
            this.bunifuCustomLabel4.Name = "bunifuCustomLabel4";
            this.bunifuCustomLabel4.Size = new System.Drawing.Size(82, 19);
            this.bunifuCustomLabel4.TabIndex = 22;
            this.bunifuCustomLabel4.Text = "Défaite  :";
            // 
            // bunifuCustomLabel3
            // 
            this.bunifuCustomLabel3.AutoSize = true;
            this.bunifuCustomLabel3.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel3.Font = new System.Drawing.Font("Century Gothic", 12.25F, System.Drawing.FontStyle.Bold);
            this.bunifuCustomLabel3.ForeColor = System.Drawing.Color.Silver;
            this.bunifuCustomLabel3.Location = new System.Drawing.Point(174, 42);
            this.bunifuCustomLabel3.Name = "bunifuCustomLabel3";
            this.bunifuCustomLabel3.Size = new System.Drawing.Size(81, 19);
            this.bunifuCustomLabel3.TabIndex = 21;
            this.bunifuCustomLabel3.Text = "Victoire :";
            // 
            // bienvenueLabel
            // 
            this.bienvenueLabel.AutoSize = true;
            this.bienvenueLabel.BackColor = System.Drawing.Color.Transparent;
            this.bienvenueLabel.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bienvenueLabel.ForeColor = System.Drawing.Color.White;
            this.bienvenueLabel.Location = new System.Drawing.Point(13, 38);
            this.bienvenueLabel.Name = "bienvenueLabel";
            this.bienvenueLabel.Size = new System.Drawing.Size(165, 48);
            this.bienvenueLabel.TabIndex = 20;
            this.bienvenueLabel.Text = "Bienvenue, \r\n123456789012 !";
            this.bienvenueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.AutoSize = true;
            this.bunifuCustomLabel1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel1.ForeColor = System.Drawing.Color.Gray;
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(17, 250);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(420, 17);
            this.bunifuCustomLabel1.TabIndex = 19;
            this.bunifuCustomLabel1.Text = "Cliquez sur \"Trouver un adversaire\" pour être ajouté à la liste d\'attente";
            // 
            // inscriptionBouton
            // 
            this.inscriptionBouton.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.inscriptionBouton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(177)))), ((int)(((byte)(136)))));
            this.inscriptionBouton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.inscriptionBouton.BorderRadius = 0;
            this.inscriptionBouton.ButtonText = "Trouver un adversaire";
            this.inscriptionBouton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.inscriptionBouton.DisabledColor = System.Drawing.Color.Gray;
            this.inscriptionBouton.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inscriptionBouton.Iconcolor = System.Drawing.Color.Transparent;
            this.inscriptionBouton.Iconimage = null;
            this.inscriptionBouton.Iconimage_right = null;
            this.inscriptionBouton.Iconimage_right_Selected = null;
            this.inscriptionBouton.Iconimage_Selected = null;
            this.inscriptionBouton.IconMarginLeft = 0;
            this.inscriptionBouton.IconMarginRight = 0;
            this.inscriptionBouton.IconRightVisible = true;
            this.inscriptionBouton.IconRightZoom = 0D;
            this.inscriptionBouton.IconVisible = true;
            this.inscriptionBouton.IconZoom = 55D;
            this.inscriptionBouton.IsTab = false;
            this.inscriptionBouton.Location = new System.Drawing.Point(20, 199);
            this.inscriptionBouton.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.inscriptionBouton.Name = "inscriptionBouton";
            this.inscriptionBouton.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(177)))), ((int)(((byte)(136)))));
            this.inscriptionBouton.OnHovercolor = System.Drawing.Color.White;
            this.inscriptionBouton.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(177)))), ((int)(((byte)(136)))));
            this.inscriptionBouton.selected = false;
            this.inscriptionBouton.Size = new System.Drawing.Size(270, 43);
            this.inscriptionBouton.TabIndex = 18;
            this.inscriptionBouton.Text = "Trouver un adversaire";
            this.inscriptionBouton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.inscriptionBouton.Textcolor = System.Drawing.Color.White;
            this.inscriptionBouton.TextFont = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inscriptionBouton.Click += new System.EventHandler(this.inscriptionBouton_Click);
            // 
            // searchPlayerCheckBox
            // 
            this.searchPlayerCheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(135)))), ((int)(((byte)(140)))));
            this.searchPlayerCheckBox.ChechedOffColor = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(135)))), ((int)(((byte)(140)))));
            this.searchPlayerCheckBox.Checked = false;
            this.searchPlayerCheckBox.CheckedOnColor = System.Drawing.Color.SeaGreen;
            this.searchPlayerCheckBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.searchPlayerCheckBox.ForeColor = System.Drawing.Color.White;
            this.searchPlayerCheckBox.Location = new System.Drawing.Point(22, 121);
            this.searchPlayerCheckBox.Name = "searchPlayerCheckBox";
            this.searchPlayerCheckBox.Size = new System.Drawing.Size(20, 20);
            this.searchPlayerCheckBox.TabIndex = 27;
            this.searchPlayerCheckBox.OnChange += new System.EventHandler(this.searchPlayerCheckBox_OnChange);
            // 
            // bunifuCustomLabel2
            // 
            this.bunifuCustomLabel2.AutoSize = true;
            this.bunifuCustomLabel2.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel2.ForeColor = System.Drawing.Color.White;
            this.bunifuCustomLabel2.Location = new System.Drawing.Point(49, 121);
            this.bunifuCustomLabel2.Name = "bunifuCustomLabel2";
            this.bunifuCustomLabel2.Size = new System.Drawing.Size(225, 21);
            this.bunifuCustomLabel2.TabIndex = 28;
            this.bunifuCustomLabel2.Text = "Rechercher un joueur précis";
            this.bunifuCustomLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // searchPlayerTextBox
            // 
            this.searchPlayerTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(25)))), ((int)(((byte)(80)))));
            this.searchPlayerTextBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("searchPlayerTextBox.BackgroundImage")));
            this.searchPlayerTextBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchPlayerTextBox.Enabled = false;
            this.searchPlayerTextBox.Font = new System.Drawing.Font("Century", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchPlayerTextBox.ForeColor = System.Drawing.Color.Silver;
            this.searchPlayerTextBox.Icon = ((System.Drawing.Image)(resources.GetObject("searchPlayerTextBox.Icon")));
            this.searchPlayerTextBox.Location = new System.Drawing.Point(20, 150);
            this.searchPlayerTextBox.Name = "searchPlayerTextBox";
            this.searchPlayerTextBox.Size = new System.Drawing.Size(156, 38);
            this.searchPlayerTextBox.TabIndex = 29;
            this.searchPlayerTextBox.text = "";
            // 
            // bunifuCustomLabel5
            // 
            this.bunifuCustomLabel5.AutoSize = true;
            this.bunifuCustomLabel5.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel5.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel5.ForeColor = System.Drawing.Color.White;
            this.bunifuCustomLabel5.Location = new System.Drawing.Point(48, 92);
            this.bunifuCustomLabel5.Name = "bunifuCustomLabel5";
            this.bunifuCustomLabel5.Size = new System.Drawing.Size(231, 21);
            this.bunifuCustomLabel5.TabIndex = 35;
            this.bunifuCustomLabel5.Text = "Règle : Souffler n\'est pas joué";
            this.bunifuCustomLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // carefulModeCheckBox
            // 
            this.carefulModeCheckBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(135)))), ((int)(((byte)(140)))));
            this.carefulModeCheckBox.ChechedOffColor = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(135)))), ((int)(((byte)(140)))));
            this.carefulModeCheckBox.Checked = false;
            this.carefulModeCheckBox.CheckedOnColor = System.Drawing.Color.SeaGreen;
            this.carefulModeCheckBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.carefulModeCheckBox.ForeColor = System.Drawing.Color.White;
            this.carefulModeCheckBox.Location = new System.Drawing.Point(22, 93);
            this.carefulModeCheckBox.Name = "carefulModeCheckBox";
            this.carefulModeCheckBox.Size = new System.Drawing.Size(20, 20);
            this.carefulModeCheckBox.TabIndex = 34;
            // 
            // tournmtsBtn
            // 
            this.tournmtsBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(25)))), ((int)(((byte)(80)))));
            this.tournmtsBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.tournmtsBtn.Image = ((System.Drawing.Image)(resources.GetObject("tournmtsBtn.Image")));
            this.tournmtsBtn.ImageActive = null;
            this.tournmtsBtn.Location = new System.Drawing.Point(387, 198);
            this.tournmtsBtn.Name = "tournmtsBtn";
            this.tournmtsBtn.Size = new System.Drawing.Size(49, 40);
            this.tournmtsBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.tournmtsBtn.TabIndex = 36;
            this.tournmtsBtn.TabStop = false;
            this.tournmtsBtn.Zoom = 10;
            this.tournmtsBtn.Click += new System.EventHandler(this.tournmtsBtn_Click);
            this.tournmtsBtn.MouseLeave += new System.EventHandler(this.tournmtsBtn_MouseLeave);
            this.tournmtsBtn.MouseHover += new System.EventHandler(this.tournmtsBtn_MouseHover);
            this.tournmtsBtn.MouseMove += new System.Windows.Forms.MouseEventHandler(this.tournmtsBtn_MouseMove);
            // 
            // bunifuSeparator2
            // 
            this.bunifuSeparator2.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.bunifuSeparator2.LineThickness = 1;
            this.bunifuSeparator2.Location = new System.Drawing.Point(171, 84);
            this.bunifuSeparator2.Name = "bunifuSeparator2";
            this.bunifuSeparator2.Size = new System.Drawing.Size(125, 10);
            this.bunifuSeparator2.TabIndex = 37;
            this.bunifuSeparator2.Transparency = 255;
            this.bunifuSeparator2.Vertical = false;
            // 
            // bunifuSeparator3
            // 
            this.bunifuSeparator3.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator3.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.bunifuSeparator3.LineThickness = 1;
            this.bunifuSeparator3.Location = new System.Drawing.Point(290, 38);
            this.bunifuSeparator3.Name = "bunifuSeparator3";
            this.bunifuSeparator3.Size = new System.Drawing.Size(10, 51);
            this.bunifuSeparator3.TabIndex = 38;
            this.bunifuSeparator3.Transparency = 255;
            this.bunifuSeparator3.Vertical = true;
            // 
            // bunifuSeparator4
            // 
            this.bunifuSeparator4.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator4.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.bunifuSeparator4.LineThickness = 1;
            this.bunifuSeparator4.Location = new System.Drawing.Point(166, 38);
            this.bunifuSeparator4.Name = "bunifuSeparator4";
            this.bunifuSeparator4.Size = new System.Drawing.Size(10, 51);
            this.bunifuSeparator4.TabIndex = 39;
            this.bunifuSeparator4.Transparency = 255;
            this.bunifuSeparator4.Vertical = true;
            // 
            // lobbyBtn
            // 
            this.lobbyBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(25)))), ((int)(((byte)(80)))));
            this.lobbyBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lobbyBtn.Image = ((System.Drawing.Image)(resources.GetObject("lobbyBtn.Image")));
            this.lobbyBtn.ImageActive = null;
            this.lobbyBtn.Location = new System.Drawing.Point(322, 198);
            this.lobbyBtn.Name = "lobbyBtn";
            this.lobbyBtn.Size = new System.Drawing.Size(49, 40);
            this.lobbyBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.lobbyBtn.TabIndex = 40;
            this.lobbyBtn.TabStop = false;
            this.lobbyBtn.Zoom = 10;
            this.lobbyBtn.Click += new System.EventHandler(this.lobbyBtn_Click);
            this.lobbyBtn.MouseLeave += new System.EventHandler(this.lobbyBtn_MouseLeave);
            this.lobbyBtn.MouseHover += new System.EventHandler(this.lobbyBtn_MouseHover);
            this.lobbyBtn.MouseMove += new System.Windows.Forms.MouseEventHandler(this.lobbyBtn_MouseMove);
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(49)))), ((int)(((byte)(60)))));
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(451, 280);
            this.Controls.Add(this.lobbyBtn);
            this.Controls.Add(this.bunifuSeparator4);
            this.Controls.Add(this.bunifuSeparator3);
            this.Controls.Add(this.bunifuSeparator2);
            this.Controls.Add(this.tournmtsBtn);
            this.Controls.Add(this.bunifuCustomLabel5);
            this.Controls.Add(this.carefulModeCheckBox);
            this.Controls.Add(this.searchPlayerTextBox);
            this.Controls.Add(this.bunifuCustomLabel2);
            this.Controls.Add(this.searchPlayerCheckBox);
            this.Controls.Add(this.annulerBouton);
            this.Controls.Add(this.defaiteCompteur);
            this.Controls.Add(this.victoireCompteur);
            this.Controls.Add(this.bunifuCustomLabel4);
            this.Controls.Add(this.bunifuCustomLabel3);
            this.Controls.Add(this.bienvenueLabel);
            this.Controls.Add(this.bunifuCustomLabel1);
            this.Controls.Add(this.inscriptionBouton);
            this.Controls.Add(this.formMobile);
            this.Controls.Add(this.boutonReduire);
            this.Controls.Add(this.bunifuSeparator1);
            this.Controls.Add(this.boutonFermer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Menu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Page d\'utilisateur";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Menu_FormClosing);
            this.Load += new System.EventHandler(this.Menu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.formMobile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.boutonReduire)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.boutonFermer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tournmtsBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lobbyBtn)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

        private System.Windows.Forms.PictureBox formMobile;
        private System.Windows.Forms.PictureBox boutonReduire;
        private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator1;
        private System.Windows.Forms.PictureBox boutonFermer;
        private Bunifu.Framework.UI.BunifuFlatButton annulerBouton;
        private Bunifu.Framework.UI.BunifuCustomLabel defaiteCompteur;
        private Bunifu.Framework.UI.BunifuCustomLabel victoireCompteur;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel4;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel3;
        private Bunifu.Framework.UI.BunifuCustomLabel bienvenueLabel;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
        private Bunifu.Framework.UI.BunifuFlatButton inscriptionBouton;
        private Bunifu.Framework.UI.BunifuCheckbox searchPlayerCheckBox;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel2;
        private Bunifu.Framework.UI.BunifuTextbox searchPlayerTextBox;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel5;
        private Bunifu.Framework.UI.BunifuCheckbox carefulModeCheckBox;
        private Bunifu.Framework.UI.BunifuImageButton tournmtsBtn;
        private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator2;
        private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator3;
        private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator4;
        private Bunifu.Framework.UI.BunifuImageButton lobbyBtn;
    }
}