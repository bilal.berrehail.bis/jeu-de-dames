﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;

namespace Client_pour_Jeu_de_dames
{
	public class TraitementDuClient
	{
        // --------- OBJETS ---------
        public PageDeConnexion maPageDeConnexion; // Objet de la Form "PageDeConnexion"
        public Menu monMenu; // Objet de la Form "Menu"
        public PlateauDeJeu monPlateau; // Objet de la Form "PlateauDeJeu"
        public ListeTournois mesTournois; // Objet de la Form "ListeTournois"
        public Lobby monLobby; // Objet de la Form "Lobby"
        // --------- OBJETS ---------

        // --------- VARIABLES ---------
        bool enTrainDeJouer = false; // Le joueur est en train de jouer ou non

        Socket monSocket; // Socket du client
        byte[] buf = new byte[1024]; // Tableau de bytes permettant la communication entre le serveur et ce client
        StringBuilder sb = new StringBuilder(); // Récuperation des paquets reçus par le serveur

        string pseudo; // Pseudo du joueur
        string password; // Mot de passe du joueur
        
        public bool playerTop; // Définit la position du joueur dans la partie
        // --------- VARIABLES ---------

        // Constructeur
        public TraitementDuClient(PageDeConnexion _maPageDeConnexion, Menu _monMenu, PlateauDeJeu _monPlateau, ListeTournois _mesTournois, Lobby _monLobby)
        {
            maPageDeConnexion = _maPageDeConnexion;
            monMenu = _monMenu;
            monPlateau = _monPlateau;
            mesTournois = _mesTournois;
            monLobby = _monLobby;

            monMenu.restoreObjects(maPageDeConnexion, monPlateau, mesTournois, monLobby, this);
            monPlateau.restoreObjects(maPageDeConnexion, monMenu, mesTournois, monLobby, this);
            mesTournois.restoreObjects(maPageDeConnexion, monMenu, monPlateau, monLobby, this);
            monLobby.restoreObjects(maPageDeConnexion, monMenu, monPlateau, mesTournois, this);
        }
        
        // Connexion au serveur
        public bool connectServer(string _pseudo, string _password)
        {
            pseudo = _pseudo;
            password = _password;

            IPAddress ipAddress = IPAddress.Parse(Program.getIp());
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, Program.getPort());

            monSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            try
            {
                monSocket.BeginConnect(localEndPoint, new AsyncCallback(connexionConnectCallback), monSocket);
                return true;
            }
            catch (SocketException ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }

        // Effectue la connexion au serveur
        public void connexionConnectCallback(IAsyncResult asyncResult)
        {
            try
            {
                monSocket.EndConnect(asyncResult);

                send("addClient " + pseudo + " " + password);
  
                monSocket.BeginReceive(buf, 0, buf.Length, SocketFlags.None, new AsyncCallback(receiveCallback), monSocket);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                MessageBox.Show("La connexion au serveur a échoué");
            }
        }
        
        // Récuperation d'un paquet envoyé par le serveur
        public void receiveCallback(IAsyncResult asyncResult)
        {
            try
            {
                string content = ""; 

                int read = monSocket.EndReceive(asyncResult);

                if (read > 0)
                {
                    sb.Append(Encoding.UTF8.GetString(buf, 0, read));

                    content = sb.ToString();
                    
                    traitementDesPacketsFilter(content);

                    sb = new StringBuilder();

                    monSocket.BeginReceive(buf, 0, buf.Length, SocketFlags.None, new AsyncCallback(receiveCallback), monSocket);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                MessageBox.Show("Le serveur vient de se déconnecter");
                Environment.Exit(1);
            }
        }

        // Envoi d'un paquet au serveur
        public void send(string data)
        {
            try
            {
                byte[] byteData = Encoding.UTF8.GetBytes(data);

                monSocket.BeginSend(byteData, 0, byteData.Length, SocketFlags.None, new AsyncCallback(sendCallback), monSocket);
            }
            catch (Exception ex)
            { 
            }
        }

        // Effectue l'envoi d'un paquet au serveur
        public void sendCallback(IAsyncResult asyncResult)
        {
            try
            {
                int send = monSocket.EndSend(asyncResult); // Envoi du packet au serveur
            }
            catch (Exception ex)
            {
            }
        }

        // Sépare les paquets récuperés par le serveur afin de les traiter séparement
        public void traitementDesPacketsFilter(string packet)
        {
            string[] packetNormalized = packet.Split(Convert.ToChar(0x01));
            for (int i = 0; i < packetNormalized.Length; i++)
            {
                if (!String.IsNullOrEmpty(packetNormalized[i]))
                {
                    traitementDesPackets(packetNormalized[i]);
                }
            }
        }

        // Traitement des paquets reçus par le serveur
        public void traitementDesPackets(string packet)
        {
            string[] packetATraiter = packet.Split(' ');
            int packetTaille = packetATraiter.Length;

            if (packetATraiter[0] == "verify_account" && packetTaille == 2 && !enTrainDeJouer)
            {
                if (FonctionsSecondaires.estUnNombre(packetATraiter[1]))
                {
                    int result = FonctionsSecondaires.s_int(packetATraiter[1]);
                    maPageDeConnexion.Invoke((MethodInvoker)delegate ()
                    {
                        maPageDeConnexion.verifyAccount(result);
                    });
                }
            }
            else if (packetATraiter[0] == "score" && packetTaille == 3)
            {
                if (FonctionsSecondaires.estUnNombre(packetATraiter[1]) && FonctionsSecondaires.estUnNombre(packetATraiter[2]))
                {
                    int victoireCount = FonctionsSecondaires.s_int(packetATraiter[1]);
                    int defaiteCount = FonctionsSecondaires.s_int(packetATraiter[2]);

                    monMenu.refreshScore(victoireCount, defaiteCount);
                }
            }
            else if (packetATraiter[0] == "inscription_success" && packetTaille == 2)
            {
                if (FonctionsSecondaires.estUnNombre(packetATraiter[1]))
                {
                    int successCode = FonctionsSecondaires.s_int(packetATraiter[1]);
                    monMenu.inscription_Success(successCode);
                }
            }
            else if (packetATraiter[0] == "match" && packetTaille == 3 && !enTrainDeJouer) // Match trouvé, on affiche le plateau de jeu
            {
                if (FonctionsSecondaires.estUnNombre(packetATraiter[2]))
                {
                    string pseudoOpponent = packetATraiter[1];
                    int recupererPlayerTop = FonctionsSecondaires.s_int(packetATraiter[2]);
                    playerTop = Convert.ToBoolean(recupererPlayerTop);
                    
                    monMenu.Hide();
                    monLobby.Hide();
                    mesTournois.Hide();
                    
                    monMenu.Invoke((MethodInvoker)delegate ()
                    {
                        monPlateau.miseAZeroPictureBox();
                        monPlateau.Show();
                    });

                    string ajouterMessage = "C'est parti ! Contre : " + pseudoOpponent;
                    monPlateau.addMessageChatBox(ajouterMessage, 1);

                    monPlateau.changeEnablePanel(true);

                    enTrainDeJouer = true;
                }
            }
            else if (packetATraiter[0] == "infos_player" && packetTaille == 6 && enTrainDeJouer)
            {
                if (FonctionsSecondaires.estUnNombre(packetATraiter[2]) && FonctionsSecondaires.estUnNombre(packetATraiter[3]))
                {
                    string pseudoPlayer = packetATraiter[1];
                    int victoryCount = FonctionsSecondaires.s_int(packetATraiter[2]);
                    int defeatCount = FonctionsSecondaires.s_int(packetATraiter[3]);
                    string dateCreated = packetATraiter[4] + " " + packetATraiter[5];

                    Form.ActiveForm.Invoke((MethodInvoker)delegate ()
                    {
                        monPlateau.openInfoPlayers(pseudoPlayer, victoryCount, defeatCount, dateCreated);
                    });
                }
            }
            else if (packetATraiter[0] == "msg" && packetTaille >= 3 && enTrainDeJouer)
            {
                if (FonctionsSecondaires.estUnNombre(packetATraiter[1]))
                {
                    int message_id = FonctionsSecondaires.s_int(packetATraiter[1]);

                    StringBuilder message = new StringBuilder();
                    for (int i = 2; i < packetATraiter.Length; i++)
                    {
                        message.Append(packetATraiter[i]);
                        message.Append(" ");
                    }

                    monPlateau.addMessageChatBox(message.ToString(), message_id);
                }
            }
            else if (packetATraiter[0] == "box" && packetTaille >= 4 && !enTrainDeJouer)
            {
                if (FonctionsSecondaires.estUnNombre(packetATraiter[1]) && FonctionsSecondaires.estUnNombre(packetATraiter[2]))
                {
                    int bt = FonctionsSecondaires.s_int(packetATraiter[1]);
                    int ic = FonctionsSecondaires.s_int(packetATraiter[2]);

                    StringBuilder message = new StringBuilder();

                    for (int i = 3; i < packetTaille; i++)
                    {
                        message.Append(packetATraiter[i]);
                        message.Append(" ");
                    }

                    message = message.Replace("@", Environment.NewLine);

                    MessageBox.Show(message.ToString(), "", (MessageBoxButtons)bt, (MessageBoxIcon)ic); // Affichage de la boite de dialogue
                }
            }
            else if (packetATraiter[0] == "set" && packetTaille == 6 && enTrainDeJouer)
            {
                if (FonctionsSecondaires.estUnNombre(packetATraiter[1]) && FonctionsSecondaires.estUnNombre(packetATraiter[2]) &&
                  FonctionsSecondaires.estUnNombre(packetATraiter[3]) && FonctionsSecondaires.estUnNombre(packetATraiter[4]))
                {
                    int x = FonctionsSecondaires.s_int(packetATraiter[1]);
                    int y = FonctionsSecondaires.s_int(packetATraiter[2]);
                    int isExist = FonctionsSecondaires.s_int(packetATraiter[3]);
                    int isTop = FonctionsSecondaires.s_int(packetATraiter[4]);

                    int imgId = FonctionsSecondaires.s_int(packetATraiter[5]);

                    string imgPawn = Program.getPionNameById(imgId);

                    monPlateau.modificationDesCases(x, y, imgPawn, Convert.ToBoolean(isExist), Convert.ToBoolean(isTop));
                }

            }
            else if (packetATraiter[0] == "req" && packetTaille >= 7 && enTrainDeJouer)
            {
                if (FonctionsSecondaires.estUnNombre(packetATraiter[1]) && FonctionsSecondaires.estUnNombre(packetATraiter[2]) && FonctionsSecondaires.estUnNombre(packetATraiter[3])
                && FonctionsSecondaires.estUnNombre(packetATraiter[4]) && FonctionsSecondaires.estUnNombre(packetATraiter[5]))
                {
                    int type = FonctionsSecondaires.s_int(packetATraiter[1]);
                    int bt = FonctionsSecondaires.s_int(packetATraiter[2]);
                    int ic = FonctionsSecondaires.s_int(packetATraiter[3]);
                    int x = FonctionsSecondaires.s_int(packetATraiter[4]);
                    int y = FonctionsSecondaires.s_int(packetATraiter[5]);

                    StringBuilder message = new StringBuilder();

                    for (int i = 6; i < packetTaille; i++)
                    {
                        message.Append(packetATraiter[i]);
                        message.Append(" ");
                    }

                    DialogResult result = MessageBox.Show(message.ToString(), "", (MessageBoxButtons)bt, (MessageBoxIcon)ic); // Affichage de la boite de dialogue

                    if (result == DialogResult.Yes) // Si le client a cliqué sur "Oui", la réponse est envoyé au serveur
                    {
                        string reqResult = "req_result 1 " + x + " " + y;
                        send(reqResult);
                    }
                    else // Si le client a cliqué sur "Non" ou sur la croix rouge, la réponse est envoyé au serveur mais le packet est différent
                    {
                        send("req_result 0 -1 -1");
                    }
                }
            }
            else if (packetATraiter[0] == "msg_final" && packetTaille >= 2 && enTrainDeJouer) // Réception du message de fin de partie
            {
                enTrainDeJouer = false;

                StringBuilder message = new StringBuilder();

                for (int i = 1; i < packetTaille; i++)
                {
                    message.Append(packetATraiter[i]);
                    message.Append(" ");
                }

                MessageBox.Show(message.ToString());

                fermetureDuPlateau();
            }
            else if (packetATraiter[0] == "list_tournmts" && packetTaille >= 10 && !enTrainDeJouer)
            {
                mesTournois.afficherTournois(packet);
            }
            else if (packetATraiter[0] == "list_tournmts" && packetTaille == 1 & !enTrainDeJouer)
            {
                mesTournois.clearTournmtsControls();
            }
            else if (packetATraiter[0] == "response_inscription_tournmt" && packetTaille == 2 && !enTrainDeJouer)
            {
                if (FonctionsSecondaires.estUnNombre(packetATraiter[1]))
                {
                    int response_id = FonctionsSecondaires.s_int(packetATraiter[1]);
                    mesTournois.responseInscriptionTournmt(response_id);
                }
            }
            else if (packetATraiter[0] == "desinscription_tournmt_ok" && packetTaille == 1 && !enTrainDeJouer)
            {
                mesTournois.desinscription_success();
            }
            else if (packetATraiter[0] == "list_tournmt_lobby" && packetTaille >= 9 && !enTrainDeJouer)
            {
                if (FonctionsSecondaires.estUnNombre(packetATraiter[1]) && FonctionsSecondaires.estUnNombre(packetATraiter[2]) &&
                    FonctionsSecondaires.estUnNombre(packetATraiter[3]))
                {
                    if (!monLobby.IsDisposed)
                    {
                        monLobby.Show();
                    }

                    int tournmt_id = FonctionsSecondaires.s_int(packetATraiter[1]);

                    int state_lobby = FonctionsSecondaires.s_int(packetATraiter[2]);
                    int state_phase = FonctionsSecondaires.s_int(packetATraiter[3]);

                    string etat_tournmt = packetATraiter[4];

                    string nextPhaseStart = String.Format("{0} {1}", packetATraiter[5], packetATraiter[6]);
                    string dateStart = String.Format("{0} {1}", packetATraiter[7], packetATraiter[8]);

                    StringBuilder tournmt_name = new StringBuilder();
                    for (int i = 9; i < packetTaille; i++)
                    {
                        tournmt_name.Append(packetATraiter[i]);
                        tournmt_name.Append(" ");
                    }

                    if (monLobby.InvokeRequired)
                    {
                        monLobby.Invoke((MethodInvoker)delegate ()
                        {
                            monLobby.getFullTournmtLobby(tournmt_id, state_lobby, state_phase, etat_tournmt, tournmt_name.ToString(), dateStart, nextPhaseStart);
                        });
                    }else
                    {
                        monLobby.getFullTournmtLobby(tournmt_id, state_lobby, state_phase, etat_tournmt, tournmt_name.ToString(), dateStart, nextPhaseStart);
                    }
                  
                }
            }
            else if (packetATraiter[0] == "list_tournmt_lobby" && packetTaille == 1 && !enTrainDeJouer)
            {
                monLobby.clearTournmtLobby();
            }
		}
        
        // Fermeture du plateau de jeu
        public void fermetureDuPlateau()
		{
            send("get_score");

            monMenu.reinitialiserLaRecherche();
            monMenu.Show();
			
            monPlateau.clearChatBox();

            //monPlateau.miseAZeroPictureBox(); // On remet en place les pions dans les cases

            monPlateau.Hide();
		}

	}
}
