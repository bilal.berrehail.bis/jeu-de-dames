﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Client_pour_Jeu_de_dames
{
	class RecupererCoordonnesCurseur
	{
        // Récupère la position du curseur en fonction du control
        public static Control findControlAtPoint(Control container, Point pos)
		{
			Control child;
			foreach (Control c in container.Controls)
			{
				if (c.Visible && c.Bounds.Contains(pos))
				{
                    child = findControlAtPoint(c, new Point(pos.X - c.Left, pos.Y - c.Top));
					if (child == null) return c;
					else return child;
				}
			}
			return null;
		}

        // Récupère la position du curseur en fonction d'une form
        public static Control findControlAtCursor(Form form)
		{
			Point pos = Cursor.Position;
			if (form.Bounds.Contains(pos))
                return findControlAtPoint(form, form.PointToClient(pos));
			return null;
		}
	}
}
