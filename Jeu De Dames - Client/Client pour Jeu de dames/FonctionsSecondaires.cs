﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client_pour_Jeu_de_dames
{
	class FonctionsSecondaires
	{
        // Conversion tableau de bytes en chaine de caractères
        public static string b_str(byte[] buffer, int packetlength = -1)
		{
			if (packetlength != -1)
			{
				return System.Text.Encoding.UTF8.GetString(buffer).Substring(0, packetlength);
			}
			return System.Text.Encoding.UTF8.GetString(buffer);
		}

        // Conversion chaine de caractères en tableau de bytes
        public static byte[] s_byte(string packet)
		{
			return System.Text.Encoding.UTF8.GetBytes(packet);
		}

        // Conversion chaine de caractères en nombre
        public static int s_int(string str)
        {
            return Convert.ToInt32(str);
        }

        // Conversion nombre ne chaine de caractères
        public static string i_str(int value)
        {
            return Convert.ToString(value);
        }

        // Check si la chaine de caractère contient une décimale
        public static bool estUnNombre(string value)
		{
			int nb;
			return int.TryParse(value, out nb);
		}
	}
}
