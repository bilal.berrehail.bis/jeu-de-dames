**Projet personnel.**

Création d'un jeu de dames en ligne avec système de tournois.

Les règles fondamentales du jeu de dames sont toutes respectées.

Le projet a été développé en C#.

* Système de chat lors d'une partie
* Possibilité de consulter les informations de son adversaire
* Personnalisation de son mode de jeu
* Recherche d'un joueur par son pseudo pour provoquer une rencontre amicale
* Système de création / inscription de tournois
* Système de phase de tournois avec date et heure de rendez-vous
* Fichier de configuration pour le paramétrage des parties :

Paramètres configurables :
```
1. Nombre de secondes par tour
2. Nombre de tours sautés avant disqualification
3. Nombre de secondes entre chaque message pour éviter le spam
```


**Développé par : BERREHAIL Bilal**