/*
Navicat MySQL Data Transfer

Source Server         : Pakrologie
Source Server Version : 50719
Source Host           : 127.0.0.1:3306
Source Database       : jeu_de_dame

Target Server Type    : MYSQL
Target Server Version : 50719
File Encoding         : 65001

Date: 2018-04-26 15:55:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for table_accounts
-- ----------------------------
DROP TABLE IF EXISTS `table_accounts`;
CREATE TABLE `table_accounts` (
  `AccountID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `AccountName` varchar(12) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `AccountPassword` varchar(12) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `IsAdmin` tinyint(1) DEFAULT '0',
  `IsOnline` tinyint(1) unsigned DEFAULT NULL,
  `CreationDate` datetime NOT NULL,
  PRIMARY KEY (`AccountID`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of table_accounts
-- ----------------------------

-- ----------------------------
-- Table structure for table_characters
-- ----------------------------
DROP TABLE IF EXISTS `table_characters`;
CREATE TABLE `table_characters` (
  `CharacterID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fkAccountID` int(10) unsigned NOT NULL,
  `fkGameID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`CharacterID`),
  KEY `fkAccountID` (`fkAccountID`),
  KEY `fkGameID` (`fkGameID`),
  CONSTRAINT `table_characters_ibfk_1` FOREIGN KEY (`fkAccountID`) REFERENCES `table_accounts` (`AccountID`),
  CONSTRAINT `table_characters_ibfk_2` FOREIGN KEY (`fkGameID`) REFERENCES `table_games` (`GameID`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of table_characters
-- ----------------------------

-- ----------------------------
-- Table structure for table_games
-- ----------------------------
DROP TABLE IF EXISTS `table_games`;
CREATE TABLE `table_games` (
  `GameID` int(10) unsigned NOT NULL,
  `GameName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`GameID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of table_games
-- ----------------------------
INSERT INTO `table_games` VALUES ('1', 'Jeu de dames');
INSERT INTO `table_games` VALUES ('2', 'Morpion');

-- ----------------------------
-- Table structure for table_infos_tournmt
-- ----------------------------
DROP TABLE IF EXISTS `table_infos_tournmt`;
CREATE TABLE `table_infos_tournmt` (
  `InfosTournmtID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fkCharacterID` int(10) unsigned NOT NULL,
  `fkTournmtID` int(10) unsigned NOT NULL,
  `CurrentPhase` int(10) unsigned NOT NULL,
  `CandidateNumber` int(2) unsigned NOT NULL,
  PRIMARY KEY (`InfosTournmtID`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of table_infos_tournmt
-- ----------------------------

-- ----------------------------
-- Table structure for table_register
-- ----------------------------
DROP TABLE IF EXISTS `table_register`;
CREATE TABLE `table_register` (
  `RegisterID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fkTournmtID` int(10) unsigned NOT NULL,
  `fkCharacterID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`RegisterID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of table_register
-- ----------------------------

-- ----------------------------
-- Table structure for table_results
-- ----------------------------
DROP TABLE IF EXISTS `table_results`;
CREATE TABLE `table_results` (
  `MatchID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fkWinnerCharacterID` int(10) unsigned NOT NULL,
  `fkLoserCharacterID` int(10) unsigned NOT NULL,
  `IsForfeit` tinyint(1) unsigned DEFAULT NULL,
  `DateMatch` datetime NOT NULL,
  PRIMARY KEY (`MatchID`),
  KEY `fkWinnerCharacterID` (`fkWinnerCharacterID`),
  KEY `fkLoserCharacterID` (`fkLoserCharacterID`),
  CONSTRAINT `table_results_ibfk_1` FOREIGN KEY (`fkWinnerCharacterID`) REFERENCES `table_characters` (`CharacterID`),
  CONSTRAINT `table_results_ibfk_2` FOREIGN KEY (`fkLoserCharacterID`) REFERENCES `table_characters` (`CharacterID`)
) ENGINE=InnoDB AUTO_INCREMENT=169 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of table_results
-- ----------------------------

-- ----------------------------
-- Table structure for table_tournmts
-- ----------------------------
DROP TABLE IF EXISTS `table_tournmts`;
CREATE TABLE `table_tournmts` (
  `TournmtID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TournmtName` varchar(10) COLLATE utf8_bin NOT NULL,
  `MaxPlayers` int(10) unsigned NOT NULL,
  `DeadLineReg` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `StartDate` datetime NOT NULL,
  PRIMARY KEY (`TournmtID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of table_tournmts
-- ----------------------------
