﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeu_De_Dame___Serveur
{
    class Lobby
    {
        public static List<structTournoi> listTournoi = new List<structTournoi>(); // Liste des tournois qui n'ont pas encore débutés

        public static List<structTournoi> listTournoiInProgress = new List<structTournoi>(); // Liste des tournois ayant commencés

        // Structure dédié au tournoi
        public struct structTournoi
        {
            public int tournmt_id;
            public int nb_inscrits;
            public int max_players;
            public string nom_tournoi;
            public string deadline_reg;
            public string date_start;

            public int currentPhase;
            public bool isPhaseStart;

            public DateTime phaseStart;

            public List<structTournoiCharacter> characters;
        }

        // Structure dédié aux inscrits au tournoi
        public struct structTournoiCharacter
        {
            public Client client;
            public int victoryCount;
            public bool iseliminated;
            public int currentPhase;
            public bool isconnected;
        }

        // Traitement des tournois
        public static void checkLobbies()
        {
            while (true)
            {
                listTournoi = fillListTournoi();

                for (int i = 0; i < listTournoi.Count; i++)
                {
                    lobbyHandler(i);
                }

                for (int i = 0; i < listTournoiInProgress.Count; i++)
                {
                    tournmtInProgressHandler(i);
                }

                testAffichage();
                System.Threading.Thread.Sleep(2500);
            }
        }

        // Affichage en mode console les informations des tournois
        public static void testAffichage()
        {
            for (int i = 0; i < listTournoi.Count; i++)
            {
                Console.WriteLine(String.Format("-- listTournoi[{0}] --", i));
                Console.WriteLine(String.Format("Tournmt_ID : {0}", listTournoi[i].tournmt_id));

                for (int e = 0; e < listTournoi[i].characters.Count; e++)
                {
                    Console.WriteLine(String.Format("-- listTournoi[{0}].characters[{1}] --", i, e));
                    Console.WriteLine(String.Format("ClientName : {0}", listTournoi[i].characters[e].client.info_main.pseudo));
                }
            }
            for (int i = 0; i < listTournoiInProgress.Count; i++)
            {
                Console.WriteLine(String.Format("-- listTournoiInProgress[{0}] --", i));
                Console.WriteLine(String.Format("Tournmt_ID : {0}", listTournoiInProgress[i].tournmt_id));

                for (int e = 0; e < listTournoiInProgress[i].characters.Count; e++)
                {
                    Console.WriteLine(String.Format("-- listTournoiInProgress[{0}].characters[{1}] --", i, e));
                    Console.WriteLine(String.Format("ClientName : {0} - Connected : {1}", listTournoiInProgress[i].characters[e].client.info_main.pseudo,
                        listTournoiInProgress[i].characters[e].isconnected));
                }
            }
        }
        
        // Récuperation des tournois depuis la base de données
        public static List<structTournoi> fillListTournoi()
        {
            string packet_tournmts = RequeteSQL.recupererTournmts();
            
            if (packet_tournmts.Length == 13)
            {
                return new List<structTournoi>();
            }

            packet_tournmts = packet_tournmts.Remove(0, packet_tournmts.Split(' ')[0].Length + 1);

            string[] soloTournoi = packet_tournmts.Split('|');
            
            for (int i = 0; i < soloTournoi.Length; i++)
            {
                string[] infos_tournoi = soloTournoi[i].Split(' ');

                if (infos_tournoi.Length >= 8)
                {
                    if (More.isDec(infos_tournoi[0]) &&
                    More.isDec(infos_tournoi[1]) &&
                    More.isDec(infos_tournoi[2]))
                    {
                        structTournoi nouveauTournoi = new structTournoi();
                        nouveauTournoi.tournmt_id = Convert.ToInt32(infos_tournoi[0]);

                        if (!isTournoiInProgress(nouveauTournoi.tournmt_id)) // Si le tournoi n'a pas encore commencé
                        {
                            int indexTournmt = getTournmtIndex(nouveauTournoi.tournmt_id);

                            if (indexTournmt == -1) // Si le tournoi n'a pas encore été créer
                            {
                                nouveauTournoi.nb_inscrits = Convert.ToInt32(infos_tournoi[1]);
                                nouveauTournoi.max_players = Convert.ToInt32(infos_tournoi[2]);
                                
                                string fusion_deadline = String.Format("{0} {1}", infos_tournoi[3], infos_tournoi[4]);
                                string fusion_startdate = String.Format("{0} {1}", infos_tournoi[5], infos_tournoi[6]);

                                nouveauTournoi.deadline_reg = fusion_deadline;
                                nouveauTournoi.date_start = fusion_startdate;

                                StringBuilder nom_tournoi = new StringBuilder();

                                for (int e = 7; e < infos_tournoi.Length; e++)
                                {
                                    nom_tournoi.Append(infos_tournoi[e]);
                                    if (e != infos_tournoi.Length - 1)
                                    {
                                        nom_tournoi.Append(" ");
                                    }
                                }

                                nouveauTournoi.nom_tournoi = nom_tournoi.ToString();

                                nouveauTournoi.characters = new List<structTournoiCharacter>();

                                listTournoi.Add(nouveauTournoi);
                            }
                        }
                    }
                }
            }

            return listTournoi;
        }

        // Traitement des tournois qui n'ont pas encore débutés
        public static void lobbyHandler(int indexLobby)
        {
            DateTime currentDate = DateTime.Now;
            DateTime tournoiDate = Convert.ToDateTime(listTournoi[indexLobby].date_start);
            
            if (!isTournoiInProgress(listTournoi[indexLobby].tournmt_id))
            {
                if (currentDate >= tournoiDate)
                {
                    initializeTournmtBeforeStart(indexLobby);
                }
            }
        }

        // Traitement des tournois ayant commencés
        public static void tournmtInProgressHandler(int indexTournmt)
        {
            DateTime currentDate = DateTime.Now;
            DateTime phaseStart = listTournoiInProgress[indexTournmt].phaseStart;

            if (!listTournoiInProgress[indexTournmt].isPhaseStart)
            {
                if (currentDate >= phaseStart)
                {
                    int nbPlayersConnected = 0;

                    for (int i = 0; i < listTournoiInProgress[indexTournmt].characters.Count; i++)
                    {
                        if (listTournoiInProgress[indexTournmt].characters[i].isconnected &&
                            listTournoiInProgress[indexTournmt].characters[i].currentPhase == listTournoiInProgress[indexTournmt].currentPhase &&
                            !listTournoiInProgress[indexTournmt].characters[i].iseliminated)
                        {
                            nbPlayersConnected++;
                        }
                        else
                        {
                            Client playerEliminated = listTournoiInProgress[indexTournmt].characters[i].client;
                            listTournoiInProgress[indexTournmt].characters.RemoveAt(i);

                            playerEliminated.sendMsgBox("Vous avez été éliminé du tournoi !", System.Windows.Forms.MessageBoxButtons.OK,
                                    System.Windows.Forms.MessageBoxIcon.Warning);

                            refreshInformationLobby(playerEliminated);

                            i--;
                        }
                    }

                    Console.WriteLine("Debut de la première phase pour le tournoi {0} : Nombre de participants connectés : {1}", listTournoiInProgress[indexTournmt].tournmt_id,
                        nbPlayersConnected);

                    structTournoi refreshTournoi = listTournoiInProgress[indexTournmt];
                    refreshTournoi.isPhaseStart = true;

                    listTournoiInProgress[indexTournmt] = refreshTournoi;

                    nextPhaseHandler(indexTournmt);
                }
            }
            else
            {
                if (isCharactersAllFinishedPlaying(listTournoiInProgress[indexTournmt].tournmt_id))
                {
                    structTournoi st = listTournoiInProgress[indexTournmt];

                    if (st.characters.Count == 1)
                    {
                        nextPhaseHandler(indexTournmt);
                        return;
                    }

                    st.currentPhase++;
                    st.phaseStart = DateTime.Now.AddMinutes(1);
                    st.isPhaseStart = false;

                    listTournoiInProgress[indexTournmt] = st;

                    for (int i = 0; i < listTournoiInProgress[indexTournmt].characters.Count; i++)
                    {
                        string pseudoPlayer = listTournoiInProgress[indexTournmt].characters[i].client.info_main.pseudo;
                        int indexClient = ClientManager.byPseudo(pseudoPlayer);
                        if (indexClient != -1)
                        {
                            refreshInformationLobby(ClientManager.listClient[indexClient]);
                        }
                    }

                    Console.WriteLine("CHANGEMENT DE PHASES");

                    // Initialisation de l'arbre des rencontres
                    RequeteSQL.modificationInfosTournmt(indexTournmt);
                }
            }
        }

        // Réinitialisation de la variable de connexion des inscrits au tournoi
        public static void resetCharacterConnectedToFalse(int indexTournmt)
        {
            for (int i = 0; i < listTournoiInProgress[indexTournmt].characters.Count; i++)
            {
                Lobby.structTournoiCharacter stc = listTournoiInProgress[indexTournmt].characters[i];
                stc.isconnected = false;

                listTournoiInProgress[indexTournmt].characters[i] = stc;
            }
        }

        // Traitement des phases du tournoi
        public static void nextPhaseHandler(int indexTournmt)
        {
            int tournmt_id = listTournoiInProgress[indexTournmt].tournmt_id;
            int nbPlayers = listTournoiInProgress[indexTournmt].characters.Count;

            resetCharacterConnectedToFalse(indexTournmt);

            Console.WriteLine("nextPhaseHandler() : nbPlayers = " + nbPlayers);

            switch (nbPlayers)
            {
                case 0: // Tournoi supprimé sans vainqueur
                    deleteTournmtInProgress(tournmt_id);
                    deleteTournmt(tournmt_id);
                    Console.WriteLine("Pas de participants donc tournoi supprimé.");
                    break;
                case 1: // Tournoi terminé
                    Client playerVictory = listTournoiInProgress[indexTournmt].characters[0].client;
                    endTournmt(tournmt_id, playerVictory);
                    break;
            }
            if (nbPlayers >= 2 && nbPlayers <= 16)
            {
                meetPlayersForPhasing(indexTournmt, nbPlayers);
            }
        }

        // Tri des joueurs inscrits au tournoi et mise en relation des joueurs entre eux
        public static void meetPlayersForPhasing(int indexTournmt, int nbPlayers)
        {
            bool isOdd = (nbPlayers % 2 == 0) ? false : true;

            for (int i = 0; i < nbPlayers - (nbPlayers % 2); i += 2)
            {
                Client player1 = listTournoiInProgress[indexTournmt].characters[i].client;
                Client player2 = listTournoiInProgress[indexTournmt].characters[i + 1].client;
                meetPlayers(indexTournmt, player1, player2);
            }

            if (isOdd)
            {
                /*if (listTournoiInProgress[indexTournmt].currentPhase >= 2)
                {
                    structTournoiCharacter initializeCharacter = listTournoiInProgress[indexTournmt].characters[nbPlayers - 3];
                    initializeCharacter.currentPhase++;
                    initializeCharacter.victoryCount++;

                    listTournoiInProgress[indexTournmt].characters[nbPlayers - 3] = initializeCharacter;

                    Client player3 = listTournoiInProgress[indexTournmt].characters[nbPlayers - 3].client;

                    player3.sendMsgBox("Vous avez été qualifié d'office à la prochaine phase du tournoi.",
                    System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);

                    refreshInformationLobby(player3);
                }
                else
                {*/
                    structTournoiCharacter initializeCharacter = listTournoiInProgress[indexTournmt].characters[nbPlayers - 1];
                    initializeCharacter.currentPhase++;
                    initializeCharacter.victoryCount++;

                    listTournoiInProgress[indexTournmt].characters[nbPlayers - 1] = initializeCharacter;

                    Client player3 = listTournoiInProgress[indexTournmt].characters[nbPlayers - 1].client;

                    player3.sendMsgBox("Vous avez été qualifié d'office à la prochaine phase du tournoi.",
                    System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);

                    refreshInformationLobby(player3);
                //}
               
            }
        }

        // Annonce la fin du tournoi et suppression de celui-ci
        public static void endTournmt(int tournmt_id, Client playerVictory)
        {
            int indexLobby = getTournmtIndex(tournmt_id);
            if (indexLobby == -1)
            {
                return;
            }

            int indexTournmt = getTournmtInProgressIndex(tournmt_id);
            if (indexTournmt == -1)
            {
                return;
            }

            int victoryCount = listTournoiInProgress[indexTournmt].characters[0].victoryCount;
            string nomTournoi = listTournoiInProgress[indexTournmt].nom_tournoi;

            Console.WriteLine("Tournoi ({0}) terminé : Vainqueur {1}.", tournmt_id, listTournoiInProgress[indexTournmt].characters[0].client.info_main.pseudo);
            playerVictory.sendMsgBox(String.Format("Félicitation !@Vous avez remporté le tournoi {0} avec {1} victoires !", nomTournoi, victoryCount), System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
            
            deleteTournmtInProgress(tournmt_id);
            deleteTournmt(tournmt_id);

            refreshInformationLobby(playerVictory);
        }

        // Envoi de packets à tous les joueurs inscrits au tournoi
        public static void sendAllCharactersOnTournmt(int tournmt_id, string packet)
        {
            int indexTournmt = getTournmtInProgressIndex(tournmt_id);
            if (indexTournmt == -1)
            {
                return;
            }

            for (int i = 0; i < listTournoiInProgress[indexTournmt].characters.Count; i++)
            {
                listTournoiInProgress[indexTournmt].characters[i].client.send(packet);
            }
        }

        // Mise en relation de deux joueurs
        public static void meetPlayers(int indexTournmt, Client player1, Client player2)
        {
            int indexPlayer1 = -1;
            int indexPlayer2 = -1;

            for (int i = 0; i < listTournoiInProgress[indexTournmt].characters.Count; i++)
            {
                if (listTournoiInProgress[indexTournmt].characters[i].client == player1)
                {
                    indexPlayer1 = i;
                }
                else if (listTournoiInProgress[indexTournmt].characters[i].client == player2)
                {
                    indexPlayer2 = i;
                }
            }

            if (indexPlayer1 == -1 || indexPlayer2 == -1)
            {
                return;
            }
            
            indexPlayer1 = ClientManager.byPseudo(listTournoiInProgress[indexTournmt].characters[indexPlayer1].client.info_main.pseudo);
            indexPlayer2 = ClientManager.byPseudo(listTournoiInProgress[indexTournmt].characters[indexPlayer2].client.info_main.pseudo);

            if (indexPlayer1 == -1 || indexPlayer2 == -1)
            {
                return;
            }

            ClientManager.listClient[indexPlayer1].info_game.opponent = ClientManager.listClient[indexPlayer2].info_main.pseudo;
            ClientManager.listClient[indexPlayer2].info_game.opponent = ClientManager.listClient[indexPlayer1].info_main.pseudo;

            ClientManager.listClient[indexPlayer1].info_main.playerTop = true;
            ClientManager.listClient[indexPlayer2].info_main.playerTop = false;

            ClientManager.listClient[indexPlayer1].info_game.tour = true;
            ClientManager.listClient[indexPlayer2].info_game.tour = false;

            Match.startGame(indexPlayer1, indexPlayer2);
        }

        // Permet de savoir si tous les joueurs ont effectués leur phase de tournoi
        public static bool isCharactersAllFinishedPlaying(int tournmt_id)
        {
            int indexTournmt = getTournmtInProgressIndex(tournmt_id);
            if (indexTournmt == -1)
            {
                return false;
            }

            for (int i = 0; i < listTournoiInProgress[indexTournmt].characters.Count; i++)
            {
                if (listTournoiInProgress[indexTournmt].characters[i].client.info_game.isplaying)
                {
                    return false;
                }
            }
            
            return true;
        }

        // Actualisation des informations du tournoi à afficher sur le client de jeu
        public static void refreshInformationLobby(Client client)
        {
            string list_tournmt_lobby = RequeteSQL.recupererTournmtsRegistered(client);
            client.send(list_tournmt_lobby);
        }
        
        // Initialisation des variables de tournoi avant le début de celui-ci
        public static void initializeTournmtBeforeStart(int indexLobby)
        {
            structTournoi tournoiToStart = listTournoi[indexLobby];

            int nb_players = tournoiToStart.characters.Count;

            if (nb_players < 2) // TOOD : Remettre à 3
            {
                Console.WriteLine("Le tournoi {0} n'a pas pu commencer parce qu'il y a uniquement {1} participants", tournoiToStart.tournmt_id,
                    nb_players);

                RequeteSQL.deleteTournmt(tournoiToStart.tournmt_id);

                for (int i = 0; i < nb_players; i++)
                {
                    string pseudoPlayer = tournoiToStart.characters[i].client.info_main.pseudo;
                    int indexClient = ClientManager.byPseudo(pseudoPlayer);
                    if (indexClient != -1)
                    {
                        refreshInformationLobby(ClientManager.listClient[indexClient]);
                        ClientManager.listClient[indexClient].sendMsgBox(String.Format("Le tournoi n'a pas pu commencer parce qu'il y a uniquement {0} participants", nb_players),
                               System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Exclamation);
                    }
                }
                return;
            }
            else if (nb_players > 16)
            {
                Console.WriteLine("Une erreur est survenue : Trop de participants au tournoi {0}.", tournoiToStart.tournmt_id);
                
                RequeteSQL.deleteTournmt(tournoiToStart.tournmt_id);

                for (int i = 0; i < nb_players; i++)
                {
                    string pseudoPlayer = tournoiToStart.characters[i].client.info_main.pseudo;
                    int indexClient = ClientManager.byPseudo(pseudoPlayer);
                    if (indexClient != -1)
                    {
                        refreshInformationLobby(ClientManager.listClient[indexClient]);
                    }
                }
                return;
            }

            Console.WriteLine(String.Format("Le tournoi {0} vient de commencer avec le nombre de participants : {1}",
                        tournoiToStart.nom_tournoi, tournoiToStart.characters.Count));
            
            tournoiToStart.currentPhase = 1;
            tournoiToStart.phaseStart = DateTime.Now.AddMinutes(1);
            tournoiToStart.isPhaseStart = false;

            for (int i = 0; i < nb_players; i++)
            {
                string pseudoPlayer = tournoiToStart.characters[i].client.info_main.pseudo;
                int indexClient = ClientManager.byPseudo(pseudoPlayer);
                
                if (indexClient != -1)
                {
                    Console.WriteLine("Le joueur {0} a été initialisé.", pseudoPlayer);
                    ClientManager.listClient[indexClient].info_game.info_tournmt.isMatchTournmt = true;
                    ClientManager.listClient[indexClient].info_game.info_tournmt.tournmtId = tournoiToStart.tournmt_id;
                }
            }

            listTournoi[indexLobby] = tournoiToStart;

            listTournoiInProgress.Add(listTournoi[indexLobby]);
            
            for (int i = 0; i < nb_players; i++)
            {
                string pseudoPlayer = tournoiToStart.characters[i].client.info_main.pseudo;
                int indexClient = ClientManager.byPseudo(pseudoPlayer);
                if (indexClient != -1)
                {
                    refreshInformationLobby(ClientManager.listClient[indexClient]);
                }
            }
            
            // Mélange des rencontres et initialisation de l'arbre des rencontres
            int indexTournmt = getTournmtInProgressIndex(listTournoi[indexLobby].tournmt_id);

            if (indexTournmt != -1)
            {
                var rnd = new Random();
                List<structTournoiCharacter> mixCharacters = listTournoiInProgress[indexTournmt].characters.OrderBy(item => rnd.Next()).ToList();

                for (int i = 0; i < listTournoiInProgress[indexTournmt].characters.Count; i++)
                {
                    listTournoiInProgress[indexTournmt].characters[i] = mixCharacters[i];
                }

                RequeteSQL.modificationInfosTournmt(indexTournmt);
            }

            Console.WriteLine("Tournoi ajouté à la liste");
        }

        // Déconnexion d'un joueur au lobby de tournoi
        public static bool disconnectLobby(Client isPlaying)
        {
            bool isMatchTournmt = isPlaying.info_game.info_tournmt.isMatchTournmt;

            if (isMatchTournmt)
            {
                bool isdisconnectedInProgress = disconnectTournoiInProgressClient(isPlaying);

                if (isdisconnectedInProgress)
                {
                    isPlaying.info_game.info_tournmt.isMatchTournmt = false;
                    Console.WriteLine("Le joueur {0} s'est déconnecté d'un tournoi en cours ! Il a donc été éliminé.", isPlaying.info_main.pseudo);
                    return true;
                }
                return false;
            }
            
            bool isdisconnected = disconnectTournoiClient(isPlaying);

            if (isdisconnected)
            {
                Console.WriteLine("Le joueur {0} s'est déconnecté du Lobby d'un tournoi !", isPlaying.info_main.pseudo);
                return true;
            }
            return false;
        }

        // Déconnexion d'un joueur à une phase de tournoi
        public static bool disconnectPhaseTournmtInProgress(Client isPlaying)
        {
            int character_id = isPlaying.info_main.character_id;

            for (int i = 0; i < listTournoiInProgress.Count; i++)
            {
                for (int e = 0; e < listTournoiInProgress[i].characters.Count; e++)
                {
                    if (listTournoiInProgress[i].characters[e].client.info_main.character_id == character_id &&
                        listTournoiInProgress[i].characters[e].isconnected)
                    {
                        structTournoiCharacter refreshCharacter = listTournoiInProgress[i].characters[e];
                        refreshCharacter.isconnected = false;

                        listTournoiInProgress[i].characters[e] = refreshCharacter;
                        return true;
                    }
                }
            }
            return false;
        }

        // Connexion d'un joueur au lobby
        public static bool connectLobby(Client isPlaying, int tournmt_id)
        {
            int character_id = isPlaying.info_main.character_id;

            structTournoiCharacter nouveauCharacter = new structTournoiCharacter();
            nouveauCharacter.client = isPlaying;
            nouveauCharacter.victoryCount = 0;
            nouveauCharacter.iseliminated = false;
            nouveauCharacter.currentPhase = 1;
            nouveauCharacter.isconnected = false;

            int indexTournmt = getTournmtIndex(tournmt_id);
            
            if (indexTournmt == -1) // Si le tournoi n'existe pas
            {
                return false;
            }

            bool isRegisteredTournmt = RequeteSQL.isRegisteredInTournmt(isPlaying, tournmt_id);

            if (!isRegisteredTournmt) // Si le joueur n'est pas inscrit au tournoi
            {
                return false;
            }

            bool isRegisteredMatchmaking = MatchMaking.dejaInscrit(isPlaying);

            if (isRegisteredMatchmaking) // Si le joueur est inscrit dans la liste d'attente du Matchmaking
            {
                return false;
            }

            if (isTournoiInProgress(tournmt_id)) // Si le tournoi a déjà commencé
            {
                return false;
            }

            if (alreadyConnected(isPlaying)) // Si le joueur est déjà connecté à un autre Lobby de tournoi
            {
                return false;
            }

            listTournoi[indexTournmt].characters.Add(nouveauCharacter);

            return true;
        }

        // Connexion d'un joueur à une phase de tournoi
        public static bool connectPhaseTournmtInProgress(Client isPlaying, int tournmt_id)
        {
            int indexTournmt = getTournmtInProgressIndex(tournmt_id);

            if (indexTournmt == -1)
            {
                return false;
            }

            bool isRegisteredTournmt = RequeteSQL.isRegisteredInTournmt(isPlaying, tournmt_id);

            if (!isRegisteredTournmt) // Si le joueur n'est pas inscrit au tournoi
            {
                return false;
            }

            bool isRegisteredMatchmaking = MatchMaking.dejaInscrit(isPlaying);

            if (isRegisteredMatchmaking) // Si le joueur est inscrit dans la liste d'attente du Matchmaking
            {
                return false;
            }

            if (listTournoiInProgress[indexTournmt].isPhaseStart) // Si la phase du tournoi a déjà commencé
            {
                return false;
            }

            for (int i = 0; i < listTournoiInProgress[indexTournmt].characters.Count; i++)
            {
                if (listTournoiInProgress[indexTournmt].characters[i].client == isPlaying &&
                    listTournoiInProgress[indexTournmt].characters[i].currentPhase == listTournoiInProgress[indexTournmt].currentPhase &&
                    !listTournoiInProgress[indexTournmt].characters[i].iseliminated)
                {
                    structTournoiCharacter refreshCharacter = listTournoiInProgress[indexTournmt].characters[i];
                    refreshCharacter.isconnected = true;

                    listTournoiInProgress[indexTournmt].characters[i] = refreshCharacter;
                    return true;
                }
            }

            return false;
        }

        // Déconnexion d'un joueur à un tournoi n'ayant pas encore commencé
        public static bool disconnectTournoiClient(Client isPlaying)
        {
            for (int i = 0; i < listTournoi.Count; i++)
            {
                for (int e = 0; e < listTournoi[i].characters.Count; e++)
                {
                    if (listTournoi[i].characters[e].client == isPlaying)
                    {
                        listTournoi[i].characters.RemoveAt(e);
                        return true;
                    }
                }
            }

            return false;
        }

        // Déconnexion d'un joueur à un tournoi ayant commencé
        public static bool disconnectTournoiInProgressClient(Client isPlaying)
        {
            for (int i = 0; i < listTournoiInProgress.Count; i++)
            {
                for (int e = 0; e < listTournoiInProgress[i].characters.Count; e++)
                {
                    if (listTournoiInProgress[i].characters[e].client == isPlaying)
                    {
                        listTournoiInProgress[i].characters.RemoveAt(e);
                        RequeteSQL.desinscriptionTournmt(isPlaying, listTournoiInProgress[i].tournmt_id);
                        disconnectTournoiClient(isPlaying);
                        isPlaying.sendMsgBox("Vous avez été éliminé du tournoi pour vous avoir déconnecté d'un tournoi en cours.", System.Windows.Forms.MessageBoxButtons.OK,
                            System.Windows.Forms.MessageBoxIcon.Information);
                        return true;
                    }
                }
            }

            return false;
        }

        // Suppression du tournoi dans la liste des tournois n'ayant pas commencé
        public static void deleteTournmt(int tournmt_id)
        {
            for (int i = 0; i < listTournoi.Count; i++)
            {
                if (listTournoi[i].tournmt_id == tournmt_id)
                {
                    listTournoi.RemoveAt(i);
                    RequeteSQL.deleteTournmt(tournmt_id);
                    break;
                }
            }
        }

        // Suppression du tournoi dans la liste des tournois ayant commencé
        public static void deleteTournmtInProgress(int tournmt_id)
        {
            for (int i = 0; i < listTournoiInProgress.Count; i++)
            {
                if (listTournoiInProgress[i].tournmt_id == tournmt_id)
                {
                    for (int e = 0; e < listTournoiInProgress[i].characters.Count; e++)
                    {
                        listTournoiInProgress[i].characters[e].client.info_game.info_tournmt.isMatchTournmt = false;
                    }
                    listTournoiInProgress.RemoveAt(i);
                    break;
                }
            }
        }

        // Permet de savoir si un joueur est déjà connecté au lobby
        public static bool alreadyConnected(Client isPlaying)
        {
            for (int i = 0; i < listTournoi.Count; i++)
            {
                for (int e = 0; e < listTournoi[i].characters.Count; e++)
                {
                    if (listTournoi[i].characters[e].client == isPlaying)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        // Récuperation de l'index d'un tournoi n'ayant pas commencé
        public static int getTournmtIndex(int tournmt_id)
        {
            for (int i = 0; i < listTournoi.Count; i++)
            {
                if (listTournoi[i].tournmt_id == tournmt_id)
                {
                    return i;
                }
            }
            return -1;
        }

        // Récuperation de l'index d'un tournoi ayant commencé
        public static int getTournmtInProgressIndex(int tournmt_id)
        {
            for (int i = 0; i < listTournoiInProgress.Count; i++)
            {
                if (listTournoiInProgress[i].tournmt_id == tournmt_id)
                {
                    return i;
                }
            }
            return -1;
        }

        // Permet de savoir si un joueur est connecté à une phase de tournoi
        public static bool isCharacterConnectedPhase(int tournmt_id, int character_id)
        {
            int indexTournmt = getTournmtInProgressIndex(tournmt_id);

            if (indexTournmt != -1)
            {
                for (int i = 0; i < listTournoiInProgress[indexTournmt].characters.Count; i++)
                {
                    if (listTournoiInProgress[indexTournmt].characters[i].client.info_main.character_id == character_id)
                    {
                        return listTournoiInProgress[indexTournmt].characters[i].isconnected;
                    }
                }
            }
            return false;
        }

        // Récuperation de l'index d'un joueur inscrit à un tournoi n'ayant pas commencé
        public static int getCharacterFromTournmt(int tournmt_id, int character_id)
        {
            int indexTournmt = getTournmtIndex(tournmt_id);

            if (indexTournmt != -1)
            {
                for (int i = 0; i < listTournoi[indexTournmt].characters.Count; i++)
                {
                    if (listTournoi[indexTournmt].characters[i].client.info_main.character_id == character_id)
                    {
                        return i;
                    }
                }
            }
            return -1;
        }

        // Récuperation de l'index d'un joueur inscrit à un tournoi ayant commencé
        public static int getCharacterFromTournmtInProgress(int tournmt_id, int character_id)
        {
            int indexTournmt = getTournmtInProgressIndex(tournmt_id);
            
            if (indexTournmt != -1)
            {
                for (int i = 0; i < listTournoiInProgress[indexTournmt].characters.Count; i++)
                {
                    if (listTournoiInProgress[indexTournmt].characters[i].client.info_main.character_id == character_id)
                    {
                        return i;
                    }
                }
            }
            return -1;
        }
        
        // Permet de savoir si un tournoi a commencé
        public static bool isTournoiInProgress(int tournmt_id)
        {
            for (int i = 0; i < listTournoiInProgress.Count; i++)
            {
                if (listTournoiInProgress[i].tournmt_id == tournmt_id)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
