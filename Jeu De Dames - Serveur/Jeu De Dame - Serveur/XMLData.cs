﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Jeu_De_Dame___Serveur
{
    class XMLData
    {
        public static string ip; // Adresse IP du serveur
        public static int port; // Port de connexion du serveur

        public static int config_delay_chat; // Délai entre chaque message
        public static int config_delay_tour; // Durée d'un tour de jeu
        public static int config_count_tour_down; // Nombres de tours ratés avant disqualification

        // Récuperation des informations du serveur depuis un fichier XML
        public static bool loadServerConfigXml(string fileName)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(fileName);

                XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes("/game/server");
                
                foreach (XmlNode node in nodeList)
                {
                    ip = node.SelectSingleNode("ip").InnerText;
                    port = More.s_int(node.SelectSingleNode("port").InnerText);
                }

                nodeList = xmlDoc.DocumentElement.SelectNodes("/game/config");

                foreach (XmlNode node in nodeList)
                {
                    config_delay_chat = More.s_int(node.SelectSingleNode("delay_chat").InnerText);
                    config_delay_tour = More.s_int(node.SelectSingleNode("delay_tour").InnerText);
                    config_count_tour_down = More.s_int(node.SelectSingleNode("count_tour_down").InnerText);
                }
                
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        // Récuperation des informations de la base de données depuis un fichier XML
        public static bool loadMysqlConfigXml(string fileName)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(fileName);

                XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes("/game/mysql");
                string ip = "";
                string database_name = "";
                string user = "";
                string password = "";

                foreach (XmlNode node in nodeList)
                {
                    ip = node.SelectSingleNode("ip").InnerText;
                    database_name = node.SelectSingleNode("database_name").InnerText;
                    user = node.SelectSingleNode("user").InnerText;
                    password = node.SelectSingleNode("password").InnerText;
                }

                RequeteSQL.setConfigMysql(ip, database_name, user, password);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
