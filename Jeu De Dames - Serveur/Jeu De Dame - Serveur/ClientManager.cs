﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;
using System.Net;

namespace Jeu_De_Dame___Serveur
{
    class Client
    {
        public Socket monSocket; // Relie le serveur et le client

        public infos info_main; // Structure contenant les variables principales du client
        public game info_game; // Structure contenant les variables "jeux" du client

        public byte[] buffer = new byte[1024]; // Tableau de bytes contenant les informations reçues par le client
        
        public Client(string pseudo, string passe, int account_id, int character_id) // Constructeur
        {
            info_main.pseudo = pseudo;
			info_main.passe = passe;
            info_main.account_id = account_id;
            info_main.character_id = character_id;
        }
        
        public struct infos
        {
            public string pseudo; //Pseudo du joueur
			public string passe; // mot de passe du joueur
            public bool playerTop; // Indique la position initiale du joueur (le plateau a été inversé donc le nom n'est plus conforme) TODO
            public int account_id; // ID de compte du joueur
            public int character_id; // ID de personnage du joueur
        }

        public struct game
        {
            public string opponent; //Pseudo de l'adversaire
            public bool isplaying; // Indique si le joueur est dans une partie ou non
            public bool tour; // Indique si c'est le tour de l'adversaire
            public int pawnAlive; //Indique le nombre de pions possédés sur le plateau
            public bool iscombo; // Indique si le joueur est en position de "combo"
            public Plateau.cases[][] plateauCases; //Les cases du plateau
            public bool asked; // Indique si le joueur doit faire un choix
            public int timeTourCount; // Indique le nombre de seconde entre les tours
			public int timeExist; // Indique la durée écoulée du tour
			public bool chatOn; // Indique si le joueur a la possibilité d'envoyer un message
			public int timeChatCount; //Indique le nombre de seconde entre chaque message
            public bool carefulMode; //Indique le mode de jeu
            public int tourDown; //Indique le nombre de tours sans jouer
            public tournmt info_tournmt; //structure contenant les variables "tournois" du client
        }

        public struct tournmt
        {
            public bool isMatchTournmt; // Indique si le match en cours est un match de qualification en tournoi
            public int tournmtId; // Indique l'id du tournoi dans lequel le joueur participe
        }

		public void send(string packet) // Redirection vers l'envoi des packets au client
		{
            Console.WriteLine("PACKET ENVOYE : {0} A {1}", packet, this.info_main.pseudo);
            InitializeServer.send(monSocket, packet);
		}

        public void deconnexion() // Déconnexion du client
        {
            ClientManager.redirectEnding(this, true);

			bool enAttente = MatchMaking.dejaInscrit(this);
            bool enAttenteByPseudo = MatchMaking.dejaInscritByPseudo(this);

			if (enAttente)
			{
                MatchMaking.supprimerListeattente(this.info_main.pseudo);
			}
            else if (enAttenteByPseudo)
            {
                MatchMaking.supprimerListeattenteByPseudo(this.info_main.pseudo);
            }

            Lobby.disconnectLobby(this);

            ClientManager.listClient.RemoveAt(ClientManager.bySocket(monSocket));

            Console.WriteLine("Le client " + info_main.pseudo + " vient de se deconnecter");

			RequeteSQL.setOnline(this, false);

			if (monSocket != null)
            {
                monSocket.Shutdown(SocketShutdown.Both);
                monSocket.Close();
            }
        }

        public void sendMsg(string message, int message_id) // Envoi d'un paquet pour l'envoie des messages
        {
            string packet = String.Format("msg {0} {1}", message_id, message);
            this.send(packet);
        }

        public void sendMsgBox(string message, MessageBoxButtons btn, MessageBoxIcon ic) // Envoi d'un paquet pour afficher une boite de dialogue sur le client de jeu
        {
            this.send(String.Format("box {0} {1} {2}", (int)btn, (int)ic, message));
        }

        public void sendRequest(string message, MessageBoxButtons bt, MessageBoxIcon ic, int x, int y) // Envoi un paquet pour l'envoie d'une requête sur le client de jeu
        {
            string packet = string.Format("req 0 {0} {1} {2} {3} {4}", (int)bt, (int)ic, (int)x, (int)y, message);
            this.send(packet);
        }
    }

    class ClientManager
    {
        public static List<Client> listClient = new List<Client>(); // Liste des clients

        // Récuperation d'un client par son Socket
        public static int bySocket(Socket socketClient)
        {
            for (int i = 0; i < listClient.Count; i++)
            {
                if (listClient[i].monSocket == socketClient)
                {
                    return i;
                }
            }

            return -1;
        }

        // Récuperation d'un client par son pseudo
        public static int byPseudo(string pseudoClient)
        {
            for (int i = 0; i < listClient.Count; i++)
            {
                if (listClient[i].info_main.pseudo == pseudoClient)
                {
                    return i;
                }
            }

            return -1;
        }

        // Redirection des joueurs après une rencontre
        public static void redirectEnding(Client playerDefeat, bool forfeit)
        {
            if (playerDefeat.info_game.isplaying)
            {
                int indexOpponent = byPseudo(playerDefeat.info_game.opponent);

                if (indexOpponent == -1)
                {
                    return;
                }

                if (listClient[indexOpponent].info_game.isplaying)
                {
                    // Ajoute dans la base de données les informations du match
                    // TODO : Enregistrer les matchs tournois ? Ou non ?
                    RequeteSQL.setInfosMatch(listClient[indexOpponent].info_main.character_id, playerDefeat.info_main.character_id, forfeit);

                    playerDefeat.info_game.isplaying = false;
                    listClient[indexOpponent].info_game.isplaying = false;

                    victory(listClient[indexOpponent], forfeit);
                    defeat(playerDefeat, forfeit);
                }
            }
        } // Redirection 

        // Victoire d'un joueur
        public static void victory(Client playerVictory, bool forfeit)
        {
            if (forfeit)
            {
                playerVictory.send("msg_final L'adversaire s'est déconnecté ... Vous avez donc gagné !");
            }
            else
            {
                playerVictory.send("msg_final Félicitation! Vous avez réussi à battre votre adversaire, vous avez gagné !");
            }
            playerVictory.info_game.isplaying = false;

            // Traitement de la phase du tournoi
            if (playerVictory.info_game.info_tournmt.isMatchTournmt)
            {
                int tournmt_id = playerVictory.info_game.info_tournmt.tournmtId;

                int indexTournmt = Lobby.getTournmtInProgressIndex(playerVictory.info_game.info_tournmt.tournmtId);

                if (indexTournmt == -1)
                {
                    return;
                }

                int indexPlayer = Lobby.getCharacterFromTournmtInProgress(tournmt_id, playerVictory.info_main.character_id);

                if (indexPlayer == -1)
                {
                    return;
                }

                Lobby.structTournoiCharacter stc = Lobby.listTournoiInProgress[indexTournmt].characters[indexPlayer];
                stc.currentPhase++;
                stc.victoryCount++;

                Lobby.listTournoiInProgress[indexTournmt].characters[indexPlayer] = stc;
            }
        }

        // Défaite d'un joueur
        public static void defeat(Client playerDefeat, bool forfeit)
        {
            if (!forfeit)
            {
                playerDefeat.send("msg_final Vous avez perdu !");

                playerDefeat.info_game.isplaying = false;
            }else
            {
                playerDefeat.send("msg_final Vous êtes resté inactif durant beaucoup trop de tours ! Vous avez été disqualifié");
                playerDefeat.info_game.isplaying = false;
            }

            // Traitement de la phase du tournoi
            if (playerDefeat.info_game.info_tournmt.isMatchTournmt)
            {
                int tournmt_id = playerDefeat.info_game.info_tournmt.tournmtId;

                int indexTournmt = Lobby.getTournmtInProgressIndex(playerDefeat.info_game.info_tournmt.tournmtId);

                if (indexTournmt == -1)
                {
                    return;
                }

                int indexPlayer = Lobby.getCharacterFromTournmtInProgress(tournmt_id, playerDefeat.info_main.character_id);

                if (indexPlayer == -1)
                {
                    return;
                }

                Lobby.structTournoiCharacter stc = Lobby.listTournoiInProgress[indexTournmt].characters[indexPlayer];
                stc.iseliminated = true;

                Lobby.listTournoiInProgress[indexTournmt].characters[indexPlayer] = stc;
            }
        }
    }
}
