﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jeu_De_Dame___Serveur
{
    class PlayerManager
    {
        // Effectue le changement de tour
        public static void changeGameTurn(Client isPlaying)
        {
            int indexOpponent = ClientManager.byPseudo(isPlaying.info_game.opponent);
            int indexClient = ClientManager.byPseudo(isPlaying.info_main.pseudo);

            if (indexClient == -1 || indexOpponent == -1)
            {
                return;
            }

            ClientManager.listClient[indexClient].info_game.timeTourCount = Environment.TickCount;
            ClientManager.listClient[indexOpponent].info_game.timeTourCount = Environment.TickCount;

			ClientManager.listClient[indexClient].info_game.timeExist = 0;
			ClientManager.listClient[indexOpponent].info_game.timeExist = 0;

            if (isPlaying.info_game.tour)
            {
                ClientManager.listClient[indexClient].info_game.tour = !ClientManager.listClient[indexClient].info_game.tour;
                ClientManager.listClient[indexOpponent].info_game.tour = !ClientManager.listClient[indexOpponent].info_game.tour;
            }

            if (!EndGame.canMakeAnAction(whosNext(isPlaying)))
            {
                ClientManager.listClient[indexClient].sendMsg("Partie terminée", 1);
                ClientManager.listClient[indexOpponent].sendMsg("Partie terminée", 1);
                ClientManager.redirectEnding(whosNext(isPlaying), false);
            }
        }

        // Détecte qui est le prochain joueur à jouer son tour
        public static Client whosNext(Client isPlaying)
        {
            int indexOpponent = ClientManager.byPseudo(isPlaying.info_game.opponent);
            int indexClient = ClientManager.byPseudo(isPlaying.info_main.pseudo);

            if (indexClient == -1 || indexOpponent == -1)
            {
                return null;
            }

            if (ClientManager.listClient[indexClient].info_game.tour)
            {
                return ClientManager.listClient[indexClient];
            }
            return ClientManager.listClient[indexOpponent];
        }

        // Récupère le client de l'adversaire du joueur
        public static Client getOpponent(Client player)
        {
            int indexOpponent = ClientManager.byPseudo(player.info_game.opponent);
            if (indexOpponent == -1)
            {
                return null;
            }

            return ClientManager.listClient[indexOpponent];
        }
    }
}