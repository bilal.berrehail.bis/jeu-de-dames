﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BunifuAnimatorNS;

namespace Jeu_De_Dame___Serveur
{
    class Action
    {
        // Traitement du mouvement de pion du joueur
        public static bool pawnMoving(Client isPlaying, int x, int y, int xSelected, int ySelected)
        {
            if (isPlaying.info_game.asked)
            {
                isPlaying.sendMsg("En attente de la réponse ...", 1);
                return false;
            }

            if (isPlaying.info_main.playerTop != isPlaying.info_game.plateauCases[ySelected][xSelected].pawnTop)
            {
                Careful.isNotCareful(isPlaying, ySelected, xSelected);
                return false;
            }

            if (!isPlaying.info_game.tour)
            {
                isPlaying.sendMsg("Ce n'est pas votre tour !", 1);
                return false;
            }

            if (isPlaying.info_game.iscombo)
            {
                if (!isPlaying.info_game.plateauCases[ySelected][xSelected].mainCombo)
                {
                    isPlaying.sendMsg("Vous pouvez encore jouer votre Pion \"combo\".", 1); 
                    return false;
                }
            }

            int indexClient = ClientManager.byPseudo(isPlaying.info_main.pseudo);

            if (indexClient == -1)
			{
				return false;
			}

            Client player = PlayerManager.whosNext(isPlaying);
            Client opponent = PlayerManager.getOpponent(player);
          
            bool playerTop = player.info_main.playerTop;
           
            if (ClientManager.listClient[indexClient].info_game.plateauCases[y][x].pawnExist)
            {
                return false;
            }

            if (x == xSelected && y == ySelected)
            {
                return false;
            }
            else if (!ClientManager.listClient[indexClient].info_game.plateauCases[y][x].pawnExist)
            {

                int ruleDistance = Distance.distanceStatus(isPlaying, y, x, ySelected, xSelected, playerTop);

                if (ruleDistance == 0 || (ruleDistance != 2 && ClientManager.listClient[indexClient].info_game.plateauCases[ySelected][xSelected].mainCombo &&
                    player.info_game.iscombo) || !Distance.sameDiagonal(y, x, ySelected, xSelected))
                {
                    isPlaying.sendMsg("Vous ne pouvez pas faire cela", 1);
                    return false;
                }
               
                // La règle " Souffler n'est pas joué " est optionnel
                if (isPlaying.info_game.carefulMode)
                {
                    Careful.resetNotCarefulOpponent(opponent);

                    Careful.checkJumpingNotPlayed(player, x, y);
                }
                
                ClientManager.listClient[indexClient].info_game.plateauCases[y][x].king = ClientManager.listClient[indexClient].info_game.plateauCases[ySelected][xSelected].king;
                ClientManager.listClient[indexClient].info_game.plateauCases[ySelected][xSelected].king = false;
                
                Match.synchroWithOpponents(isPlaying);

                setCase(isPlaying, x, y, ImageManager.getPawnImgByPlayer(playerTop, ClientManager.listClient[indexClient].info_game.plateauCases[y][x].king), true, playerTop);

                setCase(isPlaying, xSelected, ySelected);
                ImageManager.pawnToKing(isPlaying, playerTop, x, y);
               
                if (ruleDistance == 2)
                {
                    bool isKing = ClientManager.listClient[indexClient].info_game.plateauCases[y][x].king;

                    int xPawn = Distance.xPawn;
                    int yPawn = Distance.yPawn;
                    
                    setCase(isPlaying, xPawn, yPawn);
                    opponent.info_game.pawnAlive--;

                    if (!isKing)
                    {
                        if (Attack.detectCanAtk(player, x, y))
                        {
                            player.info_game.iscombo = true;
                            ClientManager.listClient[indexClient].info_game.plateauCases[y][x].mainCombo = true;
                            Match.synchroWithOpponents(isPlaying);
                            return true;
                        }
                    }
                    else
                    {
                        if (Attack.detectCanAtkForKing(player, x, y))
                        {
                            player.info_game.iscombo = true;
                            ClientManager.listClient[indexClient].info_game.plateauCases[y][x].mainCombo = true;
                            Match.synchroWithOpponents(isPlaying);
                            return true;
                        }
                    }

                    player.info_game.iscombo = false;
                    ClientManager.listClient[indexClient].info_game.plateauCases[y][x].mainCombo = false;
                    Match.synchroWithOpponents(isPlaying);
                    if (EndGame.opponentIsDead(opponent))
                    {
                        return true;
                    }
                }

                PlayerManager.changeGameTurn(player);

                xSelected = -1;
                ySelected = -1;
            }
            return true;
        }
        
        // -- NON UTILISE POUR LE MOMENT --
        public static void sendAnimation(Client isPlaying, int type, int x, int y)
        {
            int indexClient = ClientManager.byPseudo(isPlaying.info_main.pseudo);
            int indexOpponent = ClientManager.byPseudo(isPlaying.info_game.opponent);

            if (indexClient == -1|| indexOpponent == -1)
            {
                return;
            }

            string packet = "anim " + type + " " + x + " " + y;

            ClientManager.listClient[indexClient].send(packet);
            ClientManager.listClient[indexOpponent].send(packet);
        }

        // Modification d'une case sur le client de jeu
        public static void setCase(Client isPlaying, int x, int y, int imgPawn = -1, bool isExist = false, bool isTop = false)
        {
            int indexClient = ClientManager.byPseudo(isPlaying.info_main.pseudo);
            int indexOpponent = ClientManager.byPseudo(isPlaying.info_game.opponent);

            if (indexClient == -1 || indexOpponent == -1)
            {
                return;
            }

            try
            {
                string pathImg = imgPawn.ToString();

                ClientManager.listClient[indexClient].info_game.plateauCases[y][x].pawnExist = isExist;

                ClientManager.listClient[indexClient].info_game.plateauCases[y][x].pawnTop = isTop;

                Match.synchroWithOpponents(isPlaying);

                string packet = "set " + x + " " + y + " " + Convert.ToInt32(isExist) + " " + Convert.ToInt32(isTop) + " " + imgPawn;
                ClientManager.listClient[indexClient].send(packet);
                ClientManager.listClient[indexOpponent].send(packet);
            }
            catch (Exception ex)
            { Console.WriteLine(ex.ToString()); }
        }
    }
}
