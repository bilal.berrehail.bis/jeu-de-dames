﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jeu_De_Dame___Serveur
{
    class ImageManager
    {
        // Initialisation des variables plateaux d'un joueur
        public static void initializePawn(Client isPlaying)
        {
            int indexClient = ClientManager.byPseudo(isPlaying.info_main.pseudo);

            if (indexClient == -1)
            {
                return;
            }

            for (int y = 0; y < 10; y++)
            {
                for (int x = 0; x < 10; x++)
                {
                    if (ClientManager.listClient[indexClient].info_game.plateauCases[y][x].isBlack)
                    {
                        PictureBox pb = new PictureBox();
                        ClientManager.listClient[indexClient].info_game.plateauCases[y][x].Rec.X = x;
                        ClientManager.listClient[indexClient].info_game.plateauCases[y][x].Rec.Y = y;

						ClientManager.listClient[indexClient].info_game.plateauCases[y][x].isnotcareful = false;
                        
                        if (y < 4) // Pion du haut
                        {
                            ClientManager.listClient[indexClient].info_game.plateauCases[y][x].pawnExist = true;
                            ClientManager.listClient[indexClient].info_game.plateauCases[y][x].pawnTop = true;

                        }
                        if (y > 5) // Pion du bas
                        {
                            ClientManager.listClient[indexClient].info_game.plateauCases[y][x].pawnExist = true;
                            ClientManager.listClient[indexClient].info_game.plateauCases[y][x].pawnTop = false;
                        }
                    }
                }
            }
        }

        // Transforme un pion normal en dame
        public static void pawnToKing(Client isPlaying, bool playerTop, int x, int y)
        {
            int indexClient = ClientManager.byPseudo(isPlaying.info_main.pseudo);

            if (indexClient == -1)
            {
                return;
            }

            if (Distance.isLastLine(playerTop, y))
            {
                if (!ClientManager.listClient[indexClient].info_game.plateauCases[y][x].king)
                {
                    Action.setCase(isPlaying, x, y, getPawnImgByPlayer(playerTop, true), true, playerTop);
                    ClientManager.listClient[indexClient].info_game.plateauCases[y][x].king = true;
                }
            }
            Match.synchroWithOpponents(isPlaying);
        }

        // Récupère l'état du pion en fonction de s'il a été transformé en dame et s'il est le joueur 1 ou 2
        public static int getPawnImgByPlayer(bool playerTop, bool isking = false)
        {
            if (playerTop)
            {
                if (isking)
                {
                    return 2;
                }
                return 1;
            }
            if (isking)
            {
                return 4;
            }
            return 3;
        }
    }
}
