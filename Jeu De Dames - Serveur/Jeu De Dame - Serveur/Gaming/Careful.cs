﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jeu_De_Dame___Serveur
{
    class Careful
    {
        // Demande au joueur s'il souhaite détruire le pion qui n'a pas respecté la règle "Souffler n'est pas joué"
        public static void isNotCareful(Client isPlaying, int y, int x)
        {
            int indexClient = ClientManager.byPseudo(isPlaying.info_main.pseudo);

            if (indexClient == -1)
            {
                return;
            }

            if (ClientManager.listClient[indexClient].info_game.plateauCases[y][x].isnotcareful && ClientManager.listClient[indexClient].info_game.plateauCases[y][x].pawnExist)
            {
                isPlaying.sendRequest("Souhaitez-vous détruire ce pion ?", MessageBoxButtons.YesNo, MessageBoxIcon.Information, x, y);
                isPlaying.info_game.asked = true;
                return;
            }
            isPlaying.sendMsg("Ce n'est pas vos pions", 1);
        }

        // Réinitialise les variables de jeu concernant la règle "Souffler n'est pas joué"
        public static void resetNotCarefulOpponent(Client Opponent)
        {
            int indexOpponent = ClientManager.byPseudo(Opponent.info_main.pseudo);

            if (indexOpponent == -1)
            {
                return;
            }

            for (int y = 0; y < ClientManager.listClient[indexOpponent].info_game.plateauCases.Length; y++)
            {
                for (int x = 0; x < ClientManager.listClient[indexOpponent].info_game.plateauCases[y].Length; x++)
                {
                    if (Opponent.info_main.playerTop == ClientManager.listClient[indexOpponent].info_game.plateauCases[y][x].pawnTop)
                    {
                        ClientManager.listClient[indexOpponent].info_game.plateauCases[y][x].isnotcareful = false;
                    }
                }
            }
            Match.synchroWithOpponents(Opponent);
        }

        // Détecte les pions n'ayant pas respecté la règle "Souffler n'est pas joué"
        public static void checkJumpingNotPlayed(Client isPlaying, int myX, int myY, bool noPawn = false)
        {
            int indexClient = ClientManager.byPseudo(isPlaying.info_main.pseudo);

            if (indexClient == -1)
            {
                return;
            }

            bool playerTop = isPlaying.info_main.playerTop;
            for (int y1 = 0; y1 < ClientManager.listClient[indexClient].info_game.plateauCases.Length; y1++)
            {
                for (int x1 = 0; x1 < ClientManager.listClient[indexClient].info_game.plateauCases[y1].Length; x1++)
                {
                    if (ClientManager.listClient[indexClient].info_game.plateauCases[y1][x1].pawnTop == playerTop &&
                        ClientManager.listClient[indexClient].info_game.plateauCases[y1][x1].pawnExist && (ClientManager.listClient[indexClient].info_game.plateauCases[y1][x1].Rec != ClientManager.listClient[indexClient].info_game.plateauCases[myY][myX].Rec) || noPawn)
                    {
                        for (int y2 = 0; y2 < ClientManager.listClient[indexClient].info_game.plateauCases.Length; y2++)
                        {
                            for (int x2 = 0; x2 < ClientManager.listClient[indexClient].info_game.plateauCases[y2].Length; x2++)
                            {
                                if (ClientManager.listClient[indexClient].info_game.plateauCases[y2][x2].pawnTop != playerTop &&
                                    ClientManager.listClient[indexClient].info_game.plateauCases[y2][x2].pawnExist)
                                {
                                    if (!ClientManager.listClient[indexClient].info_game.plateauCases[y1][x1].king) // Pion normal
                                    {
                                        if (Attack.detectCanAtk(isPlaying, x1, y1))
                                        {
                                            ClientManager.listClient[indexClient].info_game.plateauCases[y1][x1].isnotcareful = true;
                                        }
                                    }
                                    else // Reine
                                    {
                                        if (Attack.detectCanAtkForKing(isPlaying, x1, y1))
                                        {
                                            ClientManager.listClient[indexClient].info_game.plateauCases[y1][x1].isnotcareful = true;
                                        }
                                    }
                                }
                                else if (ClientManager.listClient[indexClient].info_game.iscombo &&
                                    ClientManager.listClient[indexClient].info_game.plateauCases[y2][x2].mainCombo)
                                {
                                    ClientManager.listClient[indexClient].info_game.plateauCases[y2][x2].mainCombo = false;
                                    ClientManager.listClient[indexClient].info_game.iscombo = false;
                                }
                            }
                        }
                    }
                }
            }
            Match.synchroWithOpponents(isPlaying);
        }
    }
}