﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Jeu_De_Dame___Serveur
{
	class RequeteSQL
	{
		static string ip; // Adresse IP
		static string database_name; // Nom de la base de données
		static string utilisateur; // Nom d'utilisateur
		static string password; // Mot de passe

        static string connectionString; // Variable de connexion

        // Récupération des variables utilitaire à la connexion à la base de données
        public static void setConfigMysql(string _ip, string _database_name, string _utilisateur, string _password)
        {
            ip = _ip;
            database_name = _database_name;
            utilisateur = _utilisateur;
            password = _password;
        }

        // Chargement du fichier de configuration et initialisation de la variable de connexion à la base de données
		public static bool initialisation()
		{
            if (!XMLData.loadMysqlConfigXml(Program.fileNameData))
            {
                return false;
            }

            // Affichage mode console
            Console.WriteLine("    - Configuration MySQL -    ");
            Console.WriteLine("-------------------------------");
            Console.WriteLine("IP : " + ip);
            Console.WriteLine("Database name : " + database_name);
            Console.WriteLine("User : " + utilisateur);
            Console.WriteLine("Password : " + password);
            Console.WriteLine("-------------------------------");
            Console.WriteLine("");

            // Initialisation de la variable de connexion
            connectionString = String.Format("SERVER={0};" + "DATABASE={1};" + "UID={2};" + "PASSWORD={3};", ip, database_name, utilisateur, password);

            return true;
		}
        
        // Récuperation de l'ID de l'utilisateur
        public static int getAccountID(string username, string passwordAccount)
        {
            string requete = String.Format("SELECT * FROM table_accounts WHERE AccountName='{0}' And AccountPassword='{1}'", username, passwordAccount);

            using (MySqlConnection connec = new MySqlConnection(connectionString))
            using (MySqlCommand command = new MySqlCommand(requete, connec))
            {
                connec.Open();
                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int account_id = Convert.ToInt32(reader["AccountID"]);

                        return account_id;
                    }
                }
            }

            return -1;
        }

        // Modification de l'état de connexion du compte
        public static void setOnline(Client isPlaying, bool isOnline)
        {
            int account_id = isPlaying.info_main.account_id;

            string requete = String.Format("UPDATE table_accounts SET IsOnline='{0}' WHERE AccountID = {1}", Convert.ToInt32(isOnline), account_id);

            using (MySqlConnection connec = new MySqlConnection(connectionString))
            using (MySqlCommand command = new MySqlCommand(requete, connec))
            {
                connec.Open();

                command.ExecuteNonQuery();
            }
        }

        // Récuperation du score
        public static string getScore(Client isPlaying)
        {
            int character_id = isPlaying.info_main.character_id;

            string requete = String.Format("SELECT COUNT(*) FROM table_results WHERE fkWinnerCharacterID = {0}", character_id);

            int victoryCount = 0;
            int defeatCount = 0;

            using (MySqlConnection connec = new MySqlConnection(connectionString))
            using (MySqlCommand command = new MySqlCommand(requete, connec))
            {
                connec.Open();
                victoryCount = Convert.ToInt32(command.ExecuteScalar());
            }

            requete = String.Format("SELECT COUNT(*) FROM table_results WHERE fkLoserCharacterID = {0}", character_id);

            using (MySqlConnection connec = new MySqlConnection(connectionString))
            using (MySqlCommand command = new MySqlCommand(requete, connec))
            {
                connec.Open();
                defeatCount = Convert.ToInt32(command.ExecuteScalar());
            }

            return String.Format("score {0} {1}", victoryCount, defeatCount);
        }

        // Vérification si le compte est apte à jouer au jeu de dames
        public static string verifyAccount(string username, string passwordAccount)
        {
            string requete = String.Format("SELECT * FROM table_accounts, table_characters WHERE AccountName='{0}' And AccountPassword='{1}' And table_accounts.AccountID = table_characters.fkAccountID", username, passwordAccount);

            bool isCorrect = false;

            using (MySqlConnection connec = new MySqlConnection(connectionString))
            using (MySqlCommand command = new MySqlCommand(requete, connec))
            {
                connec.Open();
                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        isCorrect = true;

                        int gameID = Convert.ToInt32(reader["fkGameID"]);
                        bool isOnline = Convert.ToBoolean(reader["IsOnline"]);

                        if (gameID == 1) // Jeu de dames
                        {
                            if (!isOnline)
                            {
                                int character_id = Convert.ToInt32(reader["CharacterID"]);
                                return String.Format("2 {0}", character_id); // Disponible
                            }
                            else
                                return "1 -1"; // Connecté
                        }
                    }
                }
            }

            if (isCorrect)
            {
                return "3 -1"; // Pas de personnage " Jeu de dames " trouvé
            }

            return "0 -1"; // N'existe pas
        }

        // Récupération des informations de l'adversaire
        public static string getInfosOpponent(string opponent_username)
        {
            int indexClient = ClientManager.byPseudo(opponent_username);

            if (indexClient == -1)
            {
                return "Unknown 0 0 0000-00-00 00:00:00";
            }

            string requete = String.Format("SELECT * FROM table_accounts, table_characters WHERE AccountName='{0}' And table_accounts.AccountID = table_characters.fkAccountID", opponent_username);
            
            using (MySqlConnection connec = new MySqlConnection(connectionString))
            using (MySqlCommand command = new MySqlCommand(requete, connec))
            {
                connec.Open();
                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int gameID = Convert.ToInt32(reader["fkGameID"]);

                        if (gameID == 1) // Jeu de dames
                        {
                            string[] score_split = getScore(ClientManager.listClient[indexClient]).Split(' ');

                            if (score_split.Length == 3)
                            {
                                int victoryCount = Convert.ToInt32(score_split[1]);
                                int defeatCount = Convert.ToInt32(score_split[2]);

                                string creationDate = reader["CreationDate"].ToString();

                                string infosOpponent = opponent_username + " " + victoryCount + " " + defeatCount + " " + creationDate;

                                return infosOpponent;
                            }
                        }
                    }
                }
            }
            
            return "Unknown 0 0 0000-00-00 00:00:00";
        }

        // Ajout de l'issu d'un match
        public static void setInfosMatch(int character_id_winner_name, int character_id_loser_name, bool isforfeit)
        {
            DateTime myDateTime = DateTime.Now;
            string sqlFormattedDate = myDateTime.ToString("yyyy-MM-dd HH:mm:ss");
            
            string requete = String.Format("INSERT INTO table_results (fkWinnerCharacterID, fkLoserCharacterID, IsForfeit, DateMatch) VALUES ('{0}', '{1}', '{2}', '{3}')", character_id_winner_name, character_id_loser_name, Convert.ToInt32(isforfeit), sqlFormattedDate);

            using (MySqlConnection connec = new MySqlConnection(connectionString))
            using (MySqlCommand command = new MySqlCommand(requete, connec))
            {
                connec.Open();

                command.ExecuteNonQuery();
            }
        }

        // Récuperation du nombre de ligne renvoyé par la base de données
        public static int dataReaderCount(MySqlDataReader dataReader)
        {
            int readerCount = 0;
            while (dataReader.Read())
            {
                readerCount++;
            }

            return readerCount;
        }

        // Structure dédié au tournoi
        public struct structTournoi
        {
            public int tournmt_id;
            public int nb_inscrits;
            public int max_players;
            public string nom_tournoi;
            public string deadline_reg;
            public string date_start;
            public int isregistered;
        }

        // Suppression d'un tournoi, de son registre d'inscription et de son arbre des rencontres
        public static void deleteTournmt(int tournmt_id)
        {
            List<int> listRegisterID = new List<int>();
            
            string requete = String.Format("DELETE FROM table_tournmts WHERE TournmtID = {0}", tournmt_id);

            using (MySqlConnection connec = new MySqlConnection(connectionString))
            using (MySqlCommand command = new MySqlCommand(requete, connec))
            {
                connec.Open();
                command.ExecuteNonQuery();
            }

            requete = String.Format("DELETE FROM table_register WHERE fkTournmtID = {0}", tournmt_id);

            using (MySqlConnection connec = new MySqlConnection(connectionString))
            using (MySqlCommand command = new MySqlCommand(requete, connec))
            {
                connec.Open();
                command.ExecuteNonQuery();
            }

            requete = String.Format("DELETE FROM table_infos_tournmt WHERE fkTournmtID = '{0}'", tournmt_id);

            using (MySqlConnection connec = new MySqlConnection(connectionString))
            using (MySqlCommand command = new MySqlCommand(requete, connec))
            {
                connec.Open();
                command.ExecuteNonQuery();
            }
        }

        // Récuperation des informations d'un tournoi pour le client de jeu
        public static string recupererTournmtsRegistered(Client isPlaying)
        {
            int character_id = isPlaying.info_main.character_id;

            string requete = String.Format("SELECT * FROM table_register, table_tournmts WHERE table_register.fkCharacterID = {0} And table_register.fkTournmtID = table_tournmts.TournmtID", character_id);

            StringBuilder packet_tournmts = new StringBuilder("list_tournmt_lobby");

            using (MySqlConnection connec = new MySqlConnection(connectionString))
            using (MySqlCommand command = new MySqlCommand(requete, connec))
            {
                connec.Open();
                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        structTournoi monTournoi = new structTournoi();

                        monTournoi.tournmt_id = Convert.ToInt32(reader["TournmtID"]);
                        monTournoi.nom_tournoi = reader["TournmtName"].ToString();
                        monTournoi.date_start = reader["StartDate"].ToString();

                        int state_lobby = Lobby.getCharacterFromTournmtInProgress(monTournoi.tournmt_id, character_id);

                        int state_lobby_1 = Lobby.getCharacterFromTournmt(monTournoi.tournmt_id, character_id);

                        if (state_lobby == -1 && state_lobby_1 == -1)
                        {
                            state_lobby = 0;
                        }
                        else
                            state_lobby = 1;

                        int state_phase = Convert.ToInt32(Lobby.isCharacterConnectedPhase(monTournoi.tournmt_id, character_id));

                        bool tournmtStarted = Lobby.isTournoiInProgress(monTournoi.tournmt_id);

                        if (!tournmtStarted)
                        {
                            packet_tournmts.Append(" ");
                            packet_tournmts.Append(monTournoi.tournmt_id);
                            packet_tournmts.Append(" ");
                            packet_tournmts.Append(state_lobby);
                            packet_tournmts.Append(" ");
                            packet_tournmts.Append(state_phase);
                            packet_tournmts.Append(" En_attente Date inconnue ");
                            packet_tournmts.Append(monTournoi.date_start);
                            packet_tournmts.Append(" ");
                            packet_tournmts.Append(monTournoi.nom_tournoi);
                        }
                        else
                        {
                            int indexTournmt = Lobby.getTournmtInProgressIndex(monTournoi.tournmt_id);

                            if (indexTournmt != -1)
                            {
                                int indexJoueur = Lobby.getCharacterFromTournmtInProgress(monTournoi.tournmt_id, character_id);

                                if (indexJoueur != -1)
                                {
                                    int currentPhase = Lobby.listTournoiInProgress[indexTournmt].currentPhase;
                                    string nextPhaseStart = Lobby.listTournoiInProgress[indexTournmt].phaseStart.ToString("yyyy-MM-dd HH:mm:ss");

                                    bool isPhaseStart = Lobby.listTournoiInProgress[indexTournmt].isPhaseStart;
                                   
                                    packet_tournmts.Append(" ");
                                    packet_tournmts.Append(monTournoi.tournmt_id);
                                    packet_tournmts.Append(" ");
                                    packet_tournmts.Append(state_lobby);
                                    packet_tournmts.Append(" ");
                                    packet_tournmts.Append(state_phase);
                                    packet_tournmts.Append(String.Format(" Phase_N°_{0} ", currentPhase));
                                    if (isPhaseStart)
                                    {
                                        packet_tournmts.Append("En cours...");
                                    }else
                                    {
                                        packet_tournmts.Append(nextPhaseStart);
                                    }
                                    packet_tournmts.Append(" ");
                                    packet_tournmts.Append(monTournoi.date_start);
                                    packet_tournmts.Append(" ");
                                    packet_tournmts.Append(monTournoi.nom_tournoi);
                                }
                            }
                        }
                        
                        break;
                    }
                }
            }

            return packet_tournmts.ToString();
        }

        // Récuperation des informations d'un tournoi pour le serveur de jeu
        public static string recupererTournmts(Client isPlaying = null)
        {
            string requete = String.Format("SELECT * FROM table_tournmts");

            List<structTournoi> mesTournois = new List<structTournoi>();
            string packet_tournmts = "list_tournmts ";
            
            using (MySqlConnection connec = new MySqlConnection(connectionString))
            using (MySqlCommand command = new MySqlCommand(requete, connec))
            {
                connec.Open();
                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        structTournoi monTournoi = new structTournoi();

                        monTournoi.tournmt_id = Convert.ToInt32(reader["TournmtID"]);
                        monTournoi.nom_tournoi = reader["TournmtName"].ToString();
                        monTournoi.max_players = Convert.ToInt32(reader["MaxPlayers"]);
                        monTournoi.deadline_reg = reader["DeadLineReg"].ToString();
                        monTournoi.date_start = reader["StartDate"].ToString();

                        mesTournois.Add(monTournoi);
                    }
                }
            }

            for (int i = 0; i < mesTournois.Count; i++)
            {
                requete = String.Format("SELECT COUNT(*) FROM table_register WHERE fkTournmtID = {0}", mesTournois[i].tournmt_id);

                using (MySqlConnection connec = new MySqlConnection(connectionString))
                using (MySqlCommand command = new MySqlCommand(requete, connec))
                {
                    connec.Open();

                    structTournoi monTournoi = new structTournoi();

                    monTournoi.tournmt_id = mesTournois[i].tournmt_id;
                    monTournoi.nom_tournoi = mesTournois[i].nom_tournoi;
                    monTournoi.max_players = mesTournois[i].max_players;
                    monTournoi.nb_inscrits = Convert.ToInt32(command.ExecuteScalar());
                    monTournoi.deadline_reg = mesTournois[i].deadline_reg;
                    monTournoi.date_start = mesTournois[i].date_start;

                    monTournoi.isregistered = 0;
                    
                    mesTournois[i] = monTournoi;
                    
                }
            }

            if (isPlaying != null)
            {
                int character_id = isPlaying.info_main.character_id;

                for (int i = 0; i < mesTournois.Count; i++)
                {
                    requete = String.Format("SELECT * FROM table_register WHERE fkTournmtID = {0} And fkCharacterID = {1}", mesTournois[i].tournmt_id, character_id);

                    using (MySqlConnection connec = new MySqlConnection(connectionString))
                    using (MySqlCommand command = new MySqlCommand(requete, connec))
                    {
                        connec.Open();
                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                structTournoi monTournoi = new structTournoi();

                                monTournoi = mesTournois[i];
                                monTournoi.isregistered = 1;

                                mesTournois[i] = monTournoi;
                            }
                        }
                    }
                }
            }

            for (int i = 0; i < mesTournois.Count; i++)
            {
                if (isPlaying != null)
                {
                    packet_tournmts += String.Format("{0} {1} {2} {3} {4} {5} {6}|",
                   mesTournois[i].tournmt_id, mesTournois[i].nb_inscrits, mesTournois[i].max_players, mesTournois[i].isregistered, mesTournois[i].deadline_reg, mesTournois[i].date_start, mesTournois[i].nom_tournoi);
                }else
                {
                    packet_tournmts += String.Format("{0} {1} {2} {3} {4} {5}|",
                  mesTournois[i].tournmt_id, mesTournois[i].nb_inscrits, mesTournois[i].max_players, mesTournois[i].deadline_reg, mesTournois[i].date_start, mesTournois[i].nom_tournoi);
                }
            }
            
            // On retire le '|' de trop
            packet_tournmts = packet_tournmts.Remove(packet_tournmts.Length - 1, 1);
            
            return packet_tournmts;
        }

        // Récuperation de l'information si le joueur est inscrit au tournoi
        public static bool isRegisteredInTournmt(Client isPlaying, int tournmt_id)
        {
            int character_id = isPlaying.info_main.character_id;

            string requete = String.Format("SELECT * FROM table_register WHERE fkCharacterID = {0} And fkTournmtID = {1}", character_id, tournmt_id);

            using (MySqlConnection connec = new MySqlConnection(connectionString))
            using (MySqlCommand command = new MySqlCommand(requete, connec))
            {
                connec.Open();
                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    if (dataReaderCount(reader) != 0)
                    {
                        return true;
                    }

                    return false;
                }
            }
        }

        // Désinscription d'un joueur à un tournoi
        public static void desinscriptionTournmt(Client isPlaying, int tournmt_id)
        {
            int character_id = isPlaying.info_main.character_id;
          
            string requete = String.Format("DELETE FROM table_register WHERE fkTournmtID = {0} And fKCharacterID = {1}", tournmt_id, character_id);

            using (MySqlConnection connec = new MySqlConnection(connectionString))
            using (MySqlCommand command = new MySqlCommand(requete, connec))
            {
                connec.Open();
                command.ExecuteNonQuery();
            }
        }

        // Inscription d'un joueur à un tournoi
        public static int inscriptionTournmt(Client isPlaying, int tournmt_id)
        {
            int max_players = 16;

            string requete = String.Format("SELECT * FROM table_tournmts WHERE TournmtID = {0}", tournmt_id);

            using (MySqlConnection connec = new MySqlConnection(connectionString))
            using (MySqlCommand command = new MySqlCommand(requete, connec))
            {
                connec.Open();
                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    DateTime dead_line = new DateTime();
                    DateTime start_date = new DateTime();
                    DateTime myDateTime = DateTime.Now;

                    string formattedDate = myDateTime.ToString("yyyy-MM-dd HH:mm:ss");

                    if (!reader.HasRows)
                    {
                        return 1; // Tournoi n'existe pas
                    }

                    while (reader.Read())
                    {
                        max_players = Convert.ToInt32(reader["MaxPlayers"]);
                        dead_line = Convert.ToDateTime(reader["DeadLineReg"]);
                        start_date = Convert.ToDateTime(reader["StartDate"]);
                    }

                    if (myDateTime >= start_date)
                    {
                        return 2; // Le tournoi a déjà commencé
                    }
                    if (myDateTime >= dead_line)
                    {
                        return 3; // Les inscriptions sont closes
                    }
                }
            }

            int character_id = isPlaying.info_main.character_id;

            requete = String.Format("SELECT * FROM table_register WHERE table_register.fkCharacterID = {1}", tournmt_id, character_id);

            using (MySqlConnection connec = new MySqlConnection(connectionString))
            using (MySqlCommand command = new MySqlCommand(requete, connec))
            {
                connec.Open();

                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        return 4; // Déjà inscrit dans un tournoi
                    }
                }
            }

            requete = String.Format("SELECT * FROM table_register WHERE fkTournmtID = '{0}'", tournmt_id);

            using (MySqlConnection connec = new MySqlConnection(connectionString))
            using (MySqlCommand command = new MySqlCommand(requete, connec))
            {
                connec.Open();
                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    if (dataReaderCount(reader) >= max_players)
                    {
                        return 5; // Plus de places disponibles
                    }
                }
            }

            requete = String.Format("INSERT INTO table_register (fkTournmtID, fkCharacterID) VALUES ('{0}', '{1}')", tournmt_id, character_id);

            using (MySqlConnection connec = new MySqlConnection(connectionString))
            using (MySqlCommand command = new MySqlCommand(requete, connec))
            {
                connec.Open();

                command.ExecuteNonQuery();
                return 6; // Inscription réussie
            }
        }

        struct structInfos_tournmt
        {
            public int fkCharacterID;
            public int fkTournmtID;
            public int currentPhase;
            public int candidateNumber;
            public bool alreadyAdded;
        }

        // Modification des informations du déroulement d'un tournoi
        public static void modificationInfosTournmt(int indexTournmt)
        {
            List<structInfos_tournmt> listInfosTournmt = new List<structInfos_tournmt>();

            Lobby.structTournoiCharacter[] characters = Lobby.listTournoiInProgress[indexTournmt].characters.ToArray();
            
            for (int i = 0; i < characters.Length; i++)
            {
                structInfos_tournmt newInfosTournmt = new structInfos_tournmt();

                newInfosTournmt.fkCharacterID = characters[i].client.info_main.character_id;
                newInfosTournmt.fkTournmtID = characters[i].client.info_game.info_tournmt.tournmtId;
                newInfosTournmt.currentPhase = characters[i].currentPhase;
                newInfosTournmt.candidateNumber = i;
                newInfosTournmt.alreadyAdded = false;

                listInfosTournmt.Add(newInfosTournmt);
            }

            for (int i = 0; i < listInfosTournmt.Count; i++)
            {
                string requete = String.Format("SELECT * FROM table_infos_tournmt WHERE fkCharacterID = '{0}' And fkTournmtID = '{1}'", 
                    listInfosTournmt[i].fkCharacterID, listInfosTournmt[i].fkTournmtID);

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    connection.Open();
                    using (MySqlCommand command = new MySqlCommand(requete, connection))
                    {
                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            if (dataReaderCount(reader) != 0)
                            {
                                structInfos_tournmt newInfosTournmts = new structInfos_tournmt();

                                newInfosTournmts.fkCharacterID = listInfosTournmt[i].fkCharacterID;
                                newInfosTournmts.fkTournmtID = listInfosTournmt[i].fkTournmtID;
                                newInfosTournmts.currentPhase = listInfosTournmt[i].currentPhase;
                                newInfosTournmts.candidateNumber = listInfosTournmt[i].candidateNumber;
                                newInfosTournmts.alreadyAdded = true;

                                listInfosTournmt[i] = newInfosTournmts;
                            }
                        }
                    }
                }

                if (listInfosTournmt[i].alreadyAdded)
                {
                    requete = String.Format("UPDATE table_infos_tournmt SET CurrentPhase = '{0}' WHERE fkCharacterID = '{1}' And fkTournmtID = '{2}'",
                        listInfosTournmt[i].currentPhase, listInfosTournmt[i].fkCharacterID, listInfosTournmt[i].fkTournmtID);
                }
                else
                {
                    requete = String.Format("INSERT INTO table_infos_tournmt (fkCharacterID, fkTournmtID, CurrentPhase, CandidateNumber) VALUES ('{0}', '{1}', '{2}', '{3}')",
                   listInfosTournmt[i].fkCharacterID, listInfosTournmt[i].fkTournmtID, listInfosTournmt[i].currentPhase, listInfosTournmt[i].candidateNumber);
                }

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    connection.Open();
                    using (MySqlCommand command = new MySqlCommand(requete, connection))
                    {
                        command.ExecuteNonQuery();
                    }
                }
            }
        }
	}
}
