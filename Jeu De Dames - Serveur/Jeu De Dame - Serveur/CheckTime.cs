﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Jeu_De_Dame___Serveur
{
    class CheckTime
    {
        // Traitement des délais entre les tours et les messages envoyés durant une rencontre
        public static void checkDuration()
        {
            while (true)
            {
                for (int i = 0; i < ClientManager.listClient.Count; i++)
                {
					timerTour(i);
					timerChat(i);
                }

                Thread.Sleep(100);
            }
        }

        // Traitement du délai d'attente des messages
		public static void timerChat(int indexClient)
		{
			if (ClientManager.listClient[indexClient].info_game.isplaying)
			{
				if (!ClientManager.listClient[indexClient].info_game.chatOn)
				{
					int timeCount = (Environment.TickCount / 1000) - (ClientManager.listClient[indexClient].info_game.timeChatCount / 1000);
					if (timeCount >= XMLData.config_delay_chat)
					{
						ClientManager.listClient[indexClient].info_game.chatOn = true;
					}
				}
			}
		}

        // Traitement de la durée d'un tour
        public static void timerTour(int indexClient)
        {
            if (ClientManager.listClient[indexClient].info_game.isplaying)
            {
                if (ClientManager.listClient[indexClient].info_game.tour)
                {
                    int timeCount = (Environment.TickCount / 1000) - (ClientManager.listClient[indexClient].info_game.timeTourCount / 1000);
                    int timeExist = ClientManager.listClient[indexClient].info_game.timeExist;

                    if (timeCount >= XMLData.config_delay_tour)
                    {
                        int indexOpponent = ClientManager.byPseudo(ClientManager.listClient[indexClient].info_game.opponent);

                        if (indexOpponent == -1)
                        {
                            return;
                        }

                        ClientManager.listClient[indexClient].info_game.asked = false;
                        ClientManager.listClient[indexOpponent].info_game.asked = false;

                        Careful.checkJumpingNotPlayed(ClientManager.listClient[indexClient], 0, 0, true);

                        PlayerManager.changeGameTurn(ClientManager.listClient[indexClient]);

                        ClientManager.listClient[indexClient].info_game.tourDown++;

                        if (ClientManager.listClient[indexClient].info_game.tourDown == XMLData.config_count_tour_down - 1)
                        {
                            ClientManager.listClient[indexClient].sendMsg("Attention ! Encore un tour non joué et vous serez déclaré forfait !", 2);
                        }
                        else if (ClientManager.listClient[indexClient].info_game.tourDown == XMLData.config_count_tour_down)
                        {
                            ClientManager.redirectEnding(ClientManager.listClient[indexClient], true);
                            return;
                        }

                        ClientManager.listClient[indexClient].sendMsg("Temps écoulé ! Changement de tour !", 1);
                        ClientManager.listClient[indexOpponent].sendMsg("Temps écoulé ! Changement de tour !", 1);
                    }
                    else if (timeCount % 10 == 0 && timeCount != 0 && timeExist != (XMLData.config_delay_tour - timeCount))
					{
                        int indexOpponent = ClientManager.byPseudo(ClientManager.listClient[indexClient].info_game.opponent);

                        if (indexOpponent == -1)
						{
							return;
						}

                        ClientManager.listClient[indexClient].info_game.timeExist = XMLData.config_delay_tour  - timeCount;
                        ClientManager.listClient[indexOpponent].info_game.timeExist = XMLData.config_delay_tour - timeCount;

                        ClientManager.listClient[indexClient].sendMsg("Il ne vous reste plus que " + (XMLData.config_delay_tour - timeCount) + " secondes !", 1);
                        ClientManager.listClient[indexOpponent].sendMsg("Il ne lui reste plus que " + (XMLData.config_delay_tour - timeCount) + " secondes !", 1);
					}
				}
            }
        }
    }
}