﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;

namespace Jeu_De_Dame___Serveur
{
    class Packet
    {
        // Traitement des packets réçu par les clients
        public static bool handler(Client isPlaying, string packet, bool firstRcv = false, Socket clientSocket = null)
        {
            string[] packetSpace = packet.Split(' ');

            if (packetSpace.Length < 1)
            {
                return false;
            }

            if (firstRcv)
            {
                if (packetSpace[0] == "addClient" && packetSpace.Length == 3)
                {
                    string pseudoByPacket = packetSpace[1];
                    string passeByPacket = packetSpace[2];

                    int accountIdByRequest = RequeteSQL.getAccountID(pseudoByPacket, passeByPacket);

                    string[] verifyAccount = RequeteSQL.verifyAccount(pseudoByPacket, passeByPacket).Split(' '); // Récupere l'état du compte ainsi que l'ID du personnage lié

                    if (verifyAccount.Length == 2)
                    {
                        InitializeServer.send(clientSocket, String.Format("verify_account {0}", verifyAccount[0]));

                        if (verifyAccount[0] == "2")
                        {
                            if (ClientManager.byPseudo(pseudoByPacket) == -1 && accountIdByRequest != -1) // Si le pseudo n'existe pas et que l'id de compte est correct
                            {
                                int character_id = Convert.ToInt32(verifyAccount[1]);

                                Client newClient = new Client(pseudoByPacket, passeByPacket, accountIdByRequest, character_id);

                                newClient.monSocket = clientSocket;
                                newClient.info_game.isplaying = false;
                                newClient.info_game.info_tournmt.isMatchTournmt = false;

                                ClientManager.listClient.Add(newClient);

                                RequeteSQL.setOnline(newClient, true);

                                return true;
                            }
                        }
                        else // On déconnecte le socket
                        {
                            clientSocket.Shutdown(SocketShutdown.Both);
                            clientSocket.Close();
                        }
                    }
                }
                return false;
            }

            bool playing = isPlaying.info_game.isplaying;

            Console.WriteLine("PACKET RECU : {0} : DE : {1}", packet, isPlaying.info_main.pseudo);

            if (packetSpace[0] == "get_score" && packetSpace.Length == 1 && !playing)
            {
                string packetScore = RequeteSQL.getScore(isPlaying);
                isPlaying.send(packetScore);
            }
            else if (packetSpace[0] == "inscription" && packetSpace.Length == 2 && !playing)
            {
                // 0 = carefulMode activé
                // 1 = carefulMode désactivé
                if (More.isDec(packetSpace[1]))
                {
                    bool connectedToLobby = Lobby.alreadyConnected(isPlaying);

                    if (!connectedToLobby)
                    {
                        bool isMatchTournmt = isPlaying.info_game.info_tournmt.isMatchTournmt;

                        if (!isMatchTournmt)
                        {
                            int carefulMode = Convert.ToInt32(packetSpace[1]);
                            MatchMaking.inscriptionMatch(isPlaying, carefulMode);

                            isPlaying.send("inscription_success 0");
                        }
                        else
                        {
                            isPlaying.sendMsgBox("Impossible en phase de tournoi.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        isPlaying.sendMsgBox("Vous êtes connecté dans la salle d'attente du tournoi, vous ne pouvez pas chercher d'adversaire.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else if (packetSpace[0] == "inscription" && packetSpace.Length == 3 && !playing)
            {
                // 0 = carefulMode activé
                // 1 = carefulMode désactivé
                if (More.isDec(packetSpace[2]))
                {
                    bool connectedToLobby = Lobby.alreadyConnected(isPlaying);
                    if (!connectedToLobby)
                    {
                        bool isMatchTournmt = isPlaying.info_game.info_tournmt.isMatchTournmt;

                        if (!isMatchTournmt)
                        {
                            int carefulMode = Convert.ToInt32(packetSpace[2]);
                            MatchMaking.inscriptionMatchByPseudo(isPlaying, packetSpace[1], carefulMode);

                            isPlaying.send("inscription_success 1");
                        }
                        else
                        {
                            isPlaying.sendMsgBox("Impossible en phase de tournoi.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        isPlaying.sendMsgBox("Vous êtes connecté dans la salle d'attente du tournoi, vous ne pouvez pas chercher d'adversaire.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else if (packetSpace[0] == "stop_inscription" && packetSpace.Length == 1 && !playing)
            {
                MatchMaking.annulerInscription(isPlaying);
            }
            else if (packetSpace[0] == "infos_opponent" && packetSpace.Length == 1 && playing)
            {
                string infoOpponent = RequeteSQL.getInfosOpponent(isPlaying.info_game.opponent);
                isPlaying.send("infos_player " + infoOpponent);
            }
            else if (packetSpace[0] == "select" && packetSpace.Length == 5 && playing)
            {
                if (More.isDec(packetSpace[1]) && More.isDec(packetSpace[2]) &&
                More.isDec(packetSpace[3]) && More.isDec(packetSpace[4]))
                {
                    int x = More.s_int(packetSpace[1]);
                    int y = More.s_int(packetSpace[2]);
                    int xSelected = More.s_int(packetSpace[3]);
                    int ySelected = More.s_int(packetSpace[4]);

                    if (x >= 0 && x <= 9 && y >= 0 && y <= 9 &&
                        xSelected >= 0 && xSelected <= 9 && ySelected >= 0 && ySelected <= 9)
                    {
                        Action.pawnMoving(isPlaying, x, y, xSelected, ySelected);
                    }
                    return true;
                }
            }
            else if (packetSpace[0] == "req_result" && packetSpace.Length == 4 && playing)
            {
                if (More.isDec(packetSpace[1]) && More.isDec(packetSpace[2]) &&
                More.isDec(packetSpace[3]))
                {
                    if (isPlaying.info_game.asked)
                    {
                        int result = More.s_int(packetSpace[1]);
                        int x = More.s_int(packetSpace[2]);
                        int y = More.s_int(packetSpace[3]);

                        if (result == 1)
                        {
                            if (x >= 0 && x <= 9 && y >= 0 && y <= 9)
                            {
                                Action.setCase(isPlaying, x, y);
                            }
                        }
                        isPlaying.info_game.asked = false;
                    }
                    else
                    {
                        isPlaying.send("msg Vous avez pris trop de temps à répondre");
                    }
                    return true;
                }
            }
            else if (packetSpace[0] == "chat" && packetSpace.Length >= 2 && playing)
            {
                if (isPlaying.info_game.chatOn)
                {
                    int indexOpponent = ClientManager.byPseudo(isPlaying.info_game.opponent);
                    if (indexOpponent == -1)
                    {
                        return false;
                    }
                    isPlaying.info_game.timeChatCount = Environment.TickCount;

                    isPlaying.info_game.chatOn = false;

                    string messageChat = "[" + isPlaying.info_main.pseudo + "]: ";

                    if (packet.Length < 46)
                    {
                        for (int i = 1; i < packetSpace.Length; i++)
                        {
                            messageChat += packetSpace[i];
                            messageChat += " ";
                        }
                    }
                    else
                    {
                        messageChat = "Votre message ne peut contenir plus de 40 caractères ...";
                    }

                    isPlaying.sendMsg(messageChat, 0);

                    ClientManager.listClient[indexOpponent].sendMsg(messageChat, 0);
                }
                else
                {
                    isPlaying.sendMsg("Vous pouvez pas envoyer de message pour l'instant", 1);
                }
            }
            else if (packetSpace[0] == "get_tournmts" && packetSpace.Length == 1 && !playing)
            {
                string packet_tournmts = RequeteSQL.recupererTournmts(isPlaying);

                isPlaying.send(packet_tournmts);
            }
            else if (packetSpace[0] == "inscription_tournmts" && packetSpace.Length == 2 && !playing)
            {
                if (More.isDec(packetSpace[1]))
                {
                    int tournmt_id = More.s_int(packetSpace[1]);
                    int response_inscription_tournmt = RequeteSQL.inscriptionTournmt(isPlaying, tournmt_id);

                    isPlaying.send(String.Format("response_inscription_tournmt {0}", response_inscription_tournmt));
                }
            }
            else if (packetSpace[0] == "desinscription_tournmts" && packetSpace.Length == 2 && !playing)
            {
                if (More.isDec(packetSpace[1]))
                {
                    int tournmt_id = More.s_int(packetSpace[1]);

                    bool isconnectedLobby = Lobby.alreadyConnected(isPlaying);

                    if (!isconnectedLobby)
                    {
                        RequeteSQL.desinscriptionTournmt(isPlaying, tournmt_id);

                        isPlaying.send("desinscription_tournmt_ok");
                    }
                    else
                    {
                        isPlaying.sendMsgBox("Vous êtes connecté dans la salle d'attente du tournoi, vous ne pouvez pas annuler votre inscription.",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else if (packetSpace[0] == "get_lobby_tournmt" && packetSpace.Length == 1 && !playing)
            {
                string packet_lobby_tournmt = RequeteSQL.recupererTournmtsRegistered(isPlaying);

                isPlaying.send(packet_lobby_tournmt);
            }
            else if (packetSpace[0] == "connect_lobby" && packetSpace.Length == 2 && !playing)
            {
                if (More.isDec(packetSpace[1]))
                {
                    int tournmt_id = More.s_int(packetSpace[1]);

                    if (Lobby.connectLobby(isPlaying, tournmt_id))
                    {
                        isPlaying.sendMsgBox("Vous êtes connecté au Lobby", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        isPlaying.sendMsgBox("La connexion au Lobby est impossible pour le moment.@Veuillez réessayez plus tard", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else if (packetSpace[0] == "disconnect_lobby" && packetSpace.Length == 1 && !playing)
            {
                if (Lobby.disconnectLobby(isPlaying))
                {
                    isPlaying.sendMsgBox("Vous vous êtes déconnecté du Lobby", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    isPlaying.sendMsgBox("La déconnexion au Lobby est impossible a rencontré un problème.@Veuillez réessayez plus tard", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else if (packetSpace[0] == "connect_phase_tournmt" && packetSpace.Length == 2 && !playing)
            {
                if (More.isDec(packetSpace[1]))
                {
                    int tournmt_id = More.s_int(packetSpace[1]);
                    if (Lobby.connectPhaseTournmtInProgress(isPlaying, tournmt_id))
                    {
                        isPlaying.sendMsgBox("Vous vous êtes bien connecté pour la prochaine phase du tournoi@Veuillez patientez.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        isPlaying.sendMsgBox("Impossible de se connecter au tournoi en cours !", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else if (packetSpace[0] == "disconnect_phase_tournmt" && packetSpace.Length == 1 && !playing)
            {
                if (Lobby.disconnectPhaseTournmtInProgress(isPlaying))
                {
                    isPlaying.sendMsgBox("Vous venez de vous deconnecter au tournoi.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }else
                {
                    isPlaying.sendMsgBox("Impossible de se deconnecter au tournoi en cours !", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            return false;
        }
    }
}