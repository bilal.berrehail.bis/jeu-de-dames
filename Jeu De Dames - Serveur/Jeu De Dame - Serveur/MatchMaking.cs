﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeu_De_Dame___Serveur
{
	class MatchMaking
	{
		static List<Client> attente = new List<Client>(); // Liste d'attente des inscrits

        // Inscription d'un joueur avec ses préférences
		public static void inscriptionMatch(Client monClient, int carefulMode)
		{
            if (monClient.info_game.isplaying || dejaInscrit(monClient) || dejaInscritByPseudo(monClient))
			{
				return;
			}

            monClient.info_game.carefulMode = Convert.ToBoolean(carefulMode);
            
            attente.Add(monClient);

            if (attente.Count >= 2)
			{
                rechercherAdversaire(monClient);
			}
		}

        // Recherche d'un joueur possédant les mêmes préférences
        public static void rechercherAdversaire(Client monClient)
        {
            for (int i = 0; i < attente.Count; i++)
            {
                if (attente[i].info_main.pseudo != monClient.info_main.pseudo)
                {
                    if (attente[i].info_game.carefulMode == monClient.info_game.carefulMode)
                    {
                        int indexJoueur1 = ClientManager.byPseudo(attente[i].info_main.pseudo);
                        int indexJoueur2 = ClientManager.byPseudo(monClient.info_main.pseudo);

                        if (indexJoueur1 == -1 || indexJoueur2 == -1)
                        {
                            return;
                        }

                        supprimerListeattente(attente[i].info_main.pseudo);
                        supprimerListeattente(monClient.info_main.pseudo);
                        matchTrouver(indexJoueur1, indexJoueur2);
                    }
                }
            }
        }

        // Suppression de l'inscription du joueur
        public static void annulerInscription(Client monClient)
        {
            if (monClient.info_game.isplaying )
            {
                return;
            }

            if (dejaInscrit(monClient))
            {
                supprimerListeattente(monClient.info_main.pseudo);
            }
            else if (dejaInscritByPseudo(monClient)) // Enlève de la liste d'attente des recherches par pseudo
            {
                int indexClient = getIndexClientByPseudo(monClient.info_main.pseudo);

                if (indexClient != -1)
                {
                    attenteByPseudo.RemoveAt(indexClient);
                }
            }
        }

        // Mise en relation de deux joueurs
		public static void matchTrouver(int indexJoueur1, int indexJoueur2)
		{
            ClientManager.listClient[indexJoueur1].info_game.opponent = ClientManager.listClient[indexJoueur2].info_main.pseudo;
            ClientManager.listClient[indexJoueur2].info_game.opponent = ClientManager.listClient[indexJoueur1].info_main.pseudo;

            ClientManager.listClient[indexJoueur1].info_main.playerTop = false;
            ClientManager.listClient[indexJoueur2].info_main.playerTop = true;

            ClientManager.listClient[indexJoueur1].info_game.tour = true;
            ClientManager.listClient[indexJoueur2].info_game.tour = false;

            Match.startGame(indexJoueur1, indexJoueur2);
		}

        // Suppression d'un inscrit à la liste d'attente depuis son nom
		public static void supprimerListeattente(string pseudo)
		{
			for (int i = 0; i < attente.Count; i++)
			{
				if (attente[i].info_main.pseudo == pseudo)
				{
					attente.RemoveAt(i);
					return;
				}
			}
		}

        // Permet de savoir si le joueur est déjà inscrit à la liste d'attente
        public static bool dejaInscrit(Client monClient)
		{
			for (int i = 0; i < attente.Count; i++)
			{
                if (attente[i].info_main.pseudo == monClient.info_main.pseudo)
				{
					return true;
				}
			}
			return false;
		}

        //------------------------------------------------------------------------------------------------------

        // Structure dédié à une liste d'attente avec la recherche par pseudo
        struct structAttenteByPseudo
        {
            public Client client;
            public string pseudo_opponent;
        }

        static List<structAttenteByPseudo> attenteByPseudo = new List<structAttenteByPseudo>(); // Liste d'attente des inscrits ayant référencé un pseudo dans ses préférences

        // Inscription du joueur à la liste d'attente ayant référéncé un pseudo dans ses préférences
        public static void inscriptionMatchByPseudo(Client monClient, string pseudo_opponent, int carefulMode)
        {
            if (monClient.info_game.isplaying || dejaInscrit(monClient) || dejaInscritByPseudo(monClient))
            {
                return;
            }

            if (monClient.info_main.pseudo == pseudo_opponent)
            {
                return;
            }

            monClient.info_game.carefulMode = Convert.ToBoolean(carefulMode);

            structAttenteByPseudo newAttente = new structAttenteByPseudo();
            newAttente.client = monClient;
            newAttente.pseudo_opponent = pseudo_opponent;

            attenteByPseudo.Add(newAttente);

            meetingByPseudo(monClient, pseudo_opponent);
        }

        // Mise en relation de deux joueurs
        public static void meetingByPseudo(Client monClient, string pseudo_opponent)
        {
            string pseudo = monClient.info_main.pseudo;

            int indexOpponent = ClientManager.byPseudo(pseudo_opponent);

            if (indexOpponent != -1)
            {
                Client clientOpponent = ClientManager.listClient[indexOpponent];

                string opponent_pseudoOpponent = getPseudoOpponent(clientOpponent);

                if (opponent_pseudoOpponent != "")
                {
                    if (pseudo == opponent_pseudoOpponent)
                    {
                        // Relation entre les joueurs

                        int indexJoueur1 = ClientManager.byPseudo(pseudo);
                        int indexJoueur2 = ClientManager.byPseudo(pseudo_opponent);

                        if (indexJoueur1 == -1 || indexJoueur1 == -1)
                        {
                            return;
                        }

                        if (ClientManager.listClient[indexJoueur1].info_game.carefulMode ==
                            ClientManager.listClient[indexJoueur2].info_game.carefulMode)
                        {
                            supprimerListeattenteByPseudo(pseudo);
                            supprimerListeattenteByPseudo(pseudo_opponent);

                            matchTrouver(indexJoueur1, indexJoueur2);
                        }
                    }
                }
            }
        }

        // Récuperation de l'index d'un joueur dans la liste d'attente depuis son nom
        public static int getIndexClientByPseudo(string pseudo)
        {
            for (int i = 0; i < attenteByPseudo.Count; i++)
            {
                if (attenteByPseudo[i].client.info_main.pseudo == pseudo)
                {
                    return i;
                }
            }
            return -1;
        }

        // Récuperation du pseudo du joueur référencé dans les préférences d'un joueur
        public static string getPseudoOpponent(Client monClient)
        {
            string pseudo = monClient.info_main.pseudo;

            for (int i = 0; i < attenteByPseudo.Count; i++)
            {
                if (attenteByPseudo[i].client.info_main.pseudo == pseudo)
                {
                    return attenteByPseudo[i].pseudo_opponent;
                }
            }
            
            return "";
        }

        // Suppression d'un joueur dans la liste d'attente depuis son pseudo
        public static void supprimerListeattenteByPseudo(string pseudo)
        {
            for (int i = 0; i < attenteByPseudo.Count; i++)
            {
                if (attenteByPseudo[i].client.info_main.pseudo == pseudo)
                {
                    attenteByPseudo.RemoveAt(i);
                    return;
                }
            }
        }

        // Permet de savoir si le joueur est déjà inscrit à la liste d'attente en ayant référencé un pseudo dans ses préferences
        public static bool dejaInscritByPseudo(Client monClient)
        {
            for (int i = 0; i < attenteByPseudo.Count; i++)
            {
                if (attenteByPseudo[i].client.info_main.pseudo == monClient.info_main.pseudo)
                {
                    return true;
                }
            }
            return false;
        }
	}
}
