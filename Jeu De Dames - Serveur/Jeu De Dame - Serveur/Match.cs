﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeu_De_Dame___Serveur
{
    class Match
    {
        // Vérification des variables de jeux et lancement du match aux deux joueurs
        public static bool startGame(int indexJoueur1, int indexJoueur2)
        {
            string pseudoTop;
            string pseudoBot;
            
            if (ClientManager.listClient[indexJoueur1].info_game.carefulMode !=
                ClientManager.listClient[indexJoueur2].info_game.carefulMode)
            {
                return false;
            }

            if (ClientManager.listClient[indexJoueur1].info_main.playerTop ==
                ClientManager.listClient[indexJoueur2].info_main.playerTop)
            {
                return false;
            }

            if (ClientManager.listClient[indexJoueur1].info_game.opponent !=
                ClientManager.listClient[indexJoueur2].info_main.pseudo ||
                ClientManager.listClient[indexJoueur2].info_game.opponent !=
                ClientManager.listClient[indexJoueur1].info_main.pseudo)
            {
                return false;
            }

            if (ClientManager.listClient[indexJoueur1].info_game.isplaying ||
                ClientManager.listClient[indexJoueur2].info_game.isplaying)
            {
                return false;
            }

            if (ClientManager.listClient[indexJoueur1].info_main.playerTop)
            {
                pseudoTop = ClientManager.listClient[indexJoueur1].info_main.pseudo;
                pseudoBot = ClientManager.listClient[indexJoueur2].info_main.pseudo;
            }else
            {
                pseudoTop = ClientManager.listClient[indexJoueur2].info_main.pseudo;
                pseudoBot = ClientManager.listClient[indexJoueur1].info_main.pseudo;
            }

            ClientManager.listClient[indexJoueur1].info_game.timeTourCount = Environment.TickCount;
            ClientManager.listClient[indexJoueur2].info_game.timeTourCount = Environment.TickCount;

            ClientManager.listClient[indexJoueur1].info_game.isplaying = true;
            ClientManager.listClient[indexJoueur2].info_game.isplaying = true;

            ClientManager.listClient[indexJoueur1].info_game.pawnAlive = 20;
            ClientManager.listClient[indexJoueur2].info_game.pawnAlive = 20;

            ClientManager.listClient[indexJoueur1].info_game.tourDown = 0;
            ClientManager.listClient[indexJoueur2].info_game.tourDown = 0;

            ClientManager.listClient[indexJoueur1].info_game.plateauCases = new Plateau.cases[10][];
            ClientManager.listClient[indexJoueur2].info_game.plateauCases = new Plateau.cases[10][];

            Plateau.remplirPlateau(ClientManager.listClient[indexJoueur1]);
            ImageManager.initializePawn(ClientManager.listClient[indexJoueur1]);

            synchroWithOpponents(ClientManager.listClient[indexJoueur1]);

            int joueurUnIsTop = Convert.ToInt32(ClientManager.listClient[indexJoueur1].info_main.playerTop);
            int joueurDeuxIsTop = Convert.ToInt32(ClientManager.listClient[indexJoueur2].info_main.playerTop);

             // match <pseudo de l'adversaire> <son index>
            ClientManager.listClient[indexJoueur1].send("match " + ClientManager.listClient[indexJoueur2].info_main.pseudo + " " + joueurUnIsTop);
            ClientManager.listClient[indexJoueur2].send("match " + ClientManager.listClient[indexJoueur1].info_main.pseudo + " " + joueurDeuxIsTop);
            
            return true;
        }

        // Synchronisation des variables de plateaux entre les joueurs se rencontrant
        public static void synchroWithOpponents(Client isPlaying)
        {
            int indexOpponent = ClientManager.byPseudo(isPlaying.info_game.opponent);

            if (indexOpponent == -1)
			{
				return;
			}

            ClientManager.listClient[indexOpponent].info_game.plateauCases = isPlaying.info_game.plateauCases;
        }
    }
}