﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jeu_De_Dame___Serveur
{
    class More
    {
        // Conversion byte[] en string
        public static string b_str(byte[] buffer, int packetlength = -1)
        {
            if (packetlength != -1)
            {
                return System.Text.Encoding.UTF8.GetString(buffer).Substring(0, packetlength);
            }
            return System.Text.Encoding.UTF8.GetString(buffer);
        }

        // Conversion string en byte[]
        public static byte[] s_byte(string packet)
        {
            return System.Text.Encoding.UTF8.GetBytes(packet);
        }

        // Renvoi une valeur true si la chaîne de caractère contient uniquement des décimales
        public static bool isDec(string value)
        {
            int nb;
            return int.TryParse(value, out nb);
        }

        // Conversion string en int
        public static int s_int(string str)
        {
            return Convert.ToInt32(str);
        }

        // Conversion int en string
        public static string i_str(int value)
        {
            return Convert.ToString(value);
        }

        // Récuperation de la date et l'heure d'aujourd'hui
        public static string getDateNow()
        {
            return DateTime.Now.ToString();
        }
    }
}
