﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Net.Sockets;

namespace Jeu_De_Dame___Serveur
{
    class InitializeServer
    {
        public static ManualResetEvent allDone = new ManualResetEvent(false); // Variable permettant de faire le lien entre différents threads
        
        public class tmpClient
        {
            public Socket workSocket;
            public byte[] buffer = new byte[1024];
        }

        // Mise en écoute du serveur de connexion
        public static void startListening(string ip, int port)
        {
            byte[] bytes = new byte[1024];

            IPAddress ipAddress = IPAddress.Parse(ip);
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, port);

            Socket listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            try
            {
                listener.Bind(localEndPoint);
                listener.Listen(100);

                Console.WriteLine(String.Format("Le serveur vient de démarrer [ IP : {0} - PORT : {1}]", ip, port));

                while (true)
                {
                    allDone.Reset();

                    listener.BeginAccept(new AsyncCallback(acceptCallBack), listener);

                    allDone.WaitOne();
                }
            }
            catch (Exception ex)
            { }

            Console.WriteLine("Une erreur est survenue ! Veuillez vérifiez l'IP et le port du serveur de connexion.");
            Console.ReadLine();
            Environment.Exit(1);
        }

        // Récéption d'un nouveau client
        public static void acceptCallBack(IAsyncResult ar)
        {
            try
            {
                allDone.Set();

                Socket listener = (Socket)ar.AsyncState;
                Socket handler = listener.EndAccept(ar);

                tmpClient client = new tmpClient();
                client.workSocket = handler;

                handler.BeginReceive(client.buffer, 0, 1024, 0, new AsyncCallback(readCallbackCreateClient), client);
            }
            catch (Exception ex)
            { }
        }

        // Récuperation des premiers paquets envoyés par le client
        public static void readCallbackCreateClient(IAsyncResult ar)
        {
            string content = "";

            tmpClient _tmpClient = (tmpClient)ar.AsyncState;
            Socket handler = _tmpClient.workSocket;

            try
            {
                int bytesRead = handler.EndReceive(ar);

                if (bytesRead > 0)
                {
                    content = Encoding.UTF8.GetString(_tmpClient.buffer, 0, bytesRead);

                    if (!Packet.handler(null, content, true, handler))
                    {
                        disconnectSocket(handler);
                        return;
                    }

                    handler.BeginReceive(_tmpClient.buffer, 0, 1024, 0, new AsyncCallback(readCallback), _tmpClient);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                disconnectSocket(handler);
            }
        }

        // Récuperation des paquets suivants envoyés par le client
        public static void readCallback(IAsyncResult ar)
        {
            string content = "";

            tmpClient _tmpClient = (tmpClient)ar.AsyncState;
            Socket handler = _tmpClient.workSocket;

            try
            {
                int bytesRead = handler.EndReceive(ar);

                if (bytesRead > 0)
                {
                    content = Encoding.UTF8.GetString(_tmpClient.buffer, 0, bytesRead);

                    int indexClient = ClientManager.bySocket(handler);
                    if (indexClient >= 0)
                    {
                        Client client = ClientManager.listClient[indexClient];

                        Packet.handler(client, content);

                        handler.BeginReceive(_tmpClient.buffer, 0, 1024, 0, new AsyncCallback(readCallback), _tmpClient);
                    }
                }
            }
            catch (Exception ex)
            {
                disconnectSocket(handler);
            }
        }

        // Création d'un caractères entre chaque paquets afin de pouvoir la découper à la réception
        public static string packetNormalization(string packet)
        {
            return packet + Convert.ToChar(0x01);
        }

        // Préparation d'un paquet à envoyé au client
        public static void send(Socket handler, string data)
        {
            try
            {
                byte[] byteData = Encoding.UTF8.GetBytes(packetNormalization(data));

                handler.BeginSend(byteData, 0, byteData.Length, 0, new AsyncCallback(sendCallback), handler);
            }
            catch (Exception ex)
            { }
        }

        // Envoi du paquet au client
        private static void sendCallback(IAsyncResult ar)
        {
            try
            {
                Socket handler = (Socket)ar.AsyncState;

                int bytesSent = handler.EndSend(ar);
            }
            catch (Exception e)
            { }
        }

        // Déconnexion d'un client
        public static void disconnectSocket(Socket socket)
        {
            try
            {
                int indexClient = ClientManager.bySocket(socket);
                if (indexClient >= 0)
                {
                    Client client = ClientManager.listClient[indexClient];
                    client.deconnexion();
                }
                else if (socket != null)
                {
                    socket.Shutdown(SocketShutdown.Both);
                    socket.Close();
                }
            }
            catch (Exception ex)
            {
            }
        }

        // Vérification de l'état de connexion d'un client
        public static bool isConnected(Socket socket)
        {
            try
            {
                return !(socket.Poll(1, SelectMode.SelectRead) && socket.Available == 0);
            }
            catch (SocketException) { return false; }
        }
    }
}
