﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Jeu_De_Dame___Serveur
{
    class Program
    {
        public static string fileNameData = "config.xml"; // Nom du fichier de configuration

        public static Thread checkTime; // Thread gérant du délai entre les messages et les tours
        public static Thread checkLobbies; // Thread gérant le déroulement des tournois

        // Fonction principale du programme
        static void Main(string[] args)
        {
            if (!XMLData.loadServerConfigXml(fileNameData))
            {
                Console.WriteLine("Une erreur est survenue au chargement du fichier de configurations [/game/server] : " + fileNameData);
                Console.ReadLine();
                return;
            }

            checkTime = new Thread(CheckTime.checkDuration);
            checkTime.Start();

            checkLobbies = new Thread(Lobby.checkLobbies);
            checkLobbies.Start();

            if (!RequeteSQL.initialisation())
            {
                Console.WriteLine("Une erreur est survenue au chargement du fichier de configuration [/game/mysql] : " + fileNameData);
                Console.ReadLine();
                return;
            }

            Console.WriteLine("Chargement de la base de donnée effectué");

            InitializeServer.startListening(XMLData.ip, XMLData.port);
        }
    }
}
